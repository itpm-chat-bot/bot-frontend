FROM node:lts-alpine as build-stage
ARG CI_ENVIRONMENT_NAME
ARG DOMAIN_NAME

WORKDIR /app
COPY . .
RUN yarn install
RUN yarn lint
RUN VITE_APP_CI_ENV=$CI_ENVIRONMENT_NAME VITE_APP_API_BASE_URL=https://$DOMAIN_NAME yarn build

# copy build to the server
FROM scratch AS export-stage
COPY --from=build-stage /app/dist .
