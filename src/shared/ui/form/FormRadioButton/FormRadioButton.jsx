import { Controller } from 'react-hook-form';

import { RadioButton } from '../../RadioButton/RadioButton';

export function FormRadioButton({ control, name, rules, ...restProps }) {
    return (
        <Controller
            control={control}
            name={name}
            render={({ field, fieldState }) => {
                const { disabled: _, ...restField } = field;
                const { error } = fieldState;
                const errorMessage = error?.message;

                return (
                    <RadioButton
                        {...restProps}
                        {...restField}
                        error={errorMessage}
                    />
                );
            }}
            /** @todo: убрать rules, когда появится zod резолвер */
            rules={rules}
        />
    );
}

FormRadioButton.displayName = 'FormRadioButton';
