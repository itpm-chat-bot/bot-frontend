import { Controller } from 'react-hook-form';

import { Input } from '../../Input/Input';

export function FormInput({ control, name, rules, transform, ...restProps }) {
    return (
        <Controller
            control={control}
            name={name}
            render={({ field, fieldState }) => {
                const { disabled: _, onChange: __, ...restField } = field;
                const { error } = fieldState;
                const length = field.value.length;
                const errorMessage = error?.message;

                return (
                    <Input
                        {...restProps}
                        {...restField}
                        error={errorMessage}
                        length={length}
                        onChange={(e) => {
                            const { value } = e.target;
                            const preparedValue = transform ? transform(value) : value;
                            field.onChange(preparedValue);
                        }}
                    />
                );
            }}
            /** @todo: убрать rules, когда появится zod резолвер */
            rules={rules}
        />
    );
}

FormInput.displayName = 'FormInput';
