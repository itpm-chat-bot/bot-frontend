import { Controller } from 'react-hook-form';

import { RichTextEditor } from '../../RichTextEditor/RichTextEditor';

export function FormRichTextEditor({ control, name, rules, transform, ...restProps }) {
    return (
        <Controller
            control={control}
            name={name}
            render={({ field, fieldState }) => {
                const { disabled: _, onChange: __, ...restField } = field;
                const { error } = fieldState;
                const length = field.value.length;
                const errorMessage = error?.message;

                return (
                    <RichTextEditor
                        {...restProps}
                        {...restField}
                        error={errorMessage}
                        length={length}
                        onChange={(e) => {
                            const { value } = e.target;
                            const preparedValue = transform ? transform(value) : value;
                            field.onChange(preparedValue);
                        }}
                    />

                );
            }}
            /** @todo: убрать rules, когда появится zod резолвер */
            rules={rules}
        />
    );
}

FormRichTextEditor.displayName = 'FormRichTextEditor';
