import { Controller } from 'react-hook-form';

import { Switch } from '../../Switch/Switch';

export function FormSwitch({ control, name, rules, ...restProps }) {
    return (
        <Controller
            control={control}
            name={name}
            render={({ field }) => {
                const { disabled: _, onChange: __, ...restField } = field;

                return (
                    <Switch {...restProps} {...restField} />
                );
            }}
            /** @todo: убрать rules, когда появится zod резолвер */
            rules={rules}
        />
    );
}

FormSwitch.displayName = 'FormSwitch';
