import { Controller } from 'react-hook-form';

import { TextArea } from '../../TextArea/TextArea';

export function FormTextArea({ control, name, rules, transform, ...restProps }) {
    return (
        <Controller
            control={control}
            name={name}
            render={({ field, fieldState }) => {
                const { disabled: _, onChange: __, ...restField } = field;
                const { error } = fieldState;
                const length = field.value.length;
                const errorMessage = error?.message;

                return (
                    <TextArea
                        {...restProps}
                        {...restField}
                        error={errorMessage}
                        length={length}
                        onChange={(e) => {
                            const { value } = e.target;
                            const preparedValue = transform ? transform(value) : value;
                            field.onChange(preparedValue);
                        }}
                    />
                );
            }}
            /** @todo: убрать rules, когда появится zod резолвер */
            rules={rules}
        />
    );
}

FormTextArea.displayName = 'FormTextArea';
