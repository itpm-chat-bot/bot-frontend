/* This component took the concept from here https://github.com/thisisamir98/telegram-login-button */

import { useRef, useEffect } from 'react';

import PropTypes from 'prop-types';

export function TelegramLoginButton({
    botUsername,
    buttonSize,
    showPic,
    cornerRadius,
    hasRequestAccess,
    authUrl,
    className,
    ...props
}) {
    const ref = useRef(null);

    useEffect(() => {
        if (ref.current === null) {
            return;
        }

        const script = document.createElement('script');
        script.async = true;
        script.src = 'https://telegram.org/js/telegram-widget.js?22';
        script.setAttribute('data-telegram-login', botUsername);
        script.setAttribute('data-size', buttonSize);

        if (showPic !== true) {
            script.setAttribute('data-userpic', 'false');
        }

        if (cornerRadius !== undefined) {
            script.setAttribute('data-radius', cornerRadius.toString());
        }

        if (hasRequestAccess) {
            script.setAttribute('data-request-access', 'write');
        }

        script.setAttribute('data-auth-url', authUrl);

        ref.current.appendChild(script);
    }, [botUsername, buttonSize, cornerRadius, authUrl, hasRequestAccess, showPic, ref]);

    useEffect(() => {
        const config = {
            attributes: false,
            characterData: false,
            childList: true,
            subtree: false,
            attributeOldValue: false,
            characterDataOldValue: false,
        };

        const observer = new MutationObserver((mutations) => {
            mutations
                .map((mutation) => mutation.addedNodes)
                .forEach((newNode) => {
                    newNode.forEach((node) => {
                        if (node.id === `telegram-login-${botUsername}`) {
                            ref.current.style.opacity = 1;
                        }
                    });
                });
        });

        observer.observe(ref.current, config);

        return () => observer.disconnect();
    }, [botUsername]);

    return (
        <div
            ref={ref}
            className={className}
            style={{
                opacity: '0',
                transition: 'opacity 2.5s',
            }}
            {...props}
        />
    );
}

TelegramLoginButton.propTypes = {
    botUsername: PropTypes.string.isRequired,
    buttonSize: PropTypes.string,
    showPic: PropTypes.bool,
    cornerRadius: PropTypes.number,
    hasRequestAccess: PropTypes.bool,
    authUrl: PropTypes.string.isRequired,
    className: PropTypes.string,
};

TelegramLoginButton.defaultProps = {
    buttonSize: 'large',
    showPic: true,
    cornerRadius: 14,
    hasRequestAccess: true,
    className: '',
};
