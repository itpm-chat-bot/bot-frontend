import PropTypes from 'prop-types';

import { cn } from '@/shared/lib';

import styles from './Loader.module.css';

export function Loader({ size, show }) {
    return (
        <div className={styles.container} data-show={show}>
            <div
                className={cn(
                    styles.loader,
                    styles[size],
                )}
            />
        </div>
    );
}

Loader.propTypes = {
    size: PropTypes.oneOf(['big', 'medium']),
    show: PropTypes.bool,
};

Loader.defaultProps = {
    size: 'big',
    show: false,
};
