import PropTypes from 'prop-types';

export function RadioGroup({ legend, children, ...props }) {
    return (
        <fieldset {...props}>
            {legend && <legend>{legend}</legend>}
            {children}
        </fieldset>
    );
}

RadioGroup.propTypes = {
    legend: PropTypes.string,
    children: PropTypes.node,
};

RadioGroup.defaultProps = {
    legend: null,
    children: null,
};
