import { useState, useEffect, forwardRef } from 'react';

import { DatePicker } from 'antd';
import PropTypes from 'prop-types';

export const DatePickerWithoutCaching = forwardRef((props, ref) => {
    const { value } = props;
    const [key, setKey] = useState(0);

    useEffect(() => {
        if (value === null) {
            setKey((k) => k + 1);
        }
    }, [value]);

    return <DatePicker key={key} ref={ref} {...props} />;
});

DatePickerWithoutCaching.displayName = 'DatePickerWithoutCaching';

/* eslint eslint-comments/no-use: off */
DatePickerWithoutCaching.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    value: PropTypes.object,
};

DatePickerWithoutCaching.defaultProps = {
    value: null,
};
