import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';

import PropTypes from 'prop-types';

import { cn } from '@/shared/lib';

import styles from './NavTabs.module.css';

function Tab({ content, link, ...props }) {
    return (
        <NavLink
            className={({ isActive }) => (
                cn(styles.tab, {
                    [styles.active]: isActive,
                })
            )}
            to={link}
            {...props}
        >
            {content}
        </NavLink>
    );
}

export function NavTabs({ items, namespace, ...props }) {
    const { t } = useTranslation(namespace);

    return (
        <div className={styles.tabs} {...props}>
            {items.map(({ content, id, link }) => (
                <Tab key={id} content={t(content)} link={link} />
            ))}
        </div>
    );
}

Tab.propTypes = {
    content: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    namespace: PropTypes.string.isRequired,
};

NavTabs.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        content: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
        link: PropTypes.string.isRequired,
    })).isRequired,
};
