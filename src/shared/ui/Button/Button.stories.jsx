import { SearchIcon } from '../../assets/icons/svg';
import { Button } from './Button';

export default {
    tags: ['autodocs'],
    component: Button,
    argTypes: {
        leftIcon: {
            description: 'Left icon inside button',
        },
        rightIcon: {
            description: 'Right icon inside button',
        },
        disabled: {
            type: 'boolean',
            description: 'Makes the element not mutable, focusable',
        },
        size: {
            control: { type: 'select' },
            defaultValue: { summary: 'big' },
            options: ['big', 'medium', 'small'],
            description: 'Size of button',
        },
        variant: {
            control: { type: 'select' },
            defaultValue: { summary: 'big' },
            options: ['primary', 'secondary', 'tertiary'],
            description: 'Button size',
        },
        className: {
            type: 'string',
        },
    },
    args: {
        size: 'big',
        variant: 'primary',
        children: 'Click',
        style: { inlineSize: '160px' },
    },
};

export const Default = {};

export const WithLeftIcon = {
    // FIXME: icon not loading
    leftIcon: <SearchIcon />,
};

export const WithRightIcon = {
    // FIXME: icon not loading
    rightIcon: <SearchIcon />,
};

export const OnlyIcon = {
    // FIXME: icon not loading
    leftIcon: <SearchIcon />,
    children: undefined,
};

export const Disabled = {
    args: {
        disabled: true,
    },
};

export const Big = {
    args: {
        size: 'big',
    },
};

export const Medium = {
    args: {
        size: 'medium',
    },
};

export const Small = {
    args: {
        size: 'small',
    },
};

export const Primary = {
    args: {
        variant: 'primary',
    },
};

export const Secondary = {
    args: {
        variant: 'secondary',
    },
};

export const Tertiary = {
    args: {
        variant: 'Tertiary',
    },
};
