/* eslint eslint-comments/no-use: off */
/* eslint-disable react/button-has-type */
import { Children } from 'react';

import PropTypes from 'prop-types';

import { cn } from '@/shared/lib';

import styles from './Button.module.css';

export function Button({
    className,
    children,
    leftIcon,
    rightIcon,
    size,
    variant,
    ...props
}) {
    const hasIcon = leftIcon || rightIcon;

    return (
        <button
            className={cn(styles.basis, {
                [styles.big]: size === 'big',
                [styles.medium]: size === 'medium',
                [styles.small]: size === 'small',
                [styles.onlyIcon]: hasIcon && Children.count(children) === 0,
                [styles.primary]: variant === 'primary',
                [styles.secondary]: variant === 'secondary',
                [styles.tertiary]: variant === 'tertiary',
            }, className)}
            {...props}
        >
            {leftIcon}
            {children}
            {rightIcon}
        </button>
    );
}

Button.propTypes = {
    className: PropTypes.string,
    leftIcon: PropTypes.element,
    rightIcon: PropTypes.element,
    size: PropTypes.oneOf(['big', 'medium', 'small']),
    type: PropTypes.oneOf(['submit', 'button', 'reset']),
    variant: PropTypes.oneOf(['primary', 'secondary', 'tertiary']),
};

Button.defaultProps = {
    className: undefined,
    leftIcon: undefined,
    rightIcon: undefined,
    size: 'big',
    type: 'button',
    variant: 'primary',
};
