import { forwardRef } from 'react';

import { Switch as AntdSwitch } from 'antd';

import { cn } from '@/shared/lib';

export const Switch = forwardRef((props, ref) => {
    const { className, ...restProps } = props;

    /** @todo: посмотреть как правильно прокидывать реф */
    return <AntdSwitch {...restProps} ref={ref} className={cn('custom-switch', className)} />;
});

Switch.displayName = 'Switch';
