import { Input } from './Input';

export default {
    tags: ['autodocs'],
    component: Input,
    argTypes: {
        label: {
            type: 'string',
            description: 'Label for input',
        },
        length: {
            type: 'number',
            defaultValue: { summary: 0 },
            description: 'Current length of input value',
        },
        maxLength: {
            type: 'number',
            defaultValue: { summary: 0 },
            description: 'Max length of input value',
        },
        showLength: {
            type: 'boolean',
            defaultValue: { summary: true },
            description: 'Whether to show information about input value length',
        },
        error: {
            type: 'string',
            description: 'Error text if input is invalid',
        },
        disabled: {
            type: 'boolean',
            description: 'Makes the element not mutable, focusable',
        },
        className: {
            type: 'string',
        },
    },
    args: {
        length: 0,
        maxLength: 0,
        showLength: true,
    },
};

export const Default = {};

export const WithLabel = {
    args: {
        label: 'Name',
    },
};

export const WithPlaceholder = {
    args: {
        placeholder: 'Placeholder',
    },
};

export const WithoutLength = {
    args: {
        showLength: false,
    },
};

export const Invalid = {
    args: {
        error: 'Value must not contain any special character',
    },
};

export const Disabled = {
    args: {
        disabled: true,
    },
};
