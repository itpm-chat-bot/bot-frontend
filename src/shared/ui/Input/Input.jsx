import { forwardRef } from 'react';

import PropTypes from 'prop-types';

import { cn } from '@/shared/lib';

import styles from './Input.module.css';

export const Input = forwardRef((props, ref) => {
    const {
        className,
        label,
        length,
        maxLength,
        error,
        showLength,
        ...restProps
    } = props;
    const isError = Boolean(error);

    return (
        <div className={cn(styles.container, {
            [styles.invalid]: isError,
        })}
        >
            <label className={styles.label}>
                {label}
                <input
                    {...restProps}
                    ref={ref}
                    aria-invalid={isError.toString()}
                    className={cn(styles.input, className)}
                />
            </label>

            <span className={styles.hint}>
                {!isError && showLength ? `${length}/${maxLength}` : error}
            </span>
        </div>
    );
});

Input.displayName = 'Input';

/* eslint eslint-comments/no-use: off */
Input.propTypes = {
    label: PropTypes.string,
    length: PropTypes.number,
    maxLength: PropTypes.number,
    error: PropTypes.string,
    showLength: PropTypes.bool,
};

Input.defaultProps = {
    label: undefined,
    length: 0,
    maxLength: 0,
    error: undefined,
    showLength: true,
};
