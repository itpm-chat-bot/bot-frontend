import { cn } from '@/shared/lib';

import styles from './ErrorText.module.css';

export function ErrorText({ className, ...restProps }) {
    return <p {...restProps} className={cn(styles.text, className)} />;
}

ErrorText.displayName = 'ErrorText';
