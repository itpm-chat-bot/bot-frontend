import { useTranslation } from 'react-i18next';

import robot from '../../assets/robot.png';

import styles from './SmallScreenFallback.module.css';

export function SmallScreenFallback() {
    const { t } = useTranslation('common');

    const titleParts = t('fallbacks.smallScreen.title').split('$');
    const description = t('fallbacks.smallScreen.description');

    return (
        <section className={styles.section}>
            <h1>{titleParts[0]}</h1>

            <hgroup>
                <h4>{titleParts[1]}</h4>
                <h4>{titleParts[2]}</h4>
            </hgroup>

            <img alt='' src={robot} />

            <small>{description}</small>
        </section>
    );
}
