import PropTypes from 'prop-types';

import styles from './CommonHead.module.css';

export function CommonHead({ title, subtitle, ...props }) {
    return (
        <section className={styles.container} {...props}>
            <h1 className={styles.title}>{title}</h1>

            {subtitle && (
                <p className={styles.subtitle}>{subtitle}</p>
            )}
        </section>
    );
}

CommonHead.propTypes = {
    title: PropTypes.node.isRequired,
    subtitle: PropTypes.string,
};

CommonHead.defaultProps = {
    subtitle: null,
};
