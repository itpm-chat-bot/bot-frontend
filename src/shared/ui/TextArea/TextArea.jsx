import { forwardRef } from 'react';

import PropTypes from 'prop-types';

import { cn } from '@/shared/lib';

import styles from './TextArea.module.css';

export const TextArea = forwardRef((props, ref) => {
    const {
        className,
        label,
        length,
        maxLength,
        error,
        showLength,
        ...restProps
    } = props;
    const isError = Boolean(error);

    return (
        <div className={cn(styles.container, {
            [styles.invalid]: isError,
        })}
        >
            <label className={styles.label}>
                {label}
                <textarea
                    {...restProps}
                    ref={ref}
                    aria-invalid={isError.toString()}
                    className={cn(styles.textarea, className)}
                />
            </label>

            <span className={styles.hint}>
                {!isError && showLength ? `${length}/${maxLength}` : error}
            </span>
        </div>
    );
});

TextArea.displayName = 'TextArea';

/* eslint eslint-comments/no-use: off */
TextArea.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    length: PropTypes.number,
    maxLength: PropTypes.number,
    error: PropTypes.string,
    showLength: PropTypes.bool,
};

TextArea.defaultProps = {
    label: undefined,
    length: 0,
    maxLength: 0,
    error: undefined,
    showLength: true,
};
