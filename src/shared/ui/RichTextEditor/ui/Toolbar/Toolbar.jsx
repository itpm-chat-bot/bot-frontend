import { useTranslation } from 'react-i18next';

import PropTypes from 'prop-types';

import { SentimentSatisfiedIcon, BlurIcon } from '../../../../assets/icons/svg';
import { validateFormatsProp } from '../../util.js';

import styles from './Toolbar.module.css';

export function Toolbar({ formats, fileBtn }) {
    const { t } = useTranslation('common');

    const hasBold = formats.includes('bold');
    const hasItalic = formats.includes('italic');
    const hasStrikethrough = formats.includes('strikethrough');
    const hasUnderline = formats.includes('underline');
    const hasSpoiler = formats.includes('spoiler');
    const hasLink = formats.includes('link');
    const hasEmojiPalette = formats.includes('emoji');
    const hasClean = formats.includes('clean');

    const hasFileBtn = !!fileBtn;

    return (
        <div className={styles.customToolbar} id='editor-toolbar'>
            {hasBold && (
                <button aria-label='Bold' className='ql-bold' title={t('titles.editor.bold')} type='button' />
            )}
            {hasItalic && (
                <button aria-label='Italic' className='ql-italic' title={t('titles.editor.italic')} type='button' />
            )}
            {hasStrikethrough && (
                <button aria-label='Strikethrough' className='ql-strike' title={t('titles.editor.strikethrough')} type='button' />
            )}
            {hasUnderline && (
                <button aria-label='Underline' className='ql-underline' title={t('titles.editor.underline')} type='button' />
            )}
            {hasSpoiler && (
                <button aria-label='Spoiler text' className='ql-spoiler' title={t('titles.editor.spoiler')} type='button'>
                    <BlurIcon />
                </button>
            )}
            {hasLink && (
                <button aria-label='Link' className='ql-link' title={t('titles.editor.insertLink')} type='button' />
            )}
            {hasEmojiPalette && (
                <button aria-label='Emoji' className='ql-emoji' title={t('titles.editor.insertEmoji')} type='button'>
                    <SentimentSatisfiedIcon />
                </button>
            )}
            {hasFileBtn && fileBtn}
            {hasClean && (
                <button aria-label='Clean' className='ql-clean' title={t('titles.editor.resetEditing')} type='button' />
            )}
        </div>
    );
}

// TODO: validateFormatsProp() does not validate props
Toolbar.propTypes = {
    formats: PropTypes.arrayOf(validateFormatsProp),
    fileBtn: PropTypes.element,
};

Toolbar.defaultProps = {
    formats: [],
    fileBtn: null,
};
