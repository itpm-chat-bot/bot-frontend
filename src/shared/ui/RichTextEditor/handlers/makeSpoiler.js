// Example of handler and explanation (https://quilljs.com/docs/modules/toolbar/, at the end of page)
export function makeSpoiler(value) {
    if (value) {
        this.quill.format('spoiler', true);
        this.quill.format('background', 'gray');
    } else {
        this.quill.format('spoiler', false);
        this.quill.format('background', false);
    }
}
