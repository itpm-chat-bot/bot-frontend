import { Quill } from 'react-quill';

const Inline = Quill.import('blots/inline');

export class SpoilerBlot extends Inline {}

SpoilerBlot.blotName = 'spoiler'; // name of future format
SpoilerBlot.tagName = 'tg-spoiler';
