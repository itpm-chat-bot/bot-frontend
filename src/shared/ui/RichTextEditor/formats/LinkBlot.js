import { Quill } from 'react-quill';

import { rUrl } from '../../../constants/regexp.js';
import { MAX_URL_LENGTH } from '../../../constants/urls.js';

const Inline = Quill.import('blots/inline');

export class LinkBlot extends Inline {
    static FALLBACK_URL = [import.meta.env.REACT_APP_API_BASE_URL, 'invalid-link'].join('/');

    static validate(url) {
        const isValid = url.match(rUrl) && url.length <= MAX_URL_LENGTH;
        return isValid ? url : this.FALLBACK_URL;
    }

    static create(value) {
        const node = super.create(value);
        const validatedUrl = this.validate(value);
        node.setAttribute('href', validatedUrl);
        node.setAttribute('title', validatedUrl);
        node.setAttribute('rel', 'noopener noreferrer');
        node.setAttribute('target', '_blank');
        return node;
    }

    static formats(node) {
        return node.getAttribute('href');
    }
}

LinkBlot.blotName = 'link';
LinkBlot.tagName = 'a';
