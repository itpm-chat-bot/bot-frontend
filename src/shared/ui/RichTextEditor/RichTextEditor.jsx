import { forwardRef, useEffect, useRef } from 'react';
import ReactQuill, { Quill } from 'react-quill';

import PropTypes from 'prop-types';
import * as Emoji from 'quill-emoji';
import { renderToStaticMarkup } from 'react-dom/server';

import { FormatBoldIcon, FormatItalicIcon, FormatUnderlinedIcon, StrikethroughIcon, SentimentSatisfiedIcon, LinkIcon, InkEraserIcon } from '../../assets/icons/svg';
import { cn, useEditor } from '../../lib';
import { LinkBlot } from './formats/LinkBlot.js';
import { SpoilerBlot } from './formats/SpoilerBlot.js';
import { makeSpoiler } from './handlers/makeSpoiler.js';
import { Lengther } from './modules/Lengther.js';
import { Toolbar } from './ui/Toolbar/Toolbar.jsx';
import { validateFormatsProp } from './util.js';

import 'quill-emoji/dist/quill-emoji.css';
import 'react-quill/dist/quill.snow.css';

import styles from './RichTextEditor.module.css';

Quill.register('modules/emoji', Emoji);
const emojiIcon = renderToStaticMarkup(<SentimentSatisfiedIcon />);
Quill.register('modules/lengther', Lengther);
Quill.register('formats/link', LinkBlot);
Quill.register('formats/spoiler', SpoilerBlot);

// That way it is replaced default icons with custom ones https://github.com/quilljs/quill/issues/1099
const icons = Quill.import('ui/icons');
icons.bold = renderToStaticMarkup(<FormatBoldIcon />);
icons.italic = renderToStaticMarkup(<FormatItalicIcon />);
icons.strike = renderToStaticMarkup(<StrikethroughIcon />);
icons.underline = renderToStaticMarkup(<FormatUnderlinedIcon />);
icons.link = renderToStaticMarkup(<LinkIcon />);
icons.clean = renderToStaticMarkup(<InkEraserIcon />);

export const RichTextEditor = forwardRef((props, ref) => {
    const { name, value, onChange, onBlur, onKeyDown } = props;
    const { toolbarFormats, fileBtn, error, length, maxLength = 4096 } = props; // specific props
    const { showLength, shouldBeFocusedInitially, shouldBeFocusedAfterSubmit, isSubmitSuccessful } = props; // specific props
    const { theme, placeholder, style } = props; // Quill props
    const { setEditorInstance } = useEditor();

    const editorRef = useRef(null);

    const isError = Boolean(error);

    // Write ref target to context provider
    useEffect(() => {
        setEditorInstance(editorRef.current.getEditor());
    }, [setEditorInstance]);

    useEffect(() => {
        if (shouldBeFocusedInitially) {
            editorRef.current.focus();
        }
    }, [shouldBeFocusedInitially]);

    // Focus textarea after successful submit and scroll to the top of the page
    useEffect(() => {
        if (isSubmitSuccessful) {
            if (shouldBeFocusedAfterSubmit) {
                editorRef.current.focus();
            }

            window.scrollTo(0, 0);
        }
    }, [isSubmitSuccessful, shouldBeFocusedAfterSubmit]);

    return (
        <div className={cn(styles.container, {
            [styles.invalid]: isError,
        })}
        >
            <div aria-invalid={isError} className={styles.textEditor}>
                <Toolbar fileBtn={fileBtn} formats={toolbarFormats} />
                <ReactQuill
                    ref={(e) => {
                        ref(e);
                        editorRef.current = e;
                    }}
                    className={styles.customQuill}
                    modules={RichTextEditor.modules}
                    name={name}
                    placeholder={placeholder}
                    style={style}
                    theme={theme}
                    value={value}
                    onBlur={onBlur}
                    onChange={onChange}
                    onKeyDown={onKeyDown}
                />
            </div>

            <span className={styles.hint}>
                {!isError && showLength ? `${length}/${maxLength}` : error}
            </span>
        </div>
    );
});

RichTextEditor.modules = {
    toolbar: {
        container: '#editor-toolbar',
        handlers: {
            spoiler: makeSpoiler,
        },
    },
    clipboard: {
        matchVisual: false,
    },
    'emoji-toolbar': {
        buttonIcon: emojiIcon,
    },
    'emoji-textarea': false,
    'emoji-shortname': true,
};

RichTextEditor.displayName = 'RichTextEditor';

RichTextEditor.propTypes = {
    length: PropTypes.number,
    maxLength: PropTypes.number,
    fileBtn: PropTypes.element,
    toolbarFormats: PropTypes.arrayOf(validateFormatsProp),
    showLength: PropTypes.bool,
    shouldBeFocusedInitially: PropTypes.bool,
    shouldBeFocusedAfterSubmit: PropTypes.bool,
    error: PropTypes.string,
    isSubmitSuccessful: PropTypes.bool,
    theme: PropTypes.string,
    placeholder: PropTypes.string,
    style: PropTypes.shape({
        blockSize: PropTypes.string,
    }),
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
};

RichTextEditor.defaultProps = {
    length: 0,
    maxLength: 0,
    fileBtn: null,
    toolbarFormats: [],
    shouldBeFocusedInitially: false,
    shouldBeFocusedAfterSubmit: false,
    error: undefined,
    isSubmitSuccessful: false,
    showLength: true,
    theme: 'snow',
    placeholder: '',
    style: {},
    onChange: () => { },
    onBlur: () => { },
};
