export const allowedFormats = new Set(['bold', 'italic', 'strikethrough', 'underline', 'spoiler', 'link', 'emoji', 'clean']);

export function getExtensions(type) {
    switch (type) {
        case 'image':
            return ['.jpg', '.jpeg', '.png', '.gif', '.heic'].join(', ');
        case 'tgAcceptedVideo':
            return ['.mp4'].join(', ');
        case 'video':
            return ['.mov', '.avi', '.wmv', '.webm'].join(', ');
        case 'audio':
            return ['.mp3'].join(', ');
        case 'text':
            return ['.docx', '.doc', '.xlsx', '.txt', '.fb2'].join(', ');
        case 'application':
            return ['.pdf', '.mobi', '.zip', '.rar', '.epub', '.pptx'].join(', ');
        default:
            throw new Error(`Given filename extension "${type}" is not recognized`);
    }
}

/* eslint eslint-comments/no-use: off */
// eslint-disable-next-line consistent-return
export function validateFormatsProp(propValue, key, componentName, _location, propFullName) {
    if (!allowedFormats.has(propValue[key])) {
        return new Error(`Invalid prop ${propFullName} supplied to ${componentName}. Validation failed.`);
    }
}
