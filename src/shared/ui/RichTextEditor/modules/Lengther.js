export class Lengther {
    constructor(quill, options) {
        this.quill = quill;
        this.options = options;
        this.container = document.querySelector(options.container);
        quill.on('text-change', this.update.bind(this));
        this.update();
    }

    calculate() {
        // Subtract 1 because one blank line always exist in editor (https://quilljs.com/docs/api/#getlength)
        return this.quill.getLength() - 1;
    }

    update() {
        const length = this.calculate();
        const elementType = this.container.nodeName.toLowerCase();

        // Save length into attr "value" if <data /> is the length container
        if (elementType === 'data') {
            this.container.value = length;
        }

        // Save length into textContent
        this.container.textContent = length;
    }
}
