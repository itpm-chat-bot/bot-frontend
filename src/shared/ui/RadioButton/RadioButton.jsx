import { forwardRef } from 'react';

import PropTypes from 'prop-types';

import { cn } from '@/shared/lib';

import styles from './RadioButton.module.css';

export const RadioButton = forwardRef((props, ref) => {
    const { className, label, ...restProps } = props;

    return (
        <label className={styles.label}>
            <input
                {...restProps}
                ref={ref}
                className={cn(styles.input, className)}
                type='radio'
            />
            {label}
        </label>
    );
});

RadioButton.displayName = 'RadioButton';

/* eslint eslint-comments/no-use: off */
RadioButton.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
};
