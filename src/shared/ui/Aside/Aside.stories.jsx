import { Container } from '../Container/Container';
import { Main } from '../Main/Main';
import { Aside, AsideContent } from './Aside';

export default {
    tags: ['autodocs'],
    component: Aside,
    argTypes: {
        className: {
            type: 'string',
        },
    },
    args: {
        children: <AsideContent>Some content into aside</AsideContent>,
        style: {
            background: 'red',
        },
    },
    render: (args) => (
        <Main>
            <Container />
            <Aside {...args} />
        </Main>
    ),
};

export const Default = {};
