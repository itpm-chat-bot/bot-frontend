import PropTypes from 'prop-types';

import { cn } from '@/shared/lib';

import styles from './Aside.module.css';

export function Aside({ className, ...props }) {
    return <aside className={cn(styles.container, className)} {...props} />;
}

Aside.propTypes = {
    children: PropTypes.node,
};

Aside.defaultProps = {
    children: undefined,
};

export function AsideContent({ className, ...props }) {
    return <div className={cn(styles.content, className)} {...props} />;
}

AsideContent.propTypes = {
    children: PropTypes.node,
};

AsideContent.defaultProps = {
    children: undefined,
};
