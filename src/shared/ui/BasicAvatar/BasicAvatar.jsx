import PropTypes from 'prop-types';

import { cn } from '@/shared/lib';

import styles from './BasicAvatar.module.css';

export function BasicAvatar({ alt, disabled, backgroundColor, initials, size, src, ...props }) {
    return (
        <div
            className={cn(styles.basis, {
                [styles.big]: size === 'big',
                [styles.medium]: size === 'medium',
                [styles.small]: size === 'small',
                [styles.fallback]: !src,
                [styles.disabled]: disabled,
            })}
            style={{
                backgroundColor: !disabled && backgroundColor,
            }}
            {...props}
        >
            {src
                ? <img alt={alt} src={src} />
                : <span>{initials}</span>}
        </div>
    );
}

BasicAvatar.propTypes = {
    alt: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    backgroundColor: PropTypes.string,
    initials: PropTypes.string.isRequired,
    size: PropTypes.oneOf(['big', 'medium', 'small']),
    src: PropTypes.string,
};

BasicAvatar.defaultProps = {
    disabled: false,
    backgroundColor: '#4D90F1',
    size: 'big',
    src: undefined,
};
