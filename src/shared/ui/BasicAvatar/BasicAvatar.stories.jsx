import { BasicAvatar } from './BasicAvatar';

export default {
    tags: ['autodocs'],
    component: BasicAvatar,
    argTypes: {
        alt: {
            description: 'Alternative information for an image if a user for some reason cannot view it(because of slow connection, an error in the src attribute, or if the user uses a screen reader).',
        },
        disabled: {
            defaultValue: { summary: false },
            description: 'Makes the element not mutable, focusable',
        },
        backgroundColor: {
            defaultValue: { summary: '#4D90F1' },
            description: 'Background color for avatar if img source is not passed',
        },
        initials: {
            description: 'Fallback text for avatar if img source is not passed',
        },
        size: {
            defaultValue: { summary: 'big' },
            options: ['big', 'medium', 'small'],
            description: 'Avatar size',
        },
        src: {
            description: 'Image source for avatar',
        },
        className: {
            type: 'string',
        },
    },
    args: {
        alt: 'Alt text',
        disabled: false,
        backgroundColor: '#4D90F1',
        initials: 'Frank Grillo',
        size: 'big',
        src: null,
    },
};

export const Default = {};

export const WithImage = {
    // FIXME: image not loading
    src: '/android-chrome-512x512.png',
};

export const Disabled = {
    args: {
        disabled: true,
    },
};

export const DisabledWithImage = {
    args: {
        disabled: true,
        // FIXME: image not loading
        src: '',
    },
};

export const Big = {
    args: {
        size: 'big',
    },
};

export const BigWithImage = {
    args: {
        size: 'big',
        // FIXME: image not loading
        src: '',
    },
};

export const Medium = {
    args: {
        size: 'medium',
    },
};

export const MediumWithImage = {
    args: {
        size: 'medium',
        // FIXME: image not loading
        src: '',
    },
};

export const Small = {
    args: {
        size: 'small',
    },
};

export const SmalllWithImage = {
    args: {
        size: 'small',
        // FIXME: image not loading
        src: '',
    },
};
