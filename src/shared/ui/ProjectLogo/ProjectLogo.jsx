import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import Logo from '../../assets/logo.svg';

import styles from './ProjectLogo.module.css';

export function ProjectLogo() {
    const { t } = useTranslation('common');

    return (
        <Link className={styles.link} to='/'>
            <Logo />
            <span>{t('labels.logo')}</span>
        </Link>
    );
}
