import { CancelIcon, DoneIcon } from '../assets/icons/svg';

export const toasterConfig = {
    position: 'bottom-center',
    toastOptions: {
        style: {
            flexDirection: 'row-reverse',
            padding: '1rem 2rem',
            backgroundColor: 'var(--clr-second-text)',
            color: 'var(--clr-white)',
        },
        success: {
            icon: <DoneIcon color='var(--clr-green)' style={{ minInlineSize: 'fit-content' }} />,
        },
        error: {
            icon: <CancelIcon color='var(--clr-red)' style={{ minInlineSize: 'fit-content' }} />,
        },
    },
};
