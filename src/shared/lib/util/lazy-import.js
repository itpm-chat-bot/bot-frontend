import { lazy } from 'react';

/**
 * Lazy imports a non-default exported component from a load function and returns a proxy object.
 * Usage:
 * ```
 * const { Home } = lazyImport(() => import("./Home"), "Home");
 * ```
 * @param {() => Promise<I>} load - The load function that returns a promise of a module object.
 * @param {K} name - The name of the component.
 * @template T - The base component type.
 * @template I - The interface that contains component types.
 * @template K - The key type that references the component name in the interface.
 * @returns  A proxy object containing the lazy imported component.
 */

export function lazyImport(load, name) {
    const LazyComponent = lazy(() => load().then((module) => ({
        default: module[name],
    })));

    return { [name]: LazyComponent };
}
