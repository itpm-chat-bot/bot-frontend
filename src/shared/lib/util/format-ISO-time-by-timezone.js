import dayjs from 'dayjs';

export const formatISOtimeByTimezone = (ISOtime, timezone, format) => dayjs(ISOtime).tz(timezone).format(format);
