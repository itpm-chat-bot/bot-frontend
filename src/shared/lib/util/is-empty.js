export function isEmpty(value) {
    if (typeof value === 'undefined' || typeof value === 'number' || typeof value === 'boolean' || typeof value === 'function') return true;

    if (typeof value === 'string') return value.length === 0;

    if (typeof value === 'object') {
        if (value === null) return true;
        if (Array.isArray(value)) return value.length === 0;

        return Object.keys(value).length === 0;
    }

    return false;
}
