/* eslint no-bitwise: ["error", { "allow": ["<<", "&="] }] */

import { rOneOrMoreSpaces, rAnyFirstLetterOrNumber } from '../../constants/regexp.js';

export function sleep(msTime) {
    return new Promise((resolve) => {
        setTimeout(() => resolve(), msTime);
    });
}

export function getContentFromHTML(htmlString) {
    const parser = new DOMParser();
    const doc = parser.parseFromString(htmlString, 'text/html');
    const content = doc.body.textContent;
    return content;
}

export function getAntdMenuItem(label, key, icon, children, disabled, type) {
    return { label, key, icon, children, disabled, type };
}

export function generateHashFromString(str) {
    const offsetNumOfBits = 5;

    // Generate a unique number (hash) based on the string
    let hash = 0;
    for (let i = 0; i < str.length; i += 1) {
        const charCode = str.charCodeAt(i);
        hash = (hash << offsetNumOfBits) - hash + charCode;
        hash &= hash; // Convert to 32-bit integer
    }

    return hash;
}

export function generateIndexFromHash(hash, min = 0, max = hash) {
    const range = max - min + 1;
    const index = (Math.abs(hash) % range) + min;
    return index;
}

export function matchString(str, options) {
    return str.match(options.regex)?.at(0) ?? '';
}

export function getInitials(fullname) {
    const defaultValue = '';

    if (typeof fullname !== 'string') {
        return defaultValue;
    }

    const words = fullname.split(rOneOrMoreSpaces);
    const firstMatchedChars = words
        .map((word) => matchString(word, { regex: rAnyFirstLetterOrNumber }))
        .filter((char) => !!char)
        .map((char) => char.toUpperCase());
    const charsCount = firstMatchedChars.length;

    if (charsCount >= 2) return `${firstMatchedChars.at(0)}${firstMatchedChars.at(-1)}`;
    if (charsCount === 1) return firstMatchedChars.at(0);
    return defaultValue;
}
