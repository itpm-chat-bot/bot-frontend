import { createContext } from 'react';

// createContext() takes defaultValue that the context will have when there is no matching
// context provider in the tree above the component that reads context
export const EditorContext = createContext({
    editorInstance: null,
    setEditorInstance: () => console.warn('Editor provider is not found in the app providers tree'),
});
