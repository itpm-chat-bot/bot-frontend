import { createContext } from 'react';

// createContext() takes defaultValue that the context will have when there is no matching
// context provider in the tree above the component that reads context
export const AuthContext = createContext({
    loggedIn: false,
    logIn: () => console.warn('Authorization provider is not found in the app providers tree'),
    logOut: () => console.warn('Authorization provider is not found in the app providers tree'),
});
