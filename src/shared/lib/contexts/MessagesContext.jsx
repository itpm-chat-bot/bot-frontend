import { createContext } from 'react';

// createContext() takes defaultValue that the context will have when there is no matching
// context provider in the tree above the component that reads context
export const MessagesContext = createContext({
    reducerName: null,
    matchScheduledMessagesRoutePath: false,
    matchMessagesHistoryRoutePath: false,
    selectors: {},
});
