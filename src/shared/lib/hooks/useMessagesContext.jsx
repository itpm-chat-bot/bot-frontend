import { useContext } from 'react';

import { MessagesContext } from '../contexts';

export const useMessagesContext = () => useContext(MessagesContext);
