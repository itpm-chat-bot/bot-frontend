import { useMediaQuery } from 'react-responsive';

import { breakpoints } from '../../constants/breakpoints.js';

export const useTabletMediaQuery = () => useMediaQuery({ query: `(max-width: ${breakpoints.md})` });
