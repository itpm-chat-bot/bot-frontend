import { useContext } from 'react';

import { EditorContext } from '../contexts';

export const useEditor = () => useContext(EditorContext);
