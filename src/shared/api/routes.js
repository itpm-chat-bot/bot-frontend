const apiPath = '/api';

// TODO: refactor long paths like scheduledMessagesByChatIdPath
export const routes = {
    loginPath: () => [apiPath, 'login'].join('/').concat('/'),
    tokenRefreshPath: () => [apiPath, 'token-refresh'].join('/').concat('/'),
    myProfilePath: () => [apiPath, 'profiles', 'me'].join('/').concat('/'),
    chatsPath: () => [apiPath, 'chats'].join('/').concat('/'),
    chatPath: (chatId) => [apiPath, 'chats', chatId].join('/').concat('/'),
    chatUsersCount: (tgChatId) => [apiPath, 'chats', tgChatId, 'users_count'].join('/').concat('/'),
    chatAgeCount: (tgChatId) => [apiPath, 'chats', tgChatId, 'age_count'].join('/').concat('/'),
    updateChatTimezonePath: (chatId) => [apiPath, 'chats', chatId, 'update_timezone'].join('/').concat('/'),
    updateServiceMessageSettingsPath: (chatId) => [apiPath, 'chats', chatId, 'update_service_messages'].join('/').concat('/'),
    messagesPath: () => [apiPath, 'messages'].join('/').concat('/'),
    messagePath: (messageId) => [apiPath, 'messages', messageId].join('/').concat('/'),
    messageFilesPath: () => [apiPath, 'messages', 'files'].join('/').concat('/'),
    messageFilesByIdPath: (fileId) => [apiPath, 'messages', 'files', fileId].join('/').concat('/'),
    scheduledMessagesPath: () => [apiPath, 'messages'].join('/').concat('/?').concat('delayed=true'),
    scheduledMessagesByChatIdPath: (chatId) => [apiPath, 'messages'].join('/').concat('/?').concat(`chat_id=${chatId}&delayed=true`),
    sentMessagesByChatIdPath: (chatId) => [apiPath, 'messages'].join('/').concat('/?').concat(`chat_id=${chatId}&sent=true`),
    expertsPath: () => [apiPath, 'experts'].join('/').concat('/'),
    expertPath: (expertId) => [apiPath, 'experts', expertId].join('/').concat('/'),
    faqsPath: () => [apiPath, 'faq'].join('/').concat('/'),
    faqPath: (faqId) => [apiPath, 'faq', faqId].join('/').concat('/'),
    commandsPath: () => [apiPath, 'commands'].join('/').concat('/'),
    commandPath: (commandId) => [apiPath, 'commands', commandId].join('/').concat('/'),
    participantPath: (participantId) => [apiPath, 'participants', participantId].join('/').concat('/'),
    participantsPath: () => [apiPath, 'participants'].join('/').concat('/'),
};
