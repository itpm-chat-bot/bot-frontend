import { LOADING_STATUSES } from '../constants/statuses.js';

export const isLoadingStatusSelector = (state) => [
    ...Object.values(state.myProfile.loadingStatus),
    ...Object.values(state.chats.loadingStatus),
    ...Object.values(state.selectedChat.loadingStatus),
    ...Object.values(state.usersCount.loadingStatus),
    ...Object.values(state.ageCount.loadingStatus),
    ...Object.values(state.scheduledMessages.loadingStatus),
    ...Object.values(state.sentMessages.loadingStatus),
    ...Object.values(state.currentMessage.loadingStatus),
    ...Object.values(state.experts.loadingStatus),
    ...Object.values(state.commands.loadingStatus),
    state.messages.loadingStatus,
].includes(LOADING_STATUSES.LOADING);
