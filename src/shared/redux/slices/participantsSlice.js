import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

export const fetchParticipants = createAsyncThunk(
    'Participants/getParticipants',
    async ({ chatId, sortBy }) => {
        const res = await httpClient.get(routes.participantsPath(), { params: { chat: chatId, sort_by_rating: sortBy } });
        return camelcaseKeys(res.data);
    },
);

export const updateParticipant = createAsyncThunk(
    'Participants/updateParticipant',
    async ({ participantId, participantData }) => {
        const res = await httpClient.patch(routes.participantPath(participantId), participantData);
        return camelcaseKeys(res.data);
    },
);

const participantsAdapter = createEntityAdapter();

const initialState = participantsAdapter.getInitialState({
    loadingStatus: {
        fetchParticipantsReqStatus: LOADING_STATUSES.IDLE,
        updateParticipantReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchParticipantsRe: null,
        updateParticipantReq: null,
    },
});

const participantsSlice = createSlice({
    name: 'participantsSlice',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchParticipants.pending, (state) => {
                state.loadingStatus.fetchParticipantsReq = LOADING_STATUSES.LOADING;
                state.errors.fetchParticipantsReq = null;
            })
            .addCase(fetchParticipants.fulfilled, (state, action) => {
                participantsAdapter.setAll(state, action.payload);
                state.loadingStatus.fetchParticipantsReq = LOADING_STATUSES.IDLE;
            })
            .addCase(fetchParticipants.rejected, (state, action) => {
                state.loadingStatus.fetchParticipantsReq = LOADING_STATUSES.FAILED;
                state.errors.fetchParticipantsReq = action.error;
            })
            .addCase(updateParticipant.fulfilled, (state, action) => {
                participantsAdapter.updateOne(state, action);
                state.loadingStatus.updateParticipantReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(updateParticipant.rejected, (state, action) => {
                state.loadingStatus.updateParticipantReq = LOADING_STATUSES.FAILED;
                state.errors.updateParticipantReq = action.error;
            });
    },
});

export const participantsSelectors = participantsAdapter.getSelectors((state) => state.participants);
export const participantsActions = participantsSlice.actions;
export const participantsReducer = participantsSlice.reducer;
