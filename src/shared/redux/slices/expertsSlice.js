import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

export const fetchExperts = createAsyncThunk(
    'experts/getExperts',
    async (chatId) => {
        const res = await httpClient.get(routes.expertsPath(), { params: { chat: chatId } });
        return camelcaseKeys(res.data);
    },
);

export const createExpert = createAsyncThunk(
    'experts/createExpert',
    async ({ expertData }) => {
        const res = await httpClient.post(routes.expertsPath(), expertData);
        return camelcaseKeys(res.data);
    },
);

export const updateExpert = createAsyncThunk(
    'experts/updateExpert',
    async ({ expertId, expertData }) => {
        const res = await httpClient.put(routes.expertPath(expertId), expertData);
        return camelcaseKeys(res.data);
    },
);

export const deleteExpert = createAsyncThunk(
    'experts/deleteExpert',
    async (expertId) => {
        await httpClient.delete(routes.expertPath(expertId));
        return expertId;
    },
);

const expertsAdapter = createEntityAdapter();

const initialState = expertsAdapter.getInitialState({
    loadingStatus: {
        fetchExpertsReqStatus: LOADING_STATUSES.IDLE,
        createExpertReqStatus: LOADING_STATUSES.IDLE,
        updateExpertReqStatus: LOADING_STATUSES.IDLE,
        deleteExpertReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchExpertsReq: null,
        createExpertReq: null,
        updateExpertReq: null,
        deleteExpertReq: null,
    },
});

const expertsSlice = createSlice({
    name: 'expertsSlice',
    initialState,
    reducers: {
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchExperts.pending, (state) => {
                state.loadingStatus.fetchExpertsReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchExpertsReq = null;
            })
            .addCase(fetchExperts.fulfilled, (state, action) => {
                expertsAdapter.setAll(state, action.payload);
                state.loadingStatus.fetchExpertsReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(fetchExperts.rejected, (state, action) => {
                state.loadingStatus.fetchExpertsReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchExpertsReq = action.error;
            })
            .addCase(createExpert.pending, (state) => {
                state.loadingStatus.createExpertReqStatus = LOADING_STATUSES.LOADING;
                state.errors.createExpertReq = null;
            })
            .addCase(createExpert.fulfilled, (state, action) => {
                expertsAdapter.addOne(state, action.payload);
                state.loadingStatus.createExpertReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(createExpert.rejected, (state, action) => {
                state.loadingStatus.createExpertReq = LOADING_STATUSES.FAILED;
                state.errors.createExpertReq = action.error;
            })
            .addCase(updateExpert.fulfilled, (state, action) => {
                expertsAdapter.updateOne(state, action);
                state.loadingStatus.updateExpertReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(updateExpert.rejected, (state, action) => {
                state.loadingStatus.updateExpertReq = LOADING_STATUSES.FAILED;
                state.errors.updateExpertReq = action.error;
            })
            .addCase(deleteExpert.fulfilled, (state, action) => {
                expertsAdapter.removeOne(state, action);
                state.loadingStatus.deleteExpertReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(deleteExpert.rejected, (state, action) => {
                state.loadingStatus.deleteExpertReq = LOADING_STATUSES.FAILED;
                state.errors.deleteExpertReq = action.error;
            });
    },
});

export const expertsSelectors = expertsAdapter.getSelectors((state) => state.experts);
export const expertsLoadingStatusesSelector = (state) => state.experts.loadingStatus;
export const expertsErrorsSelector = (state) => state.experts.errors;
export const expertsActions = expertsSlice.actions;
export const expertsReducer = expertsSlice.reducer;
