import { createAsyncThunk, createSlice, createEntityAdapter } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';
import { normalize, schema } from 'normalizr';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';
import { isEmpty } from '../../lib';
import { deleteMessageById } from './messagesSlice.js';

export const fetchScheduledMessagesByChatId = createAsyncThunk(
    'scheduledMessages/fetchScheduledMessagesByChatId',
    async (chatId) => {
        const res = await httpClient.get(routes.scheduledMessagesByChatIdPath(chatId));

        if (isEmpty(res.data)) {
            return [];
        }

        const scheduledMessageSchema = new schema.Entity('scheduledMessages');
        const scheduledMessageListSchema = new schema.Array(scheduledMessageSchema);
        const normalizedData = normalize(res.data, scheduledMessageListSchema);

        return camelcaseKeys(normalizedData.entities.scheduledMessages, { deep: true });
    },
);

const scheduledMessagesAdapter = createEntityAdapter();

// For loading status used "fetchMessagesByChatIdReq" instead of thunk "fetchScheduledMessagesByChatIdReq"
// because there is a problem to select certain status via selector in components.
// Don't forget that this slice and sentMessagesSlice serve at MessageList component
const initialState = scheduledMessagesAdapter.getInitialState({
    loadingStatus: {
        fetchMessagesByChatIdReqStatus: LOADING_STATUSES.IDLE,
        deleteMessageByIdReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchMessagesByChatIdReq: null,
        deleteMessageByIdReq: null,
    },
    currentPageNumber: 1,
});

// TODO: think about correct state updating. Perhaps fetching must be connected via adapter addMany, but not setMany
const scheduledMessagesSlice = createSlice({
    name: 'scheduledMessagesSlice',
    initialState,
    reducers: {
        updateCurrentPageNumber: (state, action) => {
            state.currentPageNumber = action.payload;
        },
        resetCurrentPageNumber: (state) => {
            state.currentPageNumber = 1;
        },
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchScheduledMessagesByChatId.pending, (state) => {
                state.loadingStatus.fetchMessagesByChatIdReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchMessagesByChatIdReq = null;
            })
            .addCase(fetchScheduledMessagesByChatId.fulfilled, (state, action) => {
                scheduledMessagesAdapter.setMany(state, action);
                state.loadingStatus.fetchMessagesByChatIdReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.fetchMessagesByChatIdReq = null;
            })
            .addCase(fetchScheduledMessagesByChatId.rejected, (state, action) => {
                state.loadingStatus.fetchMessagesByChatIdReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchMessagesByChatIdReq = action.error;
            })
            .addCase(deleteMessageById.pending, (state) => {
                state.loadingStatus.deleteMessageByIdReqStatus = LOADING_STATUSES.LOADING;
                state.errors.deleteMessageByIdReq = null;
            })
            .addCase(deleteMessageById.fulfilled, (state, action) => {
                scheduledMessagesAdapter.removeOne(state, action);
                state.loadingStatus.deleteMessageByIdReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.deleteMessageByIdReq = null;
            })
            .addCase(deleteMessageById.rejected, (state, action) => {
                state.loadingStatus.deleteMessageByIdReqStatus = LOADING_STATUSES.FAILED;
                state.errors.deleteMessageByIdReq = action.error;
            });
    },
});

export const scheduledMessagesSelectors = scheduledMessagesAdapter.getSelectors((state) => state.scheduledMessages);
export const scheduledMessagesLoadingStatusesSelector = (state) => state.scheduledMessages.loadingStatus;
export const scheduledMessagesErrorsSelector = (state) => state.scheduledMessages.errors;
export const scheduledMessagesActions = scheduledMessagesSlice.actions;
export const scheduledMessagesReducer = scheduledMessagesSlice.reducer;
