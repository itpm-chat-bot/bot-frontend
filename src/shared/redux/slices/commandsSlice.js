import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

// FIXME: transform response data to camelcase notation
export const fetchCommands = createAsyncThunk(
    'commands/getCommands',
    async (chatId) => {
        const res = await httpClient.get(routes.commandsPath(), { params: { chat: chatId } });
        return camelcaseKeys(res.data);
    },
);

export const createCommand = createAsyncThunk(
    'commands/createCommands',
    async ({ commandData }) => {
        const res = await httpClient.post(routes.commandsPath(), commandData);
        return res.data;
    },
);
// FIXME: transform response data to camelcase notation
export const updateCommand = createAsyncThunk(
    'commands/updateCommand',
    async ({ commandId, commandData }) => {
        const res = await httpClient.put(routes.commandPath(commandId), commandData);
        return res.data;
    },
);

export const deleteCommand = createAsyncThunk(
    'commands/deleteCommand',
    async (commandId) => {
        await httpClient.delete(routes.commandPath(commandId));
        return commandId;
    },
);

const commandsAdapter = createEntityAdapter();

const initialState = commandsAdapter.getInitialState({
    loadingStatus: {
        fetchCommandsReqStatus: LOADING_STATUSES.IDLE,
        createCommandReqStatus: LOADING_STATUSES.IDLE,
        updateCommandReqStatus: LOADING_STATUSES.IDLE,
        deleteCommandReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchCommandsReq: null,
        createCommandReq: null,
        updateCommandReq: null,
        deleteCommandReq: null,
    },
});

const commandsSlice = createSlice({
    name: 'commandsSlice',
    initialState,
    reducers: {
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchCommands.pending, (state) => {
                state.loadingStatus.fetchCommandsReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchCommandsReq = null;
            })
            .addCase(fetchCommands.fulfilled, (state, action) => {
                commandsAdapter.setAll(state, action.payload);
                state.loadingStatus.fetchCommandsReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(fetchCommands.rejected, (state, action) => {
                state.loadingStatus.fetchCommandsReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchCommandsReq = action.error;
            })
            .addCase(createCommand.pending, (state) => {
                state.loadingStatus.createCommandReqStatus = LOADING_STATUSES.LOADING;
                state.errors.createCommandReq = null;
            })
            .addCase(createCommand.fulfilled, (state, action) => {
                commandsAdapter.addOne(state, action.payload);
                state.loadingStatus.createCommandReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(createCommand.rejected, (state, action) => {
                state.loadingStatus.createCommandReq = LOADING_STATUSES.FAILED;
                state.errors.createCommandReq = action.error;
            })
            .addCase(updateCommand.fulfilled, (state, action) => {
                commandsAdapter.updateOne(state, action);
                state.loadingStatus.updateCommandReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(updateCommand.rejected, (state, action) => {
                state.loadingStatus.updateCommandReq = LOADING_STATUSES.FAILED;
                state.errors.updateCommandReq = action.error;
            })
            .addCase(deleteCommand.fulfilled, (state, action) => {
                commandsAdapter.removeOne(state, action);
                state.loadingStatus.deleteCommandReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(deleteCommand.rejected, (state, action) => {
                state.loadingStatus.deleteCommandReqStatus = LOADING_STATUSES.FAILED;
                state.errors.deleteCommandReq = action.error;
            });
    },
});

export const commandsSelectors = commandsAdapter.getSelectors((state) => state.commands);
export const commandsLoadingStatusesSelector = (state) => state.commands.loadingStatus;
export const commandsErrorsSelector = (state) => state.commands.errors;
export const commandsActions = commandsSlice.actions;
export const commandsReducer = commandsSlice.reducer;
