import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

export const fetchUsersCount = createAsyncThunk(
    'chats/fetchUsersCount',
    async (tgChatId) => {
        const res = await httpClient.get(routes.chatUsersCount(tgChatId));
        return camelcaseKeys(res.data, { deep: true });
    },
);

const initialState = {
    entity: {},
    loadingStatus: {
        fetchUsersCountReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchUsersCountReq: null,
    },
};

const usersCountSlice = createSlice({
    name: 'usersCountSlice',
    initialState,
    reducers: {
        initFetchingUsersCount: (state) => {
            state.loadingStatus.fetchUsersCountReqStatus = LOADING_STATUSES.IDLE;
        },
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchUsersCount.pending, (state) => {
                state.loadingStatus.fetchUsersCountReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchUsersCountReq = null;
            })
            .addCase(fetchUsersCount.fulfilled, (state, action) => {
                state.entity = action.payload;
                state.loadingStatus.fetchUsersCountReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.fetchUsersCountReq = null;
            })
            .addCase(fetchUsersCount.rejected, (state, action) => {
                state.loadingStatus.fetchUsersCountReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchUsersCountReq = action.error;
            });
    },
});

export const usersCountEntitySelector = (state) => state.usersCount.entity;
export const usersCountLoadingStatusesSelector = (state) => state.usersCount.loadingStatus;
export const usersCountErrorsSelector = (state) => state.usersCount.errors;
export const usersCountActions = usersCountSlice.actions;
export const usersCountReducer = usersCountSlice.reducer;
