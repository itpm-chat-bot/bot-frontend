import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

// FIXME: transform response data to camelcase notation
export const fetchFAQ = createAsyncThunk(
    'faq/getFAQ',
    async (chatId) => {
        const res = await httpClient.get(routes.faqsPath(), { params: { chat: chatId } });
        return res.data;
    },
);

// FIXME: transform response data to camelcase notation
export const draftFAQ = createAsyncThunk(
    'faq/getFAQ',
    async (chatId) => {
        const res = await httpClient.get(routes.faqsPath(), { params: { chat: chatId, is_draft: true } });
        return res.data;
    },
);

// FIXME: transform response data to camelcase notation
export const createFAQ = createAsyncThunk(
    'faq/createFAQ',
    async ({ faqData }) => {
        const res = await httpClient.post(routes.faqsPath(), faqData);
        return res.data;
    },
);

// FIXME: transform response data to camelcase notation
export const updateFAQ = createAsyncThunk(
    'faq/updateFAQ',
    async ({ faqItemId, faqData }) => {
        const res = await httpClient.put(routes.faqPath(faqItemId), faqData);
        return res.data;
    },
);

export const deleteFAQ = createAsyncThunk(
    'faq/deleteFAQ',
    async (faqId) => {
        await httpClient.delete(routes.faqPath(faqId));
        return faqId;
    },
);

const faqAdapter = createEntityAdapter();

const initialState = faqAdapter.getInitialState({
    loadingStatus: {
        fetchFAQReqStatus: LOADING_STATUSES.IDLE,
        createFAQReqStatus: LOADING_STATUSES.IDLE,
        updateFAQReqStatus: LOADING_STATUSES.IDLE,
        deleteFAQReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchFAQReq: null,
        createFAQReq: null,
        updateFAQReq: null,
        deleteFAQReq: null,
    },
});

const faqSlice = createSlice({
    name: 'faqSlice',
    initialState,
    reducers: {
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchFAQ.pending, (state) => {
                state.loadingStatus.fetchFAQReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchFAQReq = null;
            })
            .addCase(fetchFAQ.fulfilled, (state, action) => {
                faqAdapter.setAll(state, action.payload);
                state.loadingStatus.fetchFAQReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(fetchFAQ.rejected, (state, action) => {
                state.loadingStatus.fetchFAQReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchFAQReq = action.error;
            })
            .addCase(createFAQ.pending, (state) => {
                state.loadingStatus.createFAQReqStatus = LOADING_STATUSES.LOADING;
                state.errors.createFAQReq = null;
            })
            .addCase(createFAQ.fulfilled, (state, action) => {
                faqAdapter.addOne(state, action.payload);
                state.loadingStatus.createFAQReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(createFAQ.rejected, (state, action) => {
                state.loadingStatus.createFAQReq = LOADING_STATUSES.FAILED;
                state.errors.createFAQReq = action.error;
            })
            .addCase(updateFAQ.fulfilled, (state, action) => {
                faqAdapter.updateOne(state, action);
                state.loadingStatus.updateFAQReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(updateFAQ.rejected, (state, action) => {
                state.loadingStatus.updateFAQReq = LOADING_STATUSES.FAILED;
                state.errors.updateFAQReq = action.error;
            })
            .addCase(deleteFAQ.fulfilled, (state, action) => {
                faqAdapter.removeOne(state, action);
                state.loadingStatus.deleteFAQReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(deleteFAQ.rejected, (state, action) => {
                state.loadingStatus.deleteFAQReq = LOADING_STATUSES.FAILED;
                state.errors.deleteFAQReq = action.error;
            });
    },
});

export const faqSelectors = faqAdapter.getSelectors((state) => state.faq);
export const faqLoadingStatusesSelector = (state) => state.faq.loadingStatus;
export const faqErrorsSelector = (state) => state.faq.errors;
export const faqActions = faqSlice.actions;
export const faqReducer = faqSlice.reducer;
