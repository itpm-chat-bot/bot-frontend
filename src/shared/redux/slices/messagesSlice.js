import { createSlice, createEntityAdapter, createAsyncThunk } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

export const fetchMessageById = createAsyncThunk(
    'messages/fetchMessageById',
    async (messageId) => {
        const res = await httpClient.get(routes.messagePath(messageId));
        return camelcaseKeys(res.data, { deep: true });
    },
);

export const deleteMessageById = createAsyncThunk(
    'messages/deleteMessageById',
    async (id) => {
        await httpClient.delete(routes.messagePath(id));
        return id;
    },
);

export const deleteFileById = createAsyncThunk(
    'messages/deleteFileById',
    async (id) => {
        await httpClient.delete(routes.messageFilesByIdPath(id));
        return id;
    },
);

const messagesAdapter = createEntityAdapter();

// TODO: if this slice is going to process loading status and errors of thunks, then these fields
// should be objects (look at other slice implementation)
const initialState = messagesAdapter.getInitialState({
    loadingStatus: LOADING_STATUSES.IDLE,
    error: null,
});

const messagesSlice = createSlice({
    name: 'messagesSlice',
    initialState,
    reducers: {
        resetState: () => initialState,
    },
    extraReducers: () => {},
});

export const messagesSelectors = messagesAdapter.getSelectors((state) => state.messages);
export const messagesLoadingStatusesSelector = (state) => state.messages.loadingStatus;
export const messagesErrorsSelector = (state) => state.messages.errors;
export const messagesActions = messagesSlice.actions;
export const messagesReducer = messagesSlice.reducer;
