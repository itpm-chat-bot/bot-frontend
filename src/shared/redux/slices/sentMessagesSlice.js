import { createAsyncThunk, createSlice, createEntityAdapter } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';
import { normalize, schema } from 'normalizr';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';
import { isEmpty } from '../../lib';
import { deleteMessageById } from './messagesSlice.js';

export const fetchSentMessagesByChatId = createAsyncThunk(
    'sentMessages/fetchSentMessagesByChatId',
    async (chatId) => {
        const res = await httpClient.get(routes.sentMessagesByChatIdPath(chatId));

        if (isEmpty(res.data)) {
            return [];
        }

        const sentMessageSchema = new schema.Entity('sentMessages');
        const sentMessageListSchema = new schema.Array(sentMessageSchema);
        const normalizedData = normalize(res.data, sentMessageListSchema);

        return camelcaseKeys(normalizedData.entities.sentMessages, { deep: true });
    },
);

const sentMessagesAdapter = createEntityAdapter();

// For loading status used "fetchMessagesByChatIdReq" instead of thunk "fetchScheduledMessagesByChatIdReq"
// because there is a problem to select certain status via selector in components.
// Don't forget that this slice and scheduledMessagesSlice serve at MessageList component
const initialState = sentMessagesAdapter.getInitialState({
    loadingStatus: {
        fetchMessagesByChatIdReqStatus: LOADING_STATUSES.IDLE,
        deleteMessageByIdReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchMessagesByChatIdReq: null,
        deleteMessageByIdReq: null,
    },
    currentPageNumber: 1,
});

// TODO: think about correct state updating. Perhaps fetching must be connected via adapter addMany, but not setMany
const sentMessagesSlice = createSlice({
    name: 'sentMessagesSlice',
    initialState,
    reducers: {
        updateCurrentPageNumber: (state, action) => {
            state.currentPageNumber = action.payload;
        },
        resetCurrentPageNumber: (state) => {
            state.currentPageNumber = 1;
        },
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchSentMessagesByChatId.pending, (state) => {
                state.loadingStatus.fetchMessagesByChatIdReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchMessagesByChatIdReq = null;
            })
            .addCase(fetchSentMessagesByChatId.fulfilled, (state, action) => {
                sentMessagesAdapter.setMany(state, action);
                state.loadingStatus.fetchMessagesByChatIdReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.fetchMessagesByChatIdReq = null;
            })
            .addCase(fetchSentMessagesByChatId.rejected, (state, action) => {
                state.loadingStatus.fetchMessagesByChatIdReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchMessagesByChatIdReq = action.error;
            })
            .addCase(deleteMessageById.pending, (state) => {
                state.loadingStatus.deleteMessageByIdReqStatus = LOADING_STATUSES.LOADING;
                state.errors.deleteMessageByIdReq = null;
            })
            .addCase(deleteMessageById.fulfilled, (state, action) => {
                sentMessagesAdapter.removeOne(state, action);
                state.loadingStatus.deleteMessageByIdReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.deleteMessageByIdReq = null;
            })
            .addCase(deleteMessageById.rejected, (state, action) => {
                state.loadingStatus.deleteMessageByIdReqStatus = LOADING_STATUSES.FAILED;
                state.errors.deleteMessageByIdReq = action.error;
            });
    },
});

export const sentMessagesSelectors = sentMessagesAdapter.getSelectors((state) => state.sentMessages);
export const sentMessagesLoadingStatusesSelector = (state) => state.sentMessages.loadingStatus;
export const sentMessagesErrorsSelector = (state) => state.sentMessages.errors;
export const sentMessagesActions = sentMessagesSlice.actions;
export const sentMessagesReducer = sentMessagesSlice.reducer;
