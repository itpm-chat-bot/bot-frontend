import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';
import decamelizeKeys from 'decamelize-keys';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

export const fetchSelectedChat = createAsyncThunk(
    'chats/fetchSelectedChat',
    async (chatId) => {
        const res = await httpClient.get(routes.chatPath(chatId));
        return camelcaseKeys(res.data, { deep: true });
    },
);

export const updateChatTimezone = createAsyncThunk(
    'chats/updateChatTimezone',
    async (arg) => {
        const { chatId, timezone } = arg;
        const res = await httpClient.patch(routes.updateChatTimezonePath(chatId), { timezone });
        return camelcaseKeys(res.data, { deep: true });
    },
);

export const updateServiceMessageSettings = createAsyncThunk(
    'chats/updateServiceMessageSettings',
    async (arg) => {
        const { chatId, settings } = arg;
        const res = await httpClient.patch(routes.updateServiceMessageSettingsPath(chatId), { ...decamelizeKeys(settings) });
        return camelcaseKeys(res.data, { deep: true });
    },
);

const initialState = {
    entity: {},
    loadingStatus: {
        fetchSelectedChatReqStatus: LOADING_STATUSES.IDLE,
        updateChatTimezoneReqStatus: LOADING_STATUSES.IDLE,
        updateServiceMessageSettingsReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchSelectedChatReq: null,
        updateChatTimezoneReq: null,
        updateServiceMessageSettingsReq: null,
    },
};

const selectedChatSlice = createSlice({
    name: 'selectedChatSlice',
    initialState,
    reducers: {
        initFetchingSelectedChat: (state) => {
            state.loadingStatus.fetchSelectedChatReqStatus = LOADING_STATUSES.IDLE;
        },
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchSelectedChat.pending, (state) => {
                state.loadingStatus.fetchSelectedChatReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchSelectedChatReq = null;
            })
            .addCase(fetchSelectedChat.fulfilled, (state, action) => {
                state.entity = action.payload;
                state.loadingStatus.fetchSelectedChatReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.fetchSelectedChatReq = null;
            })
            .addCase(fetchSelectedChat.rejected, (state, action) => {
                state.loadingStatus.fetchSelectedChatReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchSelectedChatReq = action.error;
            })
            .addCase(updateChatTimezone.pending, (state) => {
                state.loadingStatus.updateChatTimezoneReqStatus = LOADING_STATUSES.LOADING;
                state.errors.updateChatTimezoneReq = null;
            })
            .addCase(updateChatTimezone.fulfilled, (state, action) => {
                state.entity.timezone = action.payload.timezone;
                state.loadingStatus.updateChatTimezoneReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.updateChatTimezoneReq = null;
            })
            .addCase(updateChatTimezone.rejected, (state, action) => {
                state.loadingStatus.updateChatTimezoneReqStatus = LOADING_STATUSES.FAILED;
                state.errors.updateChatTimezoneReq = action.error;
            })
            .addCase(updateServiceMessageSettings.pending, (state) => {
                state.loadingStatus.updateServiceMessageSettingsReqStatus = LOADING_STATUSES.LOADING;
                state.errors.updateServiceMessageSettingsReq = null;
            })
            .addCase(updateServiceMessageSettings.fulfilled, (state, action) => {
                Object.assign(state.entity, action.payload);
                state.loadingStatus.updateServiceMessageSettingsReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.updateServiceMessageSettingsReq = null;
            })
            .addCase(updateServiceMessageSettings.rejected, (state, action) => {
                state.loadingStatus.updateServiceMessageSettingsReqStatus = LOADING_STATUSES.FAILED;
                state.errors.updateServiceMessageSettingsReq = action.error;
            });
    },
});

export const selectedChatEntitySelector = (state) => state.selectedChat.entity;
export const selectedChatLoadingStatusesSelector = (state) => state.selectedChat.loadingStatus;
export const selectedChatErrorsSelector = (state) => state.selectedChat.errors;
export const selectedChatActions = selectedChatSlice.actions;
export const selectedChatReducer = selectedChatSlice.reducer;
