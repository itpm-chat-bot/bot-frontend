import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

export const fetchCurrentMessageById = createAsyncThunk(
    'messages/fetchCurrentMessageById',
    async (messageId) => {
        const res = await httpClient.get(routes.messagePath(messageId));
        return camelcaseKeys(res.data, { deep: true });
    },
);

const initialState = {
    entity: {},
    loadingStatus: {
        fetchCurrentMessageByIdReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchCurrentMessageByIdReq: null,
    },
};

const currentMessageSlice = createSlice({
    name: 'currentMessageSlice',
    initialState,
    reducers: {
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchCurrentMessageById.pending, (state) => {
                state.loadingStatus.fetchCurrentMessageByIdReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchCurrentMessageByIdReq = null;
            })
            .addCase(fetchCurrentMessageById.fulfilled, (state, action) => {
                state.entity = action.payload;
                state.loadingStatus.fetchCurrentMessageByIdReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.fetchCurrentMessageByIdReq = null;
            })
            .addCase(fetchCurrentMessageById.rejected, (state, action) => {
                state.loadingStatus.fetchCurrentMessageByIdReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchCurrentMessageByIdReq = action.error;
            });
    },
});

export const currentMessageEntitySelector = (state) => state.currentMessage.entity;
export const currentMessageLoadingStatusesSelector = (state) => state.currentMessage.entity;
export const currentMessageErrorsSelector = (state) => state.currentMessage.errors;
export const currentMessageActions = currentMessageSlice.actions;
export const currentMessageReducer = currentMessageSlice.reducer;
