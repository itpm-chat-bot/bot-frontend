import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';
import decamelizeKeys from 'decamelize-keys';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

export const fetchMyProfile = createAsyncThunk(
    'myProfile/fetchMyProfile',
    async () => {
        const res = await httpClient.get(routes.myProfilePath());
        return camelcaseKeys(res.data, { deep: true });
    },
);

export const updateMyProfile = createAsyncThunk(
    'myProfile/updateMyProfile',
    async (data) => {
        const res = await httpClient.patch(routes.myProfilePath(), decamelizeKeys(data));
        return camelcaseKeys(res.data, { deep: true });
    },
);

const initialState = {
    entity: {},
    loadingStatus: {
        fetchMyProfileReqStatus: LOADING_STATUSES.IDLE,
        updateMyProfileReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchMyProfileReq: null,
        updateMyProfileReq: null,
    },
};

const myProfileSlice = createSlice({
    name: 'myProfileSlice',
    initialState,
    reducers: {
        initFetchingMyProfile: (state) => {
            state.loadingStatus.fetchMyProfileReqStatus = LOADING_STATUSES.IDLE;
        },
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchMyProfile.pending, (state) => {
                state.loadingStatus.fetchMyProfileReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchMyProfileReq = null;
            })
            .addCase(fetchMyProfile.fulfilled, (state, action) => {
                state.entity = action.payload;
                state.loadingStatus.fetchMyProfileReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.fetchMyProfileReq = null;
            })
            .addCase(fetchMyProfile.rejected, (state, action) => {
                state.loadingStatus.fetchMyProfileReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchMyProfileReq = action.error;
            })
            .addCase(updateMyProfile.pending, (state) => {
                state.loadingStatus.updateMyProfileReqStatus = LOADING_STATUSES.LOADING;
                state.errors.updateMyProfileReq = null;
            })
            .addCase(updateMyProfile.fulfilled, (state, action) => {
                state.entity = action.payload;
                state.loadingStatus.updateMyProfileReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.updateMyProfileReq = null;
            })
            .addCase(updateMyProfile.rejected, (state, action) => {
                state.loadingStatus.updateMyProfileReqStatus = LOADING_STATUSES.FAILED;
                state.errors.updateMyProfileReq = action.error;
            });
    },
});

export const myProfileEntitySelector = (state) => state.myProfile.entity;
export const myProfileLoadingStatusesSelector = (state) => state.myProfile.loadingStatus;
export const myProfileErrorsSelector = (state) => state.myProfile.errors;
export const myProfileActions = myProfileSlice.actions;
export const myProfileReducer = myProfileSlice.reducer;
