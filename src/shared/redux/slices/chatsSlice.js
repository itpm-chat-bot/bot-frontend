import { createAsyncThunk, createSlice, createEntityAdapter } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';
import { normalize, schema } from 'normalizr';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';
import { isEmpty } from '../../lib';
import { updateChatTimezone } from './selectedChatSlice.js';

export const fetchChats = createAsyncThunk(
    'chats/fetchChats',
    async () => {
        const res = await httpClient.get(routes.chatsPath());

        if (isEmpty(res.data)) {
            return [];
        }

        const chatSchema = new schema.Entity('chats');
        const chatListSchema = new schema.Array(chatSchema);
        const normalizedData = normalize(res.data, chatListSchema);

        return camelcaseKeys(normalizedData.entities.chats, { deep: true });
    },
);

export const deleteChat = createAsyncThunk(
    'faq/deleteChat',
    async (chatId) => {
        await httpClient.delete(routes.chatPath(chatId));
        return chatId;
    },
);
const chatsAdapter = createEntityAdapter();

const initialState = chatsAdapter.getInitialState({
    loadingStatus: {
        fetchChatsReqStatus: LOADING_STATUSES.IDLE,
        updateChatTimezoneReqStatus: LOADING_STATUSES.IDLE,
        deleteChatReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchChatsReq: null,
        updateChatTimezoneReq: null,
        deleteChatReq: null,
    },
});

const chatsSlice = createSlice({
    name: 'chatsSlice',
    initialState,
    reducers: {
        initFetchingChats: (state) => {
            state.loadingStatus.fetchChatsReqStatus = LOADING_STATUSES.IDLE;
        },
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchChats.pending, (state) => {
                state.loadingStatus.fetchChatsReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchChatsReq = null;
            })
            .addCase(fetchChats.fulfilled, (state, action) => {
                chatsAdapter.addMany(state, action);
                state.loadingStatus.fetchChatsReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.fetchChatsReq = null;
            })
            .addCase(fetchChats.rejected, (state, action) => {
                state.loadingStatus.fetchChatsReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchChatsReq = action.error;
            })
            .addCase(updateChatTimezone.pending, (state) => {
                state.loadingStatus.updateChatTimezoneReqStatus = LOADING_STATUSES.LOADING;
                state.errors.updateChatTimezoneReq = null;
            })
            .addCase(updateChatTimezone.fulfilled, (state, action) => {
                chatsAdapter.updateOne(state, action);
                state.loadingStatus.updateChatTimezoneReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.updateChatTimezoneReq = null;
            })
            .addCase(updateChatTimezone.rejected, (state, action) => {
                state.loadingStatus.updateChatTimezoneReqStatus = LOADING_STATUSES.FAILED;
                state.errors.updateChatTimezoneReq = action.error;
            })
            .addCase(deleteChat.fulfilled, (state, action) => {
                chatsAdapter.removeOne(state, action);
                state.loadingStatus.deleteChatReqStatus = LOADING_STATUSES.SUCCEEDED;
            })
            .addCase(deleteChat.rejected, (state, action) => {
                state.loadingStatus.deleteChatReqStatus = LOADING_STATUSES.FAILED;
                state.errors.deleteChatReq = action.error;
            });
    },
});

export const chatsSelectors = chatsAdapter.getSelectors((state) => state.chats);
export const chatsLoadingStatusesSelector = (state) => state.chats.loadingStatus;
export const chatsErrorsSelector = (state) => state.chats.errors;
export const chatsActions = chatsSlice.actions;
export const chatsReducer = chatsSlice.reducer;
