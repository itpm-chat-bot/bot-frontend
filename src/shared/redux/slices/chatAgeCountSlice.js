import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import camelcaseKeys from 'camelcase-keys';

import { httpClient } from '@/app/services/http-client.js';

import { routes } from '../../api/routes.js';
import { LOADING_STATUSES } from '../../constants/statuses.js';

export const fetchAgeCount = createAsyncThunk(
    'chats/fetchAgeCount',
    async (tgChatId) => {
        const res = await httpClient.get(routes.chatAgeCount(tgChatId));
        return camelcaseKeys(res.data, { deep: true });
    },
);

const initialState = {
    entity: {},
    loadingStatus: {
        fetchAgeCountReqStatus: LOADING_STATUSES.IDLE,
    },
    errors: {
        fetchAgeCountReq: null,
    },
};

const ageCountSlice = createSlice({
    name: 'ageCountSlice',
    initialState,
    reducers: {
        initFetchingAgeCount: (state) => {
            state.loadingStatus.fetchAgeCountReqStatus = LOADING_STATUSES.IDLE;
        },
        resetState: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchAgeCount.pending, (state) => {
                state.loadingStatus.fetchAgeCountReqStatus = LOADING_STATUSES.LOADING;
                state.errors.fetchAgeCountReq = null;
            })
            .addCase(fetchAgeCount.fulfilled, (state, action) => {
                state.entity = action.payload;
                state.loadingStatus.fetchAgeCountReqStatus = LOADING_STATUSES.SUCCEEDED;
                state.errors.fetchAgeCountReq = null;
            })
            .addCase(fetchAgeCount.rejected, (state, action) => {
                state.loadingStatus.fetchAgeCountReqStatus = LOADING_STATUSES.FAILED;
                state.errors.fetchAgeCountReq = action.error;
            });
    },
});

export const ageCountEntitySelector = (state) => state.ageCount.entity;
export const ageCountLoadingStatusesSelector = (state) => state.ageCount.loadingStatus;
export const ageCountErrorsSelector = (state) => state.ageCount.errors;
export const ageCountActions = ageCountSlice.actions;
export const ageCountReducer = ageCountSlice.reducer;
