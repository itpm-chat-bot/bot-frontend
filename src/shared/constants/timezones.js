export const DEFAULT_TIMEZONES = [
    'Europe/Warsaw',
    'Europe/Minsk',
    'Europe/Kiev',
    'Europe/Moscow',
    'Europe/Istanbul',
    'Asia/Tbilisi',
];

export const MAX_TIMEZONE_INPUT_LENGTH = 50;
