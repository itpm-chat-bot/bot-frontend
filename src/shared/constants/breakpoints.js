// KEEP WATCH! This file must be synchronised with './breakpoints.scss'

// antd breakpoint width (https://ant.design/components/layout#layout)
export const breakpoints = {
    xs: '480px',
    sm: '576px',
    md: '768px',
    lg: '992px',
    xl: '1200px',
    xxl: '1600px',
};
