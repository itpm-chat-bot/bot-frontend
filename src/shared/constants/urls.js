export const PRODUCTION_BOT_URL = 'https://t.me/ITPM_dev_main_bot';
export const FEEDBACK_BOT_URL = 'https://t.me/feedback_itpmbot';
export const TRIBUTE = 'https://t.me/tribute/app?startapp=d6eg';

export const MAX_URL_LENGTH = 2048;
