// KEEP WATCH! This file is based on '_colours.scss' and must be synchronised with it

export const colours = {
    // Basic colours
    white: '#FFFFFF',
    bg: '#EBEBEB',
    cardStroke: '#E0E3E7',
    primaryGray: '#FAFAFA',
    gray: '#A6A9B2',
    secondText: '#3E3F4B',
    primaryText: '#252525',

    // Accent colours
    accentBlue: '#2C37B5',
    darkBlue: '#4D90F1',
    secondBlue: '#1F66CC',
    blue: '#ADC6FF',
    cardBlue: '#E5EDFF',
    bgBlue: '#1A5AB6',

    // Functional colours
    red: '#D36668',
    green: '#00952A',
    yellow: '#EEAF0F',

    // Other
    transparent: 'transparent',
    lightGray: '#E9EBF3',
    overlay: '#00000080',
    transparentGreen: '#00952A1A',
    transparentYellow: '#EEAF0F1A',
    transparentBlue: '#4D90F11A',

    // Old colours
    gray2: '#B6C9D4',
    selected: '#C0DFFC',
    purpleLight: '#D6CEF9',
    purple: '#B3C8F2',
    greenLight: '#CEF9D5',
};
