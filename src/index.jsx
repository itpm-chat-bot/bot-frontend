import { StrictMode } from 'react';

import ReactDOM from 'react-dom/client';

import { App } from './app/App.jsx';

import 'antd/dist/reset.css';
import './app/styles/global.css';
import './app/styles/custom.css';

import './app/services/i18n.js';
import './app/services/load-timezones.js';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <StrictMode>
        <App />
    </StrictMode>,
);
