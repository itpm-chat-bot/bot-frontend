import { nanoid } from '@reduxjs/toolkit';

export const TAB_ITEMS = [
    { content: 'tabs.sent', id: nanoid(), link: 'history' },
    { content: 'tabs.scheduled', id: nanoid(), link: 'scheduled' },
];
