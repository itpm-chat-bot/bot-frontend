import { useTranslation } from 'react-i18next';
import { Outlet, useNavigate } from 'react-router-dom';

import { SwitchTimezone } from '@/features/SwitchTimezone';
import { CommonHead, Button, Container, Aside, AsideContent, Main, NavTabs } from '@/shared/ui';

import { TAB_ITEMS } from '../model/tab-items.js';

import styles from './Layout.module.css';

export function Layout() {
    const navigate = useNavigate();
    const { t } = useTranslation('messages');

    return (
        <Main>
            <Container>
                <CommonHead title={t('titles.messages')} />

                <div>
                    <NavTabs items={TAB_ITEMS} namespace='messages' />
                    <Outlet />
                </div>

            </Container>

            <Aside>
                <AsideContent>
                    <SwitchTimezone />

                    <div className={styles.buttons}>
                        <Button variant='secondary' disabled>
                            {t('buttons.newPoll')}
                        </Button>
                        <Button onClick={() => navigate('create')}>
                            {t('buttons.newMessage')}
                        </Button>
                    </div>
                </AsideContent>
            </Aside>
        </Main>
    );
}
