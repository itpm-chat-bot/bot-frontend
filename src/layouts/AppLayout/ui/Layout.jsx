import { Toaster } from 'react-hot-toast';
import { useSelector } from 'react-redux';
import { Outlet, useLocation } from 'react-router-dom';

import { useSpinDelay } from 'spin-delay';

import { toasterConfig } from '@/shared/config/toaster';
import { cn } from '@/shared/lib';
import { isLoadingStatusSelector } from '@/shared/redux/selectors';
import { Loader } from '@/shared/ui';
import { SideMenu } from '@/widgets/SideMenu';

import styles from './Layout.module.css';

export function Layout() {
    const isLoading = useSelector(isLoadingStatusSelector);
    const location = useLocation();
    const isHomePage = location.pathname === '/';

    const shouldShowLoader = useSpinDelay(isLoading);

    return (
        <div className={cn(styles.layout, {
            [styles.maxWidth]: isHomePage,
        })}
        >
            {!isHomePage && (
                <section className={styles.sider}>
                    <SideMenu />
                </section>
            )}

            <Outlet />
            <Loader show={shouldShowLoader} />
            <Toaster {...toasterConfig} />
        </div>
    );
}
