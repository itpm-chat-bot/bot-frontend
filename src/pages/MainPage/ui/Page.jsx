import { useEffect } from 'react';
import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { SwitchLanguage } from '@/features/SwitchLanguage';
import { LOADING_STATUSES } from '@/shared/constants/statuses.js';
import { PRODUCTION_BOT_URL } from '@/shared/constants/urls.js';
import { useTabletMediaQuery, isEmpty } from '@/shared/lib';
import { chatsActions, chatsErrorsSelector, chatsLoadingStatusesSelector, chatsSelectors, fetchChats } from '@/shared/redux/slices/chatsSlice.js';
import { fetchMyProfile, myProfileActions, myProfileErrorsSelector, myProfileLoadingStatusesSelector } from '@/shared/redux/slices/myProfileSlice.js';
import { updateChatTimezone } from '@/shared/redux/slices/selectedChatSlice.js';
import { Button, ProjectLogo, SmallScreenFallback } from '@/shared/ui';
import { ChatList } from '@/widgets/ChatList';
import { WelcomeIllustration } from '@/widgets/WelcomeIllustration';

import styles from './Page.module.css';

export function Page() {
    const language = useSelector((state) => state.myProfile.entity.siteLanguage);
    const chats = useSelector(chatsSelectors.selectAll);
    const { fetchChatsReqStatus } = useSelector(chatsLoadingStatusesSelector);
    const chatsErrors = useSelector(chatsErrorsSelector);
    const { fetchMyProfileReqStatus } = useSelector(myProfileLoadingStatusesSelector);
    const myProfileErrors = useSelector(myProfileErrorsSelector);

    const {
        t: tCommon,
        i18n,
    } = useTranslation('common');
    const { t: tMain } = useTranslation('main');
    const dispatch = useDispatch();

    const botAdminChats = chats.filter((chat) => chat.isBotAdmin);
    const botIsNotAdminChats = chats.filter((chat) => !chat.isBotAdmin);

    Object.values(chatsErrors)
        .concat(Object.values(myProfileErrors))
        .filter((error) => error !== null)
        .forEach(({
            message,
            description,
        }) => toast.error(`${message}\n${description}`));

    const isTablet = useTabletMediaQuery();

    useEffect(() => {
        dispatch(chatsActions.initFetchingChats());
        dispatch(myProfileActions.initFetchingMyProfile());
    }, [dispatch]);

    useEffect(() => {
        if (fetchMyProfileReqStatus === LOADING_STATUSES.IDLE) {
            dispatch(fetchMyProfile());
        }

        if (fetchChatsReqStatus === LOADING_STATUSES.IDLE) {
            dispatch(fetchChats());
        }
    }, [dispatch, fetchChatsReqStatus, fetchMyProfileReqStatus]);

    // Set timezone initially after first user registration and redirecting from telegram
    useEffect(() => {
        if (!isEmpty(chats)) {
            const localTz = Intl.DateTimeFormat()
                .resolvedOptions().timeZone;
            chats.forEach(({
                id,
                timezone,
            }) => !timezone && dispatch(updateChatTimezone({
                chatId: id,
                timezone: localTz,
            })));
        }
    }, [dispatch, chats]);

    useEffect(() => {
        i18n.changeLanguage(language);
    }, [i18n, language]);

    if (isTablet) {
        return <SmallScreenFallback />;
    }

    return (
        <section className={styles.section}>
            <WelcomeIllustration />

            <div className={styles.container}>
                <header>
                    <ProjectLogo />
                    <SwitchLanguage />
                </header>

                <main>
                    {botAdminChats.length > 0 && (
                        <ChatList
                            chats={botAdminChats}
                            isStretched={botIsNotAdminChats.length === 0}
                            title={tMain('chooseChat')}
                            isBotAdmin
                        />
                    )}

                    {botIsNotAdminChats.length > 0 && (
                        <ChatList
                            chats={botIsNotAdminChats}
                            isBotAdmin={false}
                            isStretched={botAdminChats.length === 0}
                            subtitle={tMain('botIsNotAdmin')}
                            title={tMain('inactiveChats')}
                        />
                    )}

                    {botAdminChats.length === 0 && botIsNotAdminChats.length === 0 && (
                        <ChatList
                            subtitle={tMain('botNotAdded')}
                            title={tMain('chooseChat')}
                        />
                    )}

                    <Button
                        className={styles.addChat}
                        variant='tertiary'
                    >
                        <Link
                            rel='noreferrer'
                            target='_blank'
                            to={PRODUCTION_BOT_URL}
                        >
                            {tMain('addChat')}
                        </Link>
                    </Button>
                </main>

                <footer>
                    <small>{tCommon('labels.version', { version: import.meta.env.VITE_APP_NPM_PACKAGE_VERSION })}</small>
                </footer>
            </div>
        </section>
    );
}
