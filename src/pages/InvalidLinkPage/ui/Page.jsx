import { useRouteError } from 'react-router-dom';

import logo from '@/shared/assets/logo.svg';
import unlucky from '@/shared/assets/unlucky.svg';

import styles from './Page.module.css';

export function Page() {
    const error = useRouteError();
    console.error(error);

    return (
        <section className={styles.container} id='invalid-link-page'>
            <img alt='IT PM community.' className={styles.logo} src={logo} />

            <main>
                <div className={styles.errorText}>
                    <h1>Упс...ссылка испорчена!</h1>
                    <h2>Обратитесь к администратору</h2>
                </div>
            </main>

            <img alt='' className={styles.unluckySvg} loading='lazy' src={unlucky} />
        </section>
    );
}
