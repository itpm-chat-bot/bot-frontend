import { useTranslation } from 'react-i18next';

import { CommonHead, Button, Container, Aside, AsideContent, Main } from '@/shared/ui';
import { MessageForm } from '@/widgets/messages/MessageForm';

import styles from './Page.module.css';

export function Page() {
    const { t } = useTranslation('messages');

    return (
        <Main>
            <Container>
                <CommonHead title={t('titles.newMessage')} />

                <MessageForm />
            </Container>

            <Aside>
                <AsideContent>
                    <div className={styles.pollContainer}>
                        <Button variant='secondary' disabled>
                            {t('buttons.addPoll')}
                        </Button>
                        <p className={styles.pollComment}>
                            {t('pollComment')}
                        </p>
                    </div>
                    <div className={styles.buttonsContainer}>
                        <Button form='message-form' type='submit'>
                            {t('buttons.send')}
                        </Button>
                    </div>
                </AsideContent>
            </Aside>
        </Main>
    );
}
