import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { SwitchTimezone } from '@/features/SwitchTimezone';
import { chatTypeLabelKeys } from '@/shared/constants/chatTypes';
import { selectedChatEntitySelector, selectedChatErrorsSelector } from '@/shared/redux/slices/selectedChatSlice.js';
import { Aside, AsideContent, Button, CommonHead, Container, Main } from '@/shared/ui';
import { MetaInfo, NotificationSettings } from '@/widgets/ChatDetails';

import styles from './Page.module.css';

export function Page() {
    const inviteLink = useSelector((state) => state.selectedChat.entity.inviteLink);
    const { type } = useSelector(selectedChatEntitySelector);
    const errors = useSelector(selectedChatErrorsSelector);

    const { t } = useTranslation('about-chat');

    Object.values(errors).filter((error) => error !== null)
        .forEach(({ message, description }) => toast.error(`${message}\n${description}`));

    const chatTypeLabel = ` ${t(chatTypeLabelKeys[type])}`;

    const handleClick = () => {
        window.open(inviteLink, '_blank');
    };

    return (
        <Main>
            <Container>
                <CommonHead title={(
                    <>
                        {t('title')}
                        <span className={styles.type}>{chatTypeLabel}</span>
                    </>
                )}
                />
                <MetaInfo />
                <NotificationSettings />
            </Container>

            <Aside>
                <AsideContent>
                    <SwitchTimezone />
                    <Button onClick={handleClick}>
                        {t('buttons.goToTheChat')}
                    </Button>
                </AsideContent>
            </Aside>
        </Main>
    );
}
