import { useTranslation } from 'react-i18next';
import { Link, useRouteError } from 'react-router-dom';

import unlucky from '@/shared/assets/unlucky.svg';

import { ProjectLogo } from '../../../shared/ui';

import styles from './Page.module.css';

export function Page() {
    const { t } = useTranslation('not-found');
    const error = useRouteError();
    console.error(error);

    return (
        <section className={styles.container} id='error-page'>
            <ProjectLogo />

            <main className={styles.main}>
                <div className={styles.errorText}>
                    <h1>{t('notFound.title')}</h1>
                    <h2>{t('notFound.subtitle')}</h2>
                </div>

                <div className={styles.navLink}>
                    <Link to='/'>{t('notFound.goToHome')}</Link>
                </div>
            </main>

            <img alt='' className={styles.unluckySvg} loading='lazy' src={unlucky} />
        </section>
    );
}
