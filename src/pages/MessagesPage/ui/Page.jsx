import { useEffect } from 'react';
import toast from 'react-hot-toast';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { useMessagesContext } from '@/shared/lib';
import { currentMessageActions } from '@/shared/redux/slices/currentMessageSlice.js';
import { fetchScheduledMessagesByChatId } from '@/shared/redux/slices/scheduledMessagesSlice.js';
import { fetchSentMessagesByChatId } from '@/shared/redux/slices/sentMessagesSlice.js';
import { MessageList } from '@/widgets/messages/MessageList';

export function Page() {
    const { reducerName, actions, matchScheduledMessagesRoutePath, matchMessagesHistoryRoutePath } = useMessagesContext();
    const errors = useSelector((state) => state[reducerName].errors);
    const { chatId } = useParams();

    const dispatch = useDispatch();

    // Processing errors of scheduled and sent messages slices
    Object.values(errors)
        .filter((error) => error !== null)
        .forEach(({ message, description }) => toast.error(`${message}\n${description}`));

    useEffect(() => {
        if (matchScheduledMessagesRoutePath) {
            dispatch(fetchScheduledMessagesByChatId(chatId));
        } else if (matchMessagesHistoryRoutePath) {
            dispatch(fetchSentMessagesByChatId(chatId));
        } else {
            throw new Error('Unmatched route path at MessageList. Data was not loaded');
        }

        return () => {
            dispatch(actions.resetState());
            dispatch(currentMessageActions.resetState());
        };
    }, [chatId, actions, matchScheduledMessagesRoutePath, matchMessagesHistoryRoutePath, dispatch]);

    return <MessageList />;
}
