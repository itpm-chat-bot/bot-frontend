import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Outlet, useParams } from 'react-router-dom';

import { LOADING_STATUSES } from '@/shared/constants/statuses.js';
import { chatsActions, chatsLoadingStatusesSelector, fetchChats } from '@/shared/redux/slices/chatsSlice.js';
import { fetchMyProfile } from '@/shared/redux/slices/myProfileSlice.js';
import { selectedChatActions, fetchSelectedChat, selectedChatLoadingStatusesSelector } from '@/shared/redux/slices/selectedChatSlice.js';

export function Page() {
    const myProfileId = useSelector((state) => state.myProfile.entity.id);
    const language = useSelector((state) => state.myProfile.entity.siteLanguage);
    const { fetchSelectedChatReqStatus } = useSelector(selectedChatLoadingStatusesSelector);
    const { fetchChatsReqStatus } = useSelector(chatsLoadingStatusesSelector);

    const { chatId } = useParams();

    const dispatch = useDispatch();
    const { i18n } = useTranslation('common');

    useEffect(() => {
        dispatch(selectedChatActions.initFetchingSelectedChat());
        dispatch(chatsActions.initFetchingChats());
    }, [dispatch]);

    useEffect(() => {
        if (fetchSelectedChatReqStatus === LOADING_STATUSES.IDLE) {
            dispatch(fetchSelectedChat(chatId));
        }

        if (fetchChatsReqStatus === LOADING_STATUSES.IDLE) {
            dispatch(fetchChats());
        }

        if (!myProfileId) {
            dispatch(fetchMyProfile());
        }
    }, [dispatch, fetchSelectedChatReqStatus, fetchChatsReqStatus, chatId, myProfileId]);

    useEffect(() => {
        i18n.changeLanguage(language);
    }, [language, i18n]);

    return (
        <Outlet />
    );
}
