import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';

import { httpClient } from '@/app/services/http-client.js';
import { SwitchLanguage } from '@/features/SwitchLanguage';
import { routes } from '@/shared/api/routes.js';
import { useAuth } from '@/shared/lib';
import { ProjectLogo, TelegramLoginButton } from '@/shared/ui';
import { WelcomeIllustration } from '@/widgets/WelcomeIllustration';

import { getCurrentBotUsername } from '../lib/get-current-bot-username.js';

import styles from './Page.module.css';

const redirectionUrl = [import.meta.env.VITE_APP_API_BASE_URL, 'login'].join('/').concat('/');

export function Page() {
    const auth = useAuth();
    const navigate = useNavigate();
    const [searchParams] = useSearchParams();
    const location = useLocation();

    const { t: tCommon } = useTranslation('common');
    const { t: tLogin } = useTranslation('login');

    const { loggedIn, logIn, logOut } = auth;
    const from = location.state?.from?.pathname || '/';

    if (import.meta.env.VITE_APP_CI_ENV !== 'production') {
        /* eslint eslint-comments/no-use: off */
        // eslint-disable-next-line no-console
        console.log('searchParams: ', searchParams.toString());
    }

    // This effect is responsible for user authentication
    useEffect(() => {
        if (loggedIn) {
            navigate(from, { replace: true });
            return undefined; // https://eslint.org/docs/latest/rules/consistent-return
        }

        let ignore = false;

        async function authenticate() {
            try {
                const res = await httpClient.get(routes.loginPath(), { params: searchParams });

                if (!ignore) {
                    sessionStorage.setItem('accessToken', res.data.access);
                    sessionStorage.setItem('refreshToken', res.data.refresh);
                    logIn();
                    navigate('/', { replace: true });
                }
            } catch (err) {
                logOut();

                // Suppress 400 error at production
                if (import.meta.env.VITE_APP_CI_ENV === 'production') {
                    if (err.isAxiosError && err.response.status === 400) {
                        return;
                    }
                }

                console.error(err);
                throw err;
            }
        }

        authenticate();

        return () => {
            ignore = true;
        };
    }, [searchParams, loggedIn, from, logIn, logOut, navigate]);

    return (
        <section className={styles.section}>
            <WelcomeIllustration />
            <div className={styles.container}>
                <header>
                    <ProjectLogo />
                    <SwitchLanguage />
                </header>

                <main>
                    <h1 className={styles.h1}>{tLogin('title')}</h1>
                    <h3 className={styles.h3}>{tLogin('greeting')}</h3>
                    <TelegramLoginButton
                        authUrl={redirectionUrl}
                        botUsername={getCurrentBotUsername(import.meta.env.VITE_APP_CI_ENV)}
                    />
                </main>

                <footer>
                    <small>{tCommon('labels.version', { version: import.meta.env.VITE_APP_NPM_PACKAGE_VERSION })}</small>
                </footer>
            </div>
        </section>
    );
}
