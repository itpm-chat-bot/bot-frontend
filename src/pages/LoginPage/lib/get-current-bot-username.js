import { DEV_BOT_USERNAME, PRODUCTION_BOT_USERNAME } from '@/shared/constants/usernames.js';

export function getCurrentBotUsername(environmentName) {
    switch (environmentName) {
        case 'dev':
            return DEV_BOT_USERNAME;
        case 'production':
            return PRODUCTION_BOT_USERNAME;
        default:
            console.error(`Gived unknown environment name ${environmentName} during getting current bot username`);
            return '';
    }
}
