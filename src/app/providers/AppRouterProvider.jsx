import { Suspense } from 'react';
import { RouterProvider } from 'react-router-dom';

import { Loader } from '@/shared/ui';

import { router } from '../router';

export const AppRouterProvider = () => (
    <Suspense fallback={<Loader show />}>
        <RouterProvider router={router} />
    </Suspense>
);
