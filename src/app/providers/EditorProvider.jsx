import { useState, useMemo, useCallback } from 'react';

import { EditorContext } from '@/shared/lib';

export function EditorProvider({ children }) {
    const [editor, setEditor] = useState(null);

    const setEditorInstance = useCallback((newInstance) => {
        setEditor(newInstance);
    }, [setEditor]);

    const value = useMemo(() => ({
        editorInstance: editor,
        setEditorInstance,
    }), [editor, setEditorInstance]);

    return (
        <EditorContext.Provider value={value}>
            {children}
        </EditorContext.Provider>
    );
}
