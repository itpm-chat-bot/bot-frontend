import { useMemo } from 'react';
import { Outlet, useParams, useLocation } from 'react-router-dom';

import { MessagesContext } from '../../shared/lib';
import { scheduledMessagesSelectors, scheduledMessagesActions } from '../../shared/redux/slices/scheduledMessagesSlice.js';
import { sentMessagesSelectors, sentMessagesActions } from '../../shared/redux/slices/sentMessagesSlice.js';

export function MessagesProvider() {
    const { chatId, messageId } = useParams();
    const { pathname } = useLocation();
    const matchMessageCreateRoutePath = pathname.startsWith(`/chats/${chatId}/messages/sending/create`);
    const matchScheduledMessagesRoutePath = pathname.startsWith(`/chats/${chatId}/messages/sending/scheduled`);
    const matchScheduledMessageEditRoutePath = pathname.startsWith(`/chats/${chatId}/messages/sending/scheduled/${messageId}/edit`);
    const matchMessagesHistoryRoutePath = pathname.startsWith(`/chats/${chatId}/messages/sending/history`);

    let reducerName;
    if (matchScheduledMessagesRoutePath) {
        reducerName = 'scheduledMessages';
    } else if (matchMessagesHistoryRoutePath) {
        reducerName = 'sentMessages';
    } else {
        reducerName = null;
    }

    const selectorsByName = useMemo(() => ({
        scheduledMessages: scheduledMessagesSelectors,
        sentMessages: sentMessagesSelectors,
    }), []);

    const actionsByName = useMemo(() => ({
        scheduledMessages: scheduledMessagesActions,
        sentMessages: sentMessagesActions,
    }), []);

    const value = useMemo(() => ({
        matchMessageCreateRoutePath,
        matchScheduledMessageEditRoutePath,
        matchScheduledMessagesRoutePath,
        matchMessagesHistoryRoutePath,
        reducerName,
        selectors: selectorsByName[reducerName],
        actions: actionsByName[reducerName],
    }), [matchMessageCreateRoutePath, matchScheduledMessageEditRoutePath, matchScheduledMessagesRoutePath,
        matchMessagesHistoryRoutePath, reducerName, selectorsByName, actionsByName]);

    return (
        <MessagesContext.Provider value={value}>
            <Outlet />
        </MessagesContext.Provider>
    );
}
