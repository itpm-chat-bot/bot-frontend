import { ConfigProvider, App } from 'antd';
import ruRu from 'antd/locale/ru_RU';

import { colours } from '@/shared/constants/colours.js';

export function AntdProvider({ children }) {
    return (
        <ConfigProvider
            locale={ruRu}
            theme={{
                token: {
                    fontFamily: "'InterVariable, Inter', sans-serif",
                    fontSize: 16,
                    lineHeight: 1.2,
                    colorText: colours.primaryText,
                },
                components: {
                    Alert: {
                        withDescriptionPadding: '10px 12px',
                        colorTextHeading: colours.red,
                        colorText: colours.red,
                        // TODO: add this colour to palette when UI Kit will be ready
                        colorWarningBg: '#FAE5E5',
                        colorWarningBorder: colours.transparent,
                    },
                    Button: {
                        controlHeight: 42,
                        colorText: colours.darkBlue,
                        colorTextLightSolid: colours.white,
                        borderRadius: 16,
                        colorBorder: colours.darkBlue,
                        colorPrimary: colours.blue,
                        colorPrimaryHover: colours.darkBlue,
                        colorPrimaryActive: colours.darkBlue,
                    },
                    Switch: {
                        trackMinWidth: 52,
                        trackMinWidthSM: 32,
                        trackHeight: 28,
                        trackHeightSM: 18,
                        handleSize: 24,
                        handleSizeSM: 14,
                        colorTextTertiary: colours.cardStroke, // bg color when hover
                    },
                    Select: {
                        controlHeight: 40,
                        controlHeightLG: 42,
                        fontFamily: 'InterVariable, Inter, sans-serif',
                        colorTextDisabled: colours.gray,
                        borderRadius: 16,
                        lineWidth: 1,
                        colorBorder: colours.transparent,
                    },
                    Divider: {
                        marginLG: 0,
                        colorSplit: colours.cardStroke,
                    },
                    Modal: {
                        marginXS: 22,
                        paddingMD: 36,
                        paddingContentHorizontalLG: 60,
                        padding: 32,
                        fontSize: 16,
                        fontSizeHeading5: 24,
                        fontWeightStrong: 400,
                        borderRadiusLG: 60,
                        colorBgElevated: colours.white,
                    },
                    DatePicker: {
                        controlHeight: 37,
                        padding: 22,
                        colorTextPlaceholder: colours.gray,
                        borderRadius: 16, // radius for input
                        borderRadiusLG: 24, // radius for popup box
                        borderRadiusSM: 32, // radius for day in popup
                        colorBorder: colours.cardStroke,
                        colorPrimaryHover: colours.darkBlue,
                        colorIcon: colours.gray,
                    },
                    Popover: {
                        fontWeightStrong: 400,
                        colorBgElevated: colours.white,
                        borderRadiusLG: 24,
                    },
                    Popconfirm: {
                        fontSize: 20, // size of title
                        fontWeightStrong: 400,
                        marginXS: 40, // margin between description and buttons
                        marginXXS: 12, // margin between title and description
                    },
                    Input: {
                        controlHeight: 42,
                        paddingSM: 16,
                        fontFamily: 'InterVariable, Inter, sans-serif',
                        colorTextPlaceholder: colours.gray,
                        colorError: colours.red,
                        colorBgContainer: colours.white,
                        colorErrorOutline: colours.white,
                        colorBorder: colours.cardStroke,
                        colorPrimaryHover: colours.darkBlue,
                        borderRadius: 16,
                    },
                },
            }}
        >
            <App>
                {children}
            </App>
        </ConfigProvider>
    );
}
