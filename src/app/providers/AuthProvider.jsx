import { useState, useMemo, useCallback, useLayoutEffect, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { AuthContext } from '@/shared/lib';
import { chatsActions } from '@/shared/redux/slices/chatsSlice.js';
import { commandsActions } from '@/shared/redux/slices/commandsSlice.js';
import { currentMessageActions } from '@/shared/redux/slices/currentMessageSlice.js';
import { expertsActions } from '@/shared/redux/slices/expertsSlice.js';
import { faqActions } from '@/shared/redux/slices/faqSlice.js';
import { messagesActions } from '@/shared/redux/slices/messagesSlice.js';
import { myProfileActions } from '@/shared/redux/slices/myProfileSlice.js';
import { scheduledMessagesActions } from '@/shared/redux/slices/scheduledMessagesSlice.js';
import { selectedChatActions } from '@/shared/redux/slices/selectedChatSlice.js';
import { sentMessagesActions } from '@/shared/redux/slices/sentMessagesSlice.js';

export function AuthProvider({ children }) {
    const [loggedIn, setLoggedIn] = useState(false);

    const dispatch = useDispatch();

    const logIn = useCallback(() => {
        setLoggedIn(true);
    }, [setLoggedIn]);

    const logOut = useCallback(() => {
        sessionStorage.removeItem('accessToken');
        sessionStorage.removeItem('refreshToken');
        setLoggedIn(false);

        dispatch(chatsActions.resetState());
        dispatch(commandsActions.resetState());
        dispatch(currentMessageActions.resetState());
        dispatch(expertsActions.resetState());
        dispatch(faqActions.resetState());
        dispatch(messagesActions.resetState());
        dispatch(myProfileActions.resetState());
        dispatch(scheduledMessagesActions.resetState());
        dispatch(selectedChatActions.resetState());
        dispatch(sentMessagesActions.resetState());
    }, [setLoggedIn, dispatch]);

    const value = useMemo(() => ({
        loggedIn,
        logIn,
        logOut,
    }), [loggedIn, logIn, logOut]);

    // This layout effect allows to stay user if he clicks a page reload or entered some domain links
    useLayoutEffect(() => {
        const accessToken = sessionStorage.getItem('accessToken');

        if (accessToken) {
            logIn();
        }
    }, [logIn]);

    // This effect logs out user if error has occured during refreshing token in httpClient interceptor
    useEffect(() => {
        function logoutHandler() {
            logOut();
        }

        const root = document.getElementById('root');
        root.addEventListener('logout', logoutHandler);

        return () => {
            root.removeEventListener('logout', logoutHandler);
        };
    }, [logOut]);

    return (
        <AuthContext.Provider value={value}>
            {children}
        </AuthContext.Provider>
    );
}
