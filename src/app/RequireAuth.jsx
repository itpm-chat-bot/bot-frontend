import { Navigate, useLocation } from 'react-router-dom';

import { useAuth } from '@/shared/lib';

export function RequireAuth({ children }) {
    const auth = useAuth();
    const location = useLocation();

    if (!auth.loggedIn) {
        return <Navigate state={{ from: location }} to='/login' replace />;
    }

    return children;
}
