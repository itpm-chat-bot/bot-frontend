import { Provider as ReduxProvider } from 'react-redux';

import { AntdProvider, AppRouterProvider, AuthProvider, DayjsProvider } from './providers';
import { store } from './store.js';

export function App() {
    return (
        <ReduxProvider identityFunctionCheck='always' noopCheck='always' stabilityCheck='always' store={store}>
            <AuthProvider>
                <AntdProvider>
                    <DayjsProvider>
                        <AppRouterProvider />
                    </DayjsProvider>
                </AntdProvider>
            </AuthProvider>
        </ReduxProvider>
    );
}
