import axios from 'axios';

import { routes } from '@/shared/api/routes.js';
import { customLogoutEvent } from '@/shared/lib';

/* eslint eslint-comments/no-use: off */
// eslint-disable-next-line import/prefer-default-export
export const httpClient = axios.create({
    baseURL: import.meta.env.VITE_APP_API_BASE_URL,
});

httpClient.interceptors.request.use(
    (config) => {
        const accessToken = sessionStorage.getItem('accessToken');

        if (accessToken) {
            config.headers.Authorization = `Bearer ${accessToken}`;
        }

        return config;
    },
    (error) => Promise.reject(error),
);

httpClient.interceptors.response.use(
    (response) => response,
    async (error) => {
        const originalRequest = error.config;

        if (error.response.status === 401 && !originalRequest._retry) {
            originalRequest._retry = true;

            try {
                const refreshToken = sessionStorage.getItem('refreshToken');
                const refreshResponse = await httpClient.post(routes.tokenRefreshPath(), {
                    refresh: refreshToken,
                });

                const newAccessToken = refreshResponse.data.access;
                sessionStorage.setItem('accessToken', newAccessToken);

                // Retry the original request with the new access token
                originalRequest.headers.Authorization = `Bearer ${newAccessToken}`;
                return httpClient(originalRequest);
            } catch (e) {
                console.error(e);
                document.getElementById('root').dispatchEvent(customLogoutEvent);
            }
        }

        return Promise.reject(error);
    },
);
