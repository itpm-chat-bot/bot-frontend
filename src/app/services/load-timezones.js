import { DEFAULT_TIMEZONES } from '@/shared/constants/timezones.js';

try {
    // Built-in method to get all IANA timezones which is supported by browser
    const allTimezones = Intl.supportedValuesOf('timeZone');
    localStorage.setItem('timezones', JSON.stringify(allTimezones));
} catch {
    localStorage.setItem('timezones', JSON.stringify(DEFAULT_TIMEZONES));
}
