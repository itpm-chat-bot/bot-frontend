import { initReactI18next } from 'react-i18next';

import i18n from 'i18next';
import resourcesToBackend from 'i18next-resources-to-backend';

i18n
    .use(initReactI18next)
    .use(
        resourcesToBackend(
            (language, namespace) => import(`../../../locales/${language}/${namespace}.json`),
        ),
    )
    .init({
        fallbackLng: 'en',
        lng: 'en',
        interpolation: {
            escapeValue: false,
        },
        defaultNS: 'common',
        debug: import.meta.env.DEV,
    });

i18n.on('languageChanged', (lang) => {
    document.documentElement.lang = lang;
});
