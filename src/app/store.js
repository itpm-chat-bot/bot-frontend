import { configureStore } from '@reduxjs/toolkit';

import { ageCountReducer } from '@/shared/redux/slices/chatAgeCountSlice';
import { chatsReducer } from '@/shared/redux/slices/chatsSlice.js';
import { commandsReducer } from '@/shared/redux/slices/commandsSlice.js';
import { currentMessageReducer } from '@/shared/redux/slices/currentMessageSlice.js';
import { expertsReducer } from '@/shared/redux/slices/expertsSlice.js';
import { faqReducer } from '@/shared/redux/slices/faqSlice.js';
import { messagesReducer } from '@/shared/redux/slices/messagesSlice.js';
import { myProfileReducer } from '@/shared/redux/slices/myProfileSlice.js';
import { participantsReducer } from '@/shared/redux/slices/participantsSlice.js';
import { scheduledMessagesReducer } from '@/shared/redux/slices/scheduledMessagesSlice.js';
import { selectedChatReducer } from '@/shared/redux/slices/selectedChatSlice.js';
import { sentMessagesReducer } from '@/shared/redux/slices/sentMessagesSlice.js';
import { usersCountReducer } from '@/shared/redux/slices/usersCountSlice';

export const store = configureStore({
    reducer: {
        myProfile: myProfileReducer,
        chats: chatsReducer,
        selectedChat: selectedChatReducer,
        messages: messagesReducer,
        scheduledMessages: scheduledMessagesReducer,
        sentMessages: sentMessagesReducer,
        currentMessage: currentMessageReducer,
        experts: expertsReducer,
        faq: faqReducer,
        commands: commandsReducer,
        participants: participantsReducer,
        usersCount: usersCountReducer,
        ageCount: ageCountReducer,
    },
});
