import { createBrowserRouter } from 'react-router-dom';

import { AppLayout } from '@/layouts/AppLayout';
import { ErrorPage } from '@/pages/ErrorPage';
import { lazyImport } from '@/shared/lib';
import { messageLoader } from '@/widgets/messages/MessageForm';

import { EditorProvider } from './providers/EditorProvider.jsx';
import { MessagesProvider } from './providers/MessagesProvider.jsx';
import { RequireAuth } from './RequireAuth.jsx';

const { CommandsLayout } = lazyImport(() => import('@/layouts/CommandsLayout'), 'CommandsLayout');
const { MessagesLayout } = lazyImport(() => import('@/layouts/MessagesLayout'), 'MessagesLayout');
const { FAQLayout } = lazyImport(() => import('@/widgets/FAQ'), 'FAQLayout');
const { AboutChatPage } = lazyImport(() => import('@/pages/AboutChatPage'), 'AboutChatPage');
const { MainPage } = lazyImport(() => import('@/pages/MainPage'), 'MainPage');
const { LoginPage } = lazyImport(() => import('@/pages/LoginPage'), 'LoginPage');
const { InvalidLinkPage } = lazyImport(() => import('@/pages/InvalidLinkPage'), 'InvalidLinkPage');
const { MessageCreatePage } = lazyImport(() => import('@/pages/MessageCreatePage'), 'MessageCreatePage');
const { MessagesPage } = lazyImport(() => import('@/pages/MessagesPage'), 'MessagesPage');
const { NotFoundPage } = lazyImport(() => import('@/pages/NotFoundPage'), 'NotFoundPage');
const { SelectedChatPage } = lazyImport(() => import('@/pages/SelectedChatPage'), 'SelectedChatPage');
const { CommandsPage } = lazyImport(() => import('@/widgets/Commands'), 'CommandsPage');
const { CommandChatRules } = lazyImport(() => import('@/widgets/Commands/ui/CommadsActions/CommandRules'), 'CommandChatRules');
const { CreateCommand } = lazyImport(() => import('@/widgets/Commands/ui/CommadsActions/CreateCommand'), 'CreateCommand');
const { EditCommand } = lazyImport(() => import('@/widgets/Commands/ui/CommadsActions/EditCommand'), 'EditCommand');
const { ExpertsPage } = lazyImport(() => import('@/widgets/Experts'), 'ExpertsPage');
const { DraftList } = lazyImport(() => import('@/widgets/FAQ/ui/DraftFAQ/DraftList'), 'DraftList');
const { AddedQuestionFAQ } = lazyImport(() => import('@/widgets/FAQ/ui/FaqActions/AddedQuestionFAQ'), 'AddedQuestionFAQ');
const { EditQuestionFAQ } = lazyImport(() => import('@/widgets/FAQ/ui/FaqActions/EditQuestionFAQ'), 'EditQuestionFAQ');
const { ListOfPublished } = lazyImport(() => import('@/widgets/FAQ/ui/ListOfQuestionsFAQ/ListOfPublished'), 'ListOfPublished');
const { ParticipantsPage } = lazyImport(() => import('@/widgets/Participants'), 'ParticipantsPage');
const { Activity } = lazyImport(() => import('@/widgets/Participants/ui/ActivityList/Activity'), 'Activity');
const { Administration } = lazyImport(() => import('@/widgets/Participants/ui/Administration/Administration'), 'Administration');
const { Blocking } = lazyImport(() => import('@/widgets/Participants/ui/BlockungList/Blocking'), 'Blocking');

export const router = createBrowserRouter([
    {
        path: '/',
        element: (
            <RequireAuth>
                <AppLayout />
            </RequireAuth>
        ),
        errorElement: <ErrorPage />,
        children: [
            {
                errorElement: <ErrorPage />,
                children: [
                    {
                        index: true,
                        element: <MainPage />,
                    },
                    {
                        path: 'chats/:chatId',
                        element: <SelectedChatPage />,
                        children: [
                            {
                                path: 'about',
                                element: <AboutChatPage />,
                            },
                            {
                                path: 'messages/sending',
                                element: <MessagesProvider />,
                                children: [
                                    {
                                        element: <MessagesLayout />,
                                        children: [
                                            {
                                                path: 'history',
                                                element: (
                                                    <MessagesPage />
                                                ),
                                            },
                                            {
                                                path: 'scheduled',
                                                element: (
                                                    <MessagesPage />
                                                ),
                                            },
                                        ],
                                    },
                                    {
                                        path: 'create',
                                        element: (
                                            <EditorProvider>
                                                <MessageCreatePage />
                                            </EditorProvider>
                                        ),
                                    },
                                    {
                                        path: 'scheduled/:messageId/edit',
                                        element: (
                                            <EditorProvider>
                                                <MessageCreatePage />
                                            </EditorProvider>
                                        ),
                                        loader: messageLoader,
                                    },
                                ],
                            },
                            {
                                path: 'commands',
                                element: (
                                    <EditorProvider>
                                        <CommandsLayout />
                                    </EditorProvider>
                                ),
                                children: [
                                    {
                                        index: true,
                                        element: <CommandsPage />,
                                    },
                                    {
                                        path: 'command',
                                        element: (
                                            <CreateCommand />
                                        ),
                                    },
                                    {
                                        path: 'editCommand/:commandId',
                                        element: (
                                            <EditCommand />
                                        ),
                                    },
                                ],
                            },
                            {
                                path: 'rules',
                                element: (
                                    <EditorProvider>
                                        <CommandChatRules />
                                    </EditorProvider>
                                ),
                            },
                            {
                                path: 'faq',
                                element: (
                                    <EditorProvider>
                                        <FAQLayout />
                                    </EditorProvider>
                                ),
                                children: [
                                    {
                                        path: 'published',
                                        element: <ListOfPublished />,
                                    },
                                    {
                                        path: 'draft',
                                        element: <DraftList />,
                                    },
                                    {
                                        path: 'question',
                                        element: <AddedQuestionFAQ />,
                                    },
                                    {
                                        path: 'editDraft/:faqItemId',
                                        element: <EditQuestionFAQ />,
                                    },
                                ],
                            },
                            {
                                path: 'participants',
                                element: (
                                    <EditorProvider>
                                        <ParticipantsPage />
                                    </EditorProvider>
                                ),
                                children: [
                                    {
                                        path: 'activity',
                                        element: <Activity />,
                                    },
                                    {
                                        path: 'administration',
                                        element: <Administration />,
                                    },
                                    {
                                        path: 'blocking',
                                        element: <Blocking />,
                                    },
                                ],
                            },
                            {
                                path: 'experts',
                                element: <ExpertsPage />,
                            },
                        ],
                    },
                ],
            },
        ],
    },
    {
        path: '/login',
        element: <LoginPage />,
        errorElement: <ErrorPage />,
    },
    {
        path: '/invalid-link',
        element: <InvalidLinkPage />,
    },
    {
        path: '*',
        element: <NotFoundPage />,
    },
]);
