import { Popconfirm } from 'antd';

import { DeleteIcon } from '@/shared/assets/icons/svg';
import { colours } from '@/shared/constants/colours';
import { Button } from '@/shared/ui';

export function DeleteButton({ onConfirm, ...rest }) {
    return (
        <Popconfirm
            arrow={false}
            cancelButtonProps={{ size: 'middle', className: 'custom-popconfirm-cancel-btn' }}
            cancelText='Оставить'
            color={colours.white}
            // FIXME: translate description
            description='* восстановить сообщение будет нельзя'
            icon={null}
            okButtonProps={{ size: 'middle', className: 'custom-btn', style: { marginInlineStart: '0.5rem' } }}
            // FIXME: translate oktext
            okText='Удалить'
            overlayClassName='custom-popconfirm'
            // FIXME: translate title
            title='Удалить сообщение?'
            onCancel={null}
            onConfirm={onConfirm}
        >
            <Button
                // FIXME: translate title
                size='small'
                title='Удалить сообщение'
                variant='tertiary'
                {...rest}
            >
                <DeleteIcon />
            </Button>
        </Popconfirm>
    );
}
