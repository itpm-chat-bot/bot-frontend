import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { cn } from '@/shared/lib';
import { updateMyProfile } from '@/shared/redux/slices/myProfileSlice.js';

import { LANGUAGES } from '../../model/languages.js';

import styles from './Switch.module.css';

export function Switch() {
    const myProfileId = useSelector((state) => state.myProfile.entity.id);

    const { i18n } = useTranslation('common');
    const dispatch = useDispatch();

    const activeLanguage = i18n.resolvedLanguage;

    const handleClick = (evt) => {
        const siteLanguage = evt.target.value;

        if (siteLanguage === activeLanguage) {
            return;
        }

        if (!myProfileId) {
            i18n.changeLanguage(siteLanguage);
        } else {
            dispatch(updateMyProfile({ siteLanguage }));
        }
    };

    return (
        <div className={styles.container}>
            {LANGUAGES.map(({ key, label }) => (
                <button
                    key={key}
                    className={cn(styles.btn, {
                        [styles.active]: key === activeLanguage,
                    })}
                    type='button'
                    value={key}
                    onClick={handleClick}
                >
                    {label}
                </button>
            ))}
        </div>
    );
}
