import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { Select as AntdSelect } from 'antd';

import { ExpandMoreIcon } from '@/shared/assets/icons/svg';
import { MAX_TIMEZONE_INPUT_LENGTH } from '@/shared/constants/timezones.js';
import { updateChatTimezone } from '@/shared/redux/slices/selectedChatSlice.js';

import { filterTimezoneSelectOptions } from '../../lib/filter-timezone-select-options.js';
import { getCurrentTimeByTimezone } from '../../lib/get-current-time-by-timezone.js';
import { getTimezoneSelectOptions } from '../../lib/get-timezone-select-options.js';
import { sortTimezoneSelectOptions } from '../../lib/sort-timezone-select-options.js';

import styles from './Select.module.css';

export function Select() {
    const chatId = useSelector((state) => state.selectedChat.entity.id);
    const timezone = useSelector((state) => state.selectedChat.entity.timezone);
    const [timezoneInput, setTimezoneInput] = useState(null);
    const [time, setTime] = useState(getCurrentTimeByTimezone(timezone));

    const dispatch = useDispatch();

    const handleTimezoneChange = (tz) => {
        setTime(getCurrentTimeByTimezone(tz));
        dispatch(updateChatTimezone({ chatId, timezone: tz }));
    };

    const handleTimezoneInput = (value) => {
        if (value.length > MAX_TIMEZONE_INPUT_LENGTH) {
            return;
        }

        setTimezoneInput(value);
    };

    useEffect(() => {
        const interval = setInterval(() => {
            setTime(getCurrentTimeByTimezone(timezone));
        }, 60000);

        return () => clearInterval(interval);
    }, [timezone]);

    return (
        <div className={styles.container}>
            <p className={styles.time}>{time}</p>

            <div className={styles.select}>
                <AntdSelect
                    className='custom-select'
                    filterOption={filterTimezoneSelectOptions}
                    filterSort={sortTimezoneSelectOptions}
                    listHeight={212}
                    options={getTimezoneSelectOptions()}
                    // FIXME: add translation for placeholder
                    placeholder='Выберите часовой пояс'
                    popupClassName='custom-select-popup'
                    searchValue={timezoneInput}
                    suffixIcon={<ExpandMoreIcon />}
                    value={timezone}
                    showSearch
                    onChange={handleTimezoneChange}
                    onSearch={handleTimezoneInput}
                />
            </div>
        </div>
    );
}
