import dayjs from 'dayjs';

export const getTimezoneSelectOptions = () => {
    const timezones = JSON.parse(localStorage.getItem('timezones'));

    return timezones.map((tz) => ({
        label: dayjs().tz(tz).format(`[(UTC]Z[) ${tz}]`),
        value: tz,
    }));
};
