export const filterTimezoneSelectOptions = (input, option) => (
    option.label.toLowerCase().includes(input.toLowerCase())
);
