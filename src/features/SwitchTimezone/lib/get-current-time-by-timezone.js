import { HOURS_MINUTES_VIA_COLON_FORMAT } from '@/shared/constants/date-formats.js';
import { formatISOtimeByTimezone } from '@/shared/lib/util/format-ISO-time-by-timezone.js';

export const getCurrentTimeByTimezone = (timezone) => formatISOtimeByTimezone(undefined, timezone, HOURS_MINUTES_VIA_COLON_FORMAT);
