export const sortTimezoneSelectOptions = (optionA, optionB) => (
    optionA.label.localeCompare(optionB.label)
);
