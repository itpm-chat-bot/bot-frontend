import { EditIcon } from '@/shared/assets/icons/svg';
import { Button } from '@/shared/ui';

export function EditButton(props) {
    return (
        <Button
            size='small'
            // FIXME: translate title
            title='Изменить сообщение'
            variant='tertiary'
            {...props}
        >
            <EditIcon />
        </Button>
    );
}
