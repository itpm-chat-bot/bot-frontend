import PropTypes from 'prop-types';

import { getInitials } from '@/shared/lib';
import { BasicAvatar } from '@/shared/ui';

import { generateAvatarBackgroundColor } from '../lib/generate-avatar-bg-color';

export function Avatar({ fallbackText, ...props }) {
    const initials = getInitials(fallbackText);
    const fallbackBgColor = generateAvatarBackgroundColor(fallbackText);

    return (
        <BasicAvatar
            backgroundColor={fallbackBgColor}
            initials={initials}
            {...props}
        />
    );
}

Avatar.propTypes = {
    fallbackText: PropTypes.string.isRequired,
};
