export { avatarBackgroundPalette } from './model/palette.js';
export { generateAvatarBackgroundColor } from './lib/generate-avatar-bg-color.js';
export { Avatar } from './ui/Avatar.jsx';
