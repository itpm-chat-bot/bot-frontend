import { colours } from '@/shared/constants/colours.js';

export const avatarBackgroundPalette = [
    colours.purple,
    colours.purpleLight,
    colours.green,
    colours.gray2,
    colours.blue,
];
