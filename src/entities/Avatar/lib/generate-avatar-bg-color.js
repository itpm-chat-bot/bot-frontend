import { generateHashFromString, generateIndexFromHash } from '@/shared/lib';

import { avatarBackgroundPalette } from '../model/palette.js';

export function generateAvatarBackgroundColor(value = 'default_value', palette = avatarBackgroundPalette) {
    const hash = generateHashFromString(value);
    const index = generateIndexFromHash(hash, 0, palette.length - 1);

    return palette[index];
}
