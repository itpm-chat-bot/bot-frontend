import PropTypes from 'prop-types';

import styles from './Card.module.css';

export function Card({ time, text, toolbarButtons }) {
    return (
        <div>
            <div className={styles.toolbar}>
                <span className={styles.time}>
                    {time}
                </span>

                <div className={styles.buttons} data-toolbar-buttons>
                    {toolbarButtons}
                </div>
            </div>

            <p>{text}</p>
        </div>
    );
}

Card.propTypes = {
    time: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    toolbarButtons: PropTypes.element,
};

Card.defaultProps = {
    toolbarButtons: null,
};
