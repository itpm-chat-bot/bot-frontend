export const MAX_QUESTION_COUNT = 100;
export const MAX_SHORT_ANSWER_COUNT = 200;
export const MAX_LINK_COUNT = 120;
