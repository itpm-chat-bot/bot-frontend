import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { draftFAQ, faqSelectors } from '@/shared/redux/slices/faqSlice';

import { EmptyPageFAQ } from '../EmptyPageFAQ';
import { DraftQuestion } from './DraftQuestion';

import styles from '../FAQ.module.css';

export function DraftList() {
    const dispatch = useDispatch();
    const { chatId } = useParams();
    const { t } = useTranslation('faq');
    const faqData = useSelector(faqSelectors.selectAll);

    useEffect(() => {
        dispatch(draftFAQ(chatId));
        window.scrollTo(0, 0);
    }, [chatId, dispatch]);

    return (
        <section className={styles.wrapper}>
            <div className={styles.list}>
                <DraftQuestion />
                <div className={styles.listModule}>
                    {faqData.length === 0 && (
                        <EmptyPageFAQ title={t('emptyPage.draftListIsEmpty')} />
                    )}
                </div>
            </div>
        </section>
    );
}
