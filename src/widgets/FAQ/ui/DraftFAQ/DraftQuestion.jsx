import { useState } from 'react';
import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';

import { deleteFAQ, draftFAQ, faqSelectors, updateFAQ } from '@/shared/redux/slices/faqSlice.js';

import { DeleteModal } from '../DeleteModal/DeleteModal';
import { QuestionItem } from '../Question/Question';

export function DraftQuestion() {
    const faqData = useSelector(faqSelectors.selectAll);
    const [openItems, setOpenItems] = useState([]);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [selectedItemId, setSelectedItemId] = useState(null);

    const { chatId } = useParams();
    const dispatch = useDispatch();
    const { t } = useTranslation('faq');

    const deleteModalTitle = t('modals.deleteClue');

    const showModal = (id) => {
        setIsModalVisible(true);
        setSelectedItemId(id);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
        setSelectedItemId(null);
    };

    const handleDelete = () => {
        if (selectedItemId !== null) {
            dispatch(deleteFAQ(selectedItemId));
            setOpenItems(openItems.filter((item) => item !== selectedItemId));
            setIsModalVisible(false);
        }
    };
    const navigate = useNavigate();

    const handleEdit = (faqItem) => {
        const link = `/chats/${chatId}/faq/editDraft/${faqItem.id}`;
        navigate(link);
    };
    const handlePublishDraft = async (faqItem) => {
        const data = { ...faqItem, is_draft: false, chat_id: chatId };

        await dispatch(updateFAQ({
            faqItemId: data.id,
            faqData: data,
        }));
        await dispatch(draftFAQ(chatId));
        const message = (
            <span>
                {t('addQuestion.savedToDraft')}
                <b> /FAQ </b>
                {t('addQuestion.chat')}
            </span>
        );
        toast.success(message);
    };

    return (
        <div>
            {faqData.map((faqItem) => (
                <QuestionItem
                    key={faqItem.id}
                    chatId={chatId}
                    faqItem={faqItem}
                    handleDelete={handleDelete}
                    handleEdit={() => handleEdit(faqItem)}
                    handlePublishDraft={handlePublishDraft}
                    showModal={showModal}
                />
            ))}

            <DeleteModal isVisible={isModalVisible} title={deleteModalTitle} onCancel={handleCancel} onDelete={handleDelete} />
        </div>
    );
}
