import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { faqSelectors } from '@/shared/redux/slices/faqSlice.js';

import { EmptyPageFAQ } from '../EmptyPageFAQ.jsx';
import { FAQuestion } from './FAQuestion';

import styles from '../FAQ.module.css';

export function ListOfPublished() {
    const faqData = useSelector(faqSelectors.selectAll);
    const { t } = useTranslation('faq');

    return (
        <section className={styles.wrapper}>
            <div className={styles.list}>
                <FAQuestion />
                <div className={styles.listModule}>
                    {faqData.length === 0 && (
                        <EmptyPageFAQ title={t('emptyPage.publishListIsEmpty')} />
                    )}
                </div>
            </div>
        </section>
    );
}
