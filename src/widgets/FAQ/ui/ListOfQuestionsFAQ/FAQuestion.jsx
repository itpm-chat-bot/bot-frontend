import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { deleteFAQ, faqSelectors, fetchFAQ } from '@/shared/redux/slices/faqSlice.js';

import { DeleteModal } from '../DeleteModal/DeleteModal';
import { QuestionItem } from '../Question/Question';

export function FAQuestion() {
    const faqData = useSelector(faqSelectors.selectAll);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [selectedItemId, setSelectedItemId] = useState(null);

    const { t } = useTranslation('faq');
    const dispatch = useDispatch();
    const { chatId } = useParams();

    const deleteModalTitle = t('modals.deleteClue');

    const showModal = (id) => {
        setIsModalVisible(true);
        setSelectedItemId(id);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
        setSelectedItemId(null);
    };

    const handleDelete = () => {
        if (selectedItemId !== null) {
            dispatch(deleteFAQ(selectedItemId));
            setIsModalVisible(false);
        }
    };
    useEffect(() => {
        dispatch(fetchFAQ(chatId));
        window.scrollTo(0, 0);
    }, [chatId, dispatch]);

    return (
        <>
            {faqData.map((faqItem) => (
                <QuestionItem
                    key={faqItem.id}
                    chatId={chatId}
                    faqItem={faqItem}
                    handleDelete={handleDelete}
                    showModal={showModal}
                />
            ))}

            <DeleteModal isVisible={isModalVisible} title={deleteModalTitle} onCancel={handleCancel} onDelete={handleDelete} />
        </>
    );
}
