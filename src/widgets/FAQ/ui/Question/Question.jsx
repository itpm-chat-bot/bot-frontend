import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { DeleteIcon, EditIcon, SendIcon } from '@/shared/assets/icons/svg';
import { Button } from '@/shared/ui';

import styles from './Question.module.css';

export const QuestionItem = ({
    faqItem,
    showModal,
    handlePublishDraft,
    handleEdit,
}) => {
    const [isHovered, setIsHovered] = useState(false);
    const { t } = useTranslation('faq');

    return (
        <div
            key={faqItem.id}
            className={styles.questionItem}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
        >
            {faqItem.question && (
                <div className={styles.faqQuestionBlock}>
                    <div className={styles.faqQuestionItem}>{faqItem.question}</div>
                    {faqItem.answer && (
                        <div className={styles.faqAnswer}>{faqItem.answer}</div>
                    )}
                    {faqItem.telegraph_link && (
                        <div className={styles.faqLinkAnswer}>
                            <a href={faqItem.telegraph_link} rel='noopener noreferrer' target='_blank'>
                                {faqItem.telegraph_link}
                            </a>
                        </div>
                    )}
                </div>
            )}
            {isHovered && (
                <div className={styles.buttonItemBlock}>
                    {handlePublishDraft && (
                        <Button
                            leftIcon={<SendIcon title={t('icons.send')} />}
                            size='medium'
                            variant='tertiary'
                            onClick={() => handlePublishDraft(faqItem)}
                        />
                    )}
                    <Button
                        leftIcon={<DeleteIcon title={t('icons.delete')} />}
                        size='medium'
                        variant='tertiary'
                        onClick={(e) => {
                            e.stopPropagation();
                            showModal(faqItem.id);
                        }}
                    />
                    {handleEdit && (
                        <Button
                            leftIcon={<EditIcon title={t('icons.edit')} />}
                            size='medium'
                            variant='tertiary'
                            onClick={() => handleEdit(faqItem.id)}
                        />
                    )}
                </div>
            )}
        </div>
    );
};
