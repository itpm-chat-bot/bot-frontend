import { useTranslation } from 'react-i18next';

import { SelectedSettingsOption } from './SelectedSettingsOption';

import styles from '../Settings/Settings.module.css';

export function CallCommand({ setSelectedNumberOfCalls, initialData }) {
    const { t } = useTranslation('faq');

    const initialOptions = [
        {
            value: 3,
            label: `${t('settings.noMore')} 3 ${t('settings.onceAnHour')}`,
        },
        {
            value: 5,
            label: `${t('settings.noMore')} 5 ${t('settings.onceAnHour')}`,
        },
        {
            value: 10,
            label: `${t('settings.noMore')} 10 ${t('settings.onceAnDay')}`,
        },
    ];

    return (
        <SelectedSettingsOption
            description={(
                <div className={styles.titleBlock}>
                    <h2 className={styles.h2}>{t('settings.numberOfCalls')}</h2>
                    <h4 className={styles.h4}>{t('settings.descriptionNumberOfCalls')}</h4>
                </div>
            )}
            initOptionText={t('settings.oncePeriod')}
            initRadioButtonName='noLimits'
            initText={t('settings.noLimits')}
            initialData={initialData}
            initialOptions={initialOptions}
            periodText={t('settings.oncePeriod')}
            secondRadioButtonName='oncePeriod'
            setOption={setSelectedNumberOfCalls}

        />
    );
}
