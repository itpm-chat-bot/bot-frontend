import { useTranslation } from 'react-i18next';

import { SelectedSettingsOption } from './SelectedSettingsOption';

import styles from '../Settings/Settings.module.css';

export function DeleteCommand({ setSelectDeleteTime, initialData }) {
    const { t } = useTranslation('faq');

    const initialOptions = [
        { value: 300, label: `5 ${t('settings.minutes')}` },
        { value: 1800, label: `30 ${t('settings.minutes')}` },
        { value: 3600, label: `1 ${t('settings.hour')}` },
        { value: 86400, label: `1 ${t('settings.day')}` },
    ];

    return (
        <SelectedSettingsOption
            description={(
                <div className={styles.titleBlock}>
                    <h2 className={styles.h2}>{t('settings.deleteMessageTitle')}</h2>
                    <h4 className={styles.h4}>{t('settings.descriptionDeleteTime')}</h4>
                </div>
            )}
            initOptionText={t('settings.deleteVia')}
            initRadioButtonName='doNotDelete'
            initText={t('settings.doNotDelete')}
            initialData={initialData}
            initialOptions={initialOptions}
            periodText={t('settings.deleteVia')}
            secondRadioButtonName='deleteVia'
            setOption={setSelectDeleteTime}

        />
    );
}
