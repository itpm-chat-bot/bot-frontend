import { useCallback, useEffect, useRef, useState } from 'react';

import { RadioButton } from '@/shared/ui';

import styles from '../Settings/Settings.module.css';

const SelectOption = ({ option, onClick }) => (
    <div
        key={option.value}
        className={styles.option}
        role='button'
        tabIndex={0}
        onClick={() => onClick(option)}
        onKeyDown={(e) => e.key === 'Enter' && onClick(option)}
    >
        {option.label}
    </div>
);

export function SelectedSettingsOption({
    initialOptions,
    initialData,
    setOption,
    periodText,
    description,
    initText,
    initRadioButtonName,
    secondRadioButtonName,
    initOptionText,
}) {
    const [restriction, setRestriction] = useState('');
    const [period, setPeriod] = useState('');
    const [isOpen, setIsOpen] = useState(false);
    const [availableOptions, setAvailableOptions] = useState(initialOptions);
    const initialOptionsRef = useRef(initialOptions);

    useEffect(() => {
        if (initialData === null || initialData === 0) {
            setRestriction('');
            setIsOpen(false);
            setOption(0);
            setPeriod(periodText);
        } else {
            setRestriction('period');
            setOption(initialData);
            const initialOption = initialOptionsRef.current.find((option) => option.value === initialData);
            setPeriod(initialOption ? initialOption.label : periodText);
        }
    }, [initialData, periodText, setOption]);

    const handleCheckboxChange = useCallback((checkedValue) => {
        setRestriction(checkedValue);
        setIsOpen(false);

        if (checkedValue === '') {
            setAvailableOptions(initialOptionsRef.current);
            setPeriod(periodText);
            setOption(0);
        }
        if (checkedValue === 'period') {
            setIsOpen(true);
        }
    }, [periodText, setOption]);

    const handleItemClick = useCallback((value, time) => {
        setPeriod(value);
        setAvailableOptions(initialOptionsRef.current.filter((option) => option.label !== value));
        setOption(time);
    }, [setOption]);

    const handleOptionClick = useCallback((clickedOption) => {
        setPeriod(clickedOption.label);
        setOption(clickedOption.value);
        setIsOpen(true);
    }, [setOption]);

    const toggleSelect = useCallback(() => {
        if (restriction !== '') {
            setIsOpen((prevIsOpen) => !prevIsOpen);
        }
    }, [restriction]);

    return (
        <div className={styles.callsBlock}>
            {description}
            <div className={styles.options}>
                <div className={styles.periodCheckbox} role='group'>
                    <RadioButton
                        checked={restriction === ''}
                        label={initText}
                        name={initRadioButtonName}
                        onChange={() => handleCheckboxChange('')}
                    />
                </div>
                <div
                    className={styles.periodCheckbox}
                    role='button'
                    tabIndex={0}
                    onClick={toggleSelect}
                    onKeyDown={(e) => e.key === 'Enter' && toggleSelect()}
                >
                    <RadioButton
                        checked={restriction === 'period'}
                        label={`${period}` || initOptionText}
                        name={secondRadioButtonName}
                        value={restriction === '' ? initText : period}
                        onChange={() => handleCheckboxChange('period')}
                        onClick={toggleSelect}
                    />
                    {isOpen && (
                        <div className={styles.option}>
                            {availableOptions.map((option) => (
                                <div
                                    key={option.label}
                                    aria-label={option.label}
                                    className={styles.value}
                                    role='button'
                                    tabIndex={0}
                                    onClick={() => handleItemClick(option.label, option.value)}
                                    onKeyDown={(e) => e.key === 'Enter' && handleItemClick(option.label, option.value)}
                                >
                                    <SelectOption option={option} onClick={handleOptionClick} />
                                </div>
                            ))}
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}
