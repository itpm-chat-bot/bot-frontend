import { useTranslation } from 'react-i18next';

import { Button } from '@/shared/ui';

import styles from './Question/Question.module.css';

function SendNowButton({ loading, onClick }) {
    const { t } = useTranslation('faq');

    return (
        <Button
            id='now'
            loading={loading}
            type='button'
            variant='primary'
            onClick={onClick}
        >
            {t('btn.publishButton')}
        </Button>
    );
}

function ScheduleButton({ loading, onClick }) {
    const { t } = useTranslation('faq');

    return (
        <Button
            id='schedule'
            loading={loading}
            type='button'
            variant='secondary'
            onClick={onClick}
        >
            {t('btn.draftButton')}
        </Button>
    );
}

export function BtnGroup({ handlePublishFAQ, handleDraftFAQ }) {
    return (
        <div className={styles.btnGroup}>
            <ScheduleButton onClick={handleDraftFAQ} />
            <SendNowButton onClick={handlePublishFAQ} />
        </div>
    );
}
