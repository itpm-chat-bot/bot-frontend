import styles from './FAQ.module.css';

export function EmptyPageFAQ({ title }) {
    return (
        <div className={styles.emptyTitle}>
            <h3>{title}</h3>
        </div>
    );
}
