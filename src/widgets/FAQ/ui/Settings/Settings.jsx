import { useEffect, useState } from 'react';
import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { commandsSelectors, fetchCommands, updateCommand } from '@/shared/redux/slices/commandsSlice';
import { Button } from '@/shared/ui';

import { SettingsManage } from '../SettingsManage/SettingsManage';

import styles from './Settings.module.css';

export function Settings({ onCancel }) {
    const dispatch = useDispatch();
    const { chatId } = useParams();
    const commands = useSelector(commandsSelectors.selectAll);
    const [selectedNumberOfCalls, setSelectedNumberOfCalls] = useState('');
    const [initialSelectDeleteTime, setInitialSelectDeleteTime] = useState('');
    const [initialNumberOfCalls, setInitialNumberOfCalls] = useState('');
    const [selectDeleteTime, setSelectDeleteTime] = useState('');
    const { t } = useTranslation('faq');

    const callPeriod = selectedNumberOfCalls === 10 ? 'day' : 'hour';

    const filteredCommands = commands.filter((command) => command.commandName === 'faq');
    const faqCommandIds = filteredCommands.map((command) => command.id);

    useEffect(() => {
        dispatch(fetchCommands(chatId));
    }, [chatId, dispatch]);

    const handleUpdateCommand = async () => {
        if (faqCommandIds.length === 0) {
            console.error('No FAQ command found');
            return;
        }

        const updatedCommandData = {
            id: Number(faqCommandIds),
            chat_id: chatId,
            command_name: 'faq',
            command_description: 'Вопросы и ответы',
            command_text: '',
            number_calls: selectedNumberOfCalls,
            call_period: callPeriod,
            delete_after_seconds: selectDeleteTime,
            allowed_access: 'all',
            chat: chatId,
        };

        try {
            await dispatch(updateCommand({
                commandId: updatedCommandData.id,
                commandData: updatedCommandData,
            }));
        } catch (error) {
            console.error('Error updating command:', error);
        }
        onCancel();
        toast.success(t('settings.saveSettingsNotification'));
    };

    const handleCancel = () => {
        onCancel();
        setInitialNumberOfCalls('');
        setInitialSelectDeleteTime('');
        setSelectedNumberOfCalls('');
        setSelectDeleteTime('');
    };
    useEffect(() => {
        const commandItem = commands.find((item) => item.id === Number(faqCommandIds));
        if (commandItem) {
            setInitialSelectDeleteTime(commandItem.deleteAfterSeconds);
            setInitialNumberOfCalls(commandItem.numberCalls);
        }
    }, [commands, faqCommandIds]);

    return (
        <div className={styles.settings}>
            {initialNumberOfCalls !== '' && initialSelectDeleteTime !== '' && (
                <SettingsManage
                    initialNumberOfCalls={initialNumberOfCalls}
                    initialSelectDeleteTime={initialSelectDeleteTime}
                    setSelectDeleteTime={setSelectDeleteTime}
                    setSelectedNumberOfCalls={setSelectedNumberOfCalls}
                />
            )}
            <div className={styles.buttons}>
                <Button key='delete' size='medium' type='button' variant='secondary' onClick={handleCancel}>
                    {t('modals.cancelButton')}
                </Button>
                <Button key='cancel' size='medium' type='button' variant='primary' onClick={handleUpdateCommand}>
                    {t('modals.saveButton')}
                </Button>
            </div>
        </div>
    );
}
