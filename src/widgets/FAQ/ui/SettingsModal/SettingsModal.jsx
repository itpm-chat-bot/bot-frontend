import { Modal } from 'antd';

import { Settings } from '../Settings/Settings';

export function SettingModal({ open, onCancel, onReturn }) {
    return (
        <Modal
            className='custom-modal-settings'
            closable={false}
            footer={null}
            open={open}
        >
            <Settings onCancel={onCancel} onReturn={onReturn} />
        </Modal>
    );
}
