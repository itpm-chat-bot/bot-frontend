import { useTranslation } from 'react-i18next';

import { Modal } from 'antd';

export function DeleteModal({ isVisible, onCancel, onDelete, title }) {
    const { t } = useTranslation('faq');

    return (
        <Modal
            cancelText={t('modals.cancelButton')}
            className='custom-ant-modal'
            closable={false}
            okText={t('modals.deleteButton')}
            open={isVisible}
            title={title}
            centered
            onCancel={onCancel}
            onOk={onDelete}
        />
    );
}
