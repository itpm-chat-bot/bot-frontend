import { useCallback, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';

import { createFAQ, faqSelectors, updateFAQ } from '@/shared/redux/slices/faqSlice';
import { Aside, CommonHead, Container, FormTextArea, Main, RadioButton } from '@/shared/ui';

import { ExitModal } from '../../../onReviewComponents/UseNavigateBlocking/ExitModal';
import { useNavigationBlocking } from '../../../onReviewComponents/UseNavigateBlocking/useNavigationBlocking';
import { rules } from '../../lib/rules';
import { getDefaultExpertsValues } from '../../lib/utils';
import { MAX_LINK_COUNT, MAX_QUESTION_COUNT, MAX_SHORT_ANSWER_COUNT } from '../../model/constants';
import { BtnGroup } from '../BtnGroup';

import styles from './FaqForm.module.css';

export function QuestionForm({ isEditing = false }) {
    const { chatId, faqItemId } = useParams();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { t } = useTranslation('faq');
    const mainText = isEditing ? t('addQuestion.editQuestion') : t('addQuestion.newQuestion');
    const faqInformation = useSelector(faqSelectors.selectAll);
    const { control,
        watch,
        handleSubmit,
        setFocus,
        setValue,
        reset } = useForm({
        defaultValues: getDefaultExpertsValues(),
    });

    const [isShortAnswerVisible, setShortAnswerVisible] = useState(true);
    const [isLinkAnswerVisible, setLinkAnswerVisible] = useState(false);
    const [isShortAnswerChecked, setShortAnswerChecked] = useState(true);
    const [isLinkAnswerChecked, setLinkAnswerChecked] = useState(true);

    const handleAnswerChange = useCallback((shortAnswer) => {
        setShortAnswerVisible(shortAnswer);
        setLinkAnswerVisible(!shortAnswer);
        setShortAnswerChecked(shortAnswer);
        setLinkAnswerChecked(!shortAnswer);
    }, []);

    const watchIsShortAnswerChecked = watch('isShortAnswerChecked');

    const isBlockingCondition = useCallback(() => {
        const { questionValue, answerValue, linkValue } = watch();
        if (isEditing) {
            const draftItem = faqInformation.find((item) => item.id === Number(faqItemId));
            return (

                questionValue.trim() !== draftItem?.question
                || (watchIsShortAnswerChecked && answerValue.trim() !== draftItem?.answer)
                || (!watchIsShortAnswerChecked && linkValue.trim() !== draftItem?.telegraph_link)
            );
        }
        return questionValue || answerValue || linkValue;
        /* eslint eslint-comments/no-use: off */
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [watch]);

    const {
        isModalVisible,
        handleLeavePage,
        handleCancel,
    } = useNavigationBlocking(isBlockingCondition);

    const handleFAQSubmission = useCallback(async (data, isDraft) => {
        const faqData = {
            question: data.questionValue,
            answer: isShortAnswerChecked ? data.answerValue : '',
            chat_id: chatId,
            is_draft: isDraft,
            telegraph_link: isLinkAnswerChecked ? data.linkValue : '',
        };

        try {
            const action = isEditing ? updateFAQ : createFAQ;
            const faqItemIdParam = isEditing ? { faqItemId } : {};
            dispatch(action({ ...faqItemIdParam, faqData }));
            reset();
            navigate(`/chats/${chatId}/faq/${isDraft ? 'draft' : 'published'}`);
            toast.success(isDraft
                ? t('addQuestion.savedToDraft')
                : t('addQuestion.publishedMessage'));
        } catch (error) {
            toast.error('Ошибка при создании FAQ:');
            console.error(error);
        }
    }, [t, dispatch, reset, chatId, faqItemId, isShortAnswerChecked, isLinkAnswerChecked, navigate, isEditing]);

    useEffect(() => {
        const draftItem = faqInformation.find((item) => item.id === Number(faqItemId));
        if (draftItem) {
            setValue('questionValue', draftItem.question);
            setValue('answerValue', draftItem.answer);
            setValue('linkValue', draftItem.telegraph_link);

            if (draftItem.telegraph_link) {
                setLinkAnswerVisible(true);
                setLinkAnswerChecked(true);
                setShortAnswerVisible(false);
                setShortAnswerChecked(false);
            } else {
                setLinkAnswerVisible(false);
                setLinkAnswerChecked(false);
                setShortAnswerVisible(true);
                setShortAnswerChecked(true);
            }
        }
    }, [dispatch, isEditing, faqItemId, setValue, setFocus, chatId, faqInformation]);

    const handleDraftFAQ = handleSubmit((data) => handleFAQSubmission(data, true));
    const handlePublishFAQ = handleSubmit((data) => handleFAQSubmission(data, false));

    return (
        <Main>
            <Container>
                <form className={styles.content}>
                    <CommonHead title={mainText} />
                    <div className={styles.question}>
                        <FormTextArea
                            control={control}
                            label={t('addQuestion.question')}
                            maxLength={MAX_QUESTION_COUNT}
                            name='questionValue'
                            placeholder={t('addQuestion.addQuestionPlaceholder')}
                            rules={rules.questionValue}
                            style={{
                                height: '38px',
                            }}
                        />
                    </div>
                    <div className={styles.answersBlock}>
                        <div className={styles.answer}>
                            <RadioButton
                                checked={isShortAnswerVisible}
                                id='shortAnswer'
                                label={t('addQuestion.shortAnswer')}
                                name='shortAnswer'
                                onChange={() => handleAnswerChange(true)}
                            />
                            {isShortAnswerVisible && (
                                <div className={styles.answerText}>
                                    <h4 className={styles.clue}>
                                        {t('addQuestion.shortAnswerClue')}
                                    </h4>
                                    <FormTextArea
                                        control={control}
                                        maxLength={MAX_SHORT_ANSWER_COUNT}
                                        name='answerValue'
                                        placeholder={t('addQuestion.shortAnswerPlaceholder')}
                                        rules={rules.answerValue}
                                    />
                                </div>
                            )}
                        </div>
                        <div className={styles.answer}>
                            <RadioButton
                                checked={isLinkAnswerVisible}
                                id='linkAnswer'
                                label={t('addQuestion.linkAnswer')}
                                name='linkAnswer'
                                onChange={() => handleAnswerChange(false)}
                            />
                            {isLinkAnswerVisible && (
                                <div className={styles.answerText}>
                                    <h4 className={styles.clue}>
                                        {t('addQuestion.linkAnswerClue')}
                                        <a className={styles.link} href='https://www.notion.so/' rel='noopener noreferrer' target='_blank'>
                                            {' '}
                                            notion
                                            {' '}
                                        </a>

                                        {t('addQuestion.and')}
                                        <a className={styles.link} href='https://telegra.ph/' rel='noopener noreferrer' target='_blank'>
                                            {' '}
                                            telegraph.
                                            {' '}
                                        </a>
                                    </h4>
                                    <FormTextArea
                                        control={control}
                                        maxLength={MAX_LINK_COUNT}
                                        name='linkValue'
                                        placeholder='https://www...'
                                        rules={rules.linkValue}
                                    />
                                </div>
                            )}
                        </div>
                    </div>
                </form>
            </Container>
            <Aside>
                <div className={styles.buttonBlock}>
                    <BtnGroup
                        handleDraftFAQ={handleDraftFAQ}
                        handlePublishFAQ={handlePublishFAQ}
                    />
                </div>
                <ExitModal isVisible={isModalVisible} onCancel={handleCancel} onClose={handleLeavePage} />
            </Aside>
        </Main>
    );
}
