import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Outlet, useLocation, useNavigate, useParams } from 'react-router-dom';

import { nanoid } from '@reduxjs/toolkit';

import { AddIcon, SettingsIcon } from '@/shared/assets/icons/svg';
import { Button, CommonHead, NavTabs } from '@/shared/ui';

import { SettingModal } from './SettingsModal/SettingsModal';

import styles from './FAQ.module.css';

const tabItems = [
    { content: 'tabs.published', id: nanoid(), link: 'published' },
    { content: 'tabs.draft', id: nanoid(), link: 'draft' },
];

export function FAQLayout() {
    const { chatId } = useParams();
    const location = useLocation();
    const [isSettingsModalVisible, setSettingsModalVisible] = useState(false);
    const navigate = useNavigate();
    const { t } = useTranslation('faq');
    const openSettingsModal = () => {
        setSettingsModalVisible(true);
    };

    const closeSettingsModal = () => {
        setSettingsModalVisible(false);
    };

    const shouldRenderNavTabs = location.pathname !== `/chats/${chatId}/faq/question`
        && !location.pathname.startsWith(`/chats/${chatId}/faq/editDraft/`);

    return (
        <section className={styles.leftContainer}>
            {shouldRenderNavTabs && (
                <div className={styles.section}>
                    <div className={styles.head}>
                        <CommonHead
                            subtitle={t('subtitle')}
                            title={t('faq')}
                        />
                        <Button
                            leftIcon={<SettingsIcon />}
                            size='medium'
                            variant='tertiary'
                            onClick={openSettingsModal}
                        />
                    </div>
                    <div className={styles.taskModule}>
                        <NavTabs items={tabItems} namespace='faq' />
                        <div className={styles.rightBlock}>
                            <Button
                                leftIcon={<AddIcon />}
                                size='medium'
                                variant='tertiary'
                                onClick={() => navigate('question')}
                            >
                                {t('tabs.addQuestion')}
                            </Button>
                        </div>
                    </div>
                </div>
            )}
            <Outlet />
            <SettingModal open={isSettingsModalVisible} onCancel={closeSettingsModal} onReturn={closeSettingsModal} />
        </section>
    );
}
