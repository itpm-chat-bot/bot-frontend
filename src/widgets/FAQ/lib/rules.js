import { t } from 'i18next';

import { MAX_QUESTION_COUNT, MAX_SHORT_ANSWER_COUNT } from '../model/constants.js';

export const rules = {
    questionValue: {
        maxLength: {
            value: MAX_QUESTION_COUNT,
            message: `${t('rules.enterNoMore', { ns: 'faq' })} ${MAX_QUESTION_COUNT} ${t('rules.symbols', { ns: 'faq' })}`,
        },
        validate: (value) => {
            if (value.trim() === '') {
                return (t('rules.obligatoryField', { ns: 'faq' }));
            }
            return true;
        },
    },
    answerValue: {
        maxLength: {
            value: MAX_SHORT_ANSWER_COUNT,
            message: `${t('rules.enterNoMore', { ns: 'faq' })} ${MAX_SHORT_ANSWER_COUNT} ${t('rules.symbols', { ns: 'faq' })}`,
        },
        validate: (value) => {
            if (value.trim() === '') {
                return (t('rules.obligatoryField', { ns: 'faq' }));
            }
            return true;
        },
    },
    linkValue: {
        validate: (value) => {
            const telegraphPattern = /^https:\/\/telegra\.ph\//;
            const notionPattern = /^(https:\/\/\S+\.)notion\S*\/.*$/;

            if (value.trim() === '') {
                return (t('rules.obligatoryField', { ns: 'faq' }));
            }

            if (!telegraphPattern.test(value) && !notionPattern.test(value)) {
                return (t('rules.incorrectLink', { ns: 'faq' }));
            }

            return true;
        },
    },

};
