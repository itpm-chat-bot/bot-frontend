export function getDefaultExpertsValues(
    questionValue = '',
    answerValue = '',
    linkValue = '',
    type = 'text',
    value = '',
) {
    return {
        questionValue,
        linkValue,
        answerValue,
        type,
        value,
    };
}
