import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { Switch, Divider } from 'antd';
import PropTypes from 'prop-types';

import { updateServiceMessageSettings } from '@/shared/redux/slices/selectedChatSlice.js';

import styles from './NotificationSettings.module.css';

function ServiceMessageSetting({ label, checked, id, onChange }) {
    return (
        <div className={styles.optionRow}>
            <label className={styles.label} htmlFor={id}>{label}</label>
            <Switch
                checked={checked}
                className='custom-switch'
                id={id}
                size='small'
                onChange={onChange}
            />
        </div>
    );
}

export function NotificationSettings() {
    const chatId = useSelector((state) => state.selectedChat.entity.id);
    const shouldRemoveServiceMessageNewChatMembers = useSelector(
        (state) => state.selectedChat.entity.shouldRemoveServiceMessageNewChatMembers,
    );
    const shouldRemoveServiceMessageLeftChatMembers = useSelector(
        (state) => state.selectedChat.entity.shouldRemoveServiceMessageLeftChatMembers,
    );
    const shouldRemoveServiceMessageNewChatPhoto = useSelector(
        (state) => state.selectedChat.entity.shouldRemoveServiceMessageNewChatPhoto,
    );
    const shouldRemoveServiceMessageNewChatTitle = useSelector(
        (state) => state.selectedChat.entity.shouldRemoveServiceMessageNewChatTitle,
    );
    const shouldRemoveServiceMessageRemovedChatPhoto = useSelector(
        (state) => state.selectedChat.entity.shouldRemoveServiceMessageRemovedChatPhoto,
    );

    const dispatch = useDispatch();

    const { t } = useTranslation('about-chat');

    const settings = [
        {
            label: t('notificationSettings.addUsers'),
            checked: shouldRemoveServiceMessageNewChatMembers,
            fieldName: 'shouldRemoveServiceMessageNewChatMembers',
        },
        {
            label: t('notificationSettings.removedUsers'),
            checked: shouldRemoveServiceMessageLeftChatMembers,
            fieldName: 'shouldRemoveServiceMessageLeftChatMembers',
        },
        {
            label: t('notificationSettings.changeName'),
            checked: shouldRemoveServiceMessageNewChatTitle,
            fieldName: 'shouldRemoveServiceMessageNewChatTitle',
        },
        {
            label: t('notificationSettings.removeAvatar'),
            checked: shouldRemoveServiceMessageRemovedChatPhoto,
            fieldName: 'shouldRemoveServiceMessageRemovedChatPhoto',
        },
        {
            label: t('notificationSettings.updateAvatar'),
            checked: shouldRemoveServiceMessageNewChatPhoto,
            fieldName: 'shouldRemoveServiceMessageNewChatPhoto',
        },
    ];

    const handleServiceMessageSettingChange = (fieldName) => (checked, _e) => {
        dispatch(updateServiceMessageSettings({ chatId, settings: { [fieldName]: checked } }));
    };

    const handleAllServiceMessageSettingsChange = (checked, _e) => {
        const fields = settings.map(({ fieldName }) => [fieldName, checked]);
        dispatch(updateServiceMessageSettings({ chatId, settings: Object.fromEntries(fields) }));
    };

    return (
        <section>
            <h2 className={styles.title}>{t('notificationSettings.title')}</h2>

            <form>
                <fieldset className={styles.fieldset}>
                    <div className={styles.optionRowsContainer}>
                        <ServiceMessageSetting
                            checked={settings.every((s) => s.checked === true)}
                            id='removeAllServiceMessages'
                            label={t('notificationSettings.selectAll')}
                            onChange={handleAllServiceMessageSettingsChange}
                        />

                        <Divider />

                        {settings.map(({ label, checked, fieldName }) => (
                            <ServiceMessageSetting
                                key={fieldName}
                                checked={checked}
                                id={fieldName}
                                label={label}
                                onChange={handleServiceMessageSettingChange(fieldName)}
                            />
                        ))}
                    </div>
                </fieldset>
            </form>
        </section>
    );
}

ServiceMessageSetting.propTypes = {
    label: PropTypes.string,
    checked: PropTypes.bool,
    id: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
};

ServiceMessageSetting.defaultProps = {
    label: '',
    checked: undefined,
};
