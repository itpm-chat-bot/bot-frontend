import { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Avatar } from '@/entities/Avatar';
import { LinkIcon } from '@/shared/assets/icons/svg';
import { getInitials } from '@/shared/lib/util';
import { ageCountEntitySelector, fetchAgeCount } from '@/shared/redux/slices/chatAgeCountSlice';
import { selectedChatEntitySelector } from '@/shared/redux/slices/selectedChatSlice';
import { fetchUsersCount, usersCountEntitySelector } from '@/shared/redux/slices/usersCountSlice';

import { calculateActiveUsersPercent } from '../../lib/calculateActiveUsersPercent';

import styles from './MetaInfo.module.css';

export function MetaInfo() {
    const { tgId: tgChatId, avatar, title, participantsNumber, description, inviteLink } = useSelector(selectedChatEntitySelector);
    const { generalNumberOfActiveUsers, activeLast24HoursUsers } = useSelector(usersCountEntitySelector);
    const { days, months, years } = useSelector(ageCountEntitySelector);
    const copyMessageRef = useRef(null);
    const { t } = useTranslation('about-chat');
    const dispatch = useDispatch();

    const activeUsersPercent = calculateActiveUsersPercent(generalNumberOfActiveUsers, participantsNumber);

    const activeUsersCount = `${generalNumberOfActiveUsers} (${activeUsersPercent})`;

    const handleCopyLink = async () => {
        if (!copyMessageRef.current) return;
        try {
            await navigator.clipboard.writeText(inviteLink);
            copyMessageRef.current.innerText = t('copied');
            setTimeout(() => {
                copyMessageRef.current.innerText = t('copyLink');
            }, 1000);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        if (tgChatId) {
            dispatch(fetchUsersCount(tgChatId));
            dispatch(fetchAgeCount(tgChatId));
        }
    }, [dispatch, tgChatId]);

    return (
        <section className={styles.section}>
            <div className={styles.firstContainer}>
                <Avatar
                    alt=''
                    fallbackText={getInitials(title)}
                    size='big'
                    src={avatar}
                />
                <div className={styles.chatInfo}>
                    <h3>{title}</h3>
                    <p>{description}</p>
                    <button
                        className={styles.inviteLink}
                        type='button'
                        onClick={handleCopyLink}
                    >
                        <LinkIcon />
                        <span ref={copyMessageRef}>{t('copyLink')}</span>
                    </button>
                </div>
            </div>

            <div className={styles.secondContainer}>
                <div className={styles.infoRow}>
                    <p className={styles.info}>
                        <span>{tgChatId}</span>
                        <span>{t('chatId')}</span>
                    </p>
                    <p className={styles.info}>
                        <span>
                            {days !== 0 && `${t('days', { count: days })} `}
                            {months !== 0 && `${t('months', { count: months })} `}
                            {years !== 0 && `${t('years', { count: years })} `}
                        </span>
                        <span>{t('botInChat')}</span>
                    </p>
                </div>

                <div className={styles.infoRow}>
                    <p className={styles.info}>
                        <span>{participantsNumber}</span>
                        <span>{t('participants')}</span>
                    </p>
                    <p className={styles.info}>
                        <span className={styles.red}>-10</span>
                        <span>{t('perDay')}</span>
                    </p>
                    <p className={styles.info}>
                        <span>{activeUsersCount}</span>
                        <span>{t('active')}</span>
                    </p>
                    <p className={styles.info}>
                        <span className={styles.green}>
                            +
                            {activeLast24HoursUsers}
                        </span>
                        <span>{t('perDay')}</span>
                    </p>
                </div>
            </div>
        </section>
    );
}
