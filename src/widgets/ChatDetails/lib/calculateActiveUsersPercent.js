export const calculateActiveUsersPercent = (generalNumberOfActiveUsers, participantsNumber) => {
    if (participantsNumber <= 0) return '0%';

    const percent = ((generalNumberOfActiveUsers * 100) / participantsNumber).toFixed(0);
    return percent > 0 && percent < 1 ? '<1%' : `${percent}%`;
};
