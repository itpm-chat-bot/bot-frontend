import { useTranslation } from 'react-i18next';

import coin1 from '@/shared/assets/coin1.png';
import coin2 from '@/shared/assets/coin2.png';
import coin3 from '@/shared/assets/coin3.png';

import styles from './WelcomeIllustration.module.css';

export function WelcomeIllustration() {
    const { t: tMain } = useTranslation('main');
    return (
        <div className={styles.container}>
            <img alt='coin1' className={styles.coinLarge} src={coin1} />
            <div className={styles.textContainer}>
                <p className={styles.mainTitle}>
                    {tMain('title')}
                </p>
                <h1 className={styles.mainSubtitle}>
                    {tMain('illustrationText')}
                </h1>
            </div>
            <img alt='coin2' className={styles.coinMedium} src={coin2} />
            <img alt='coin3' className={styles.coinSmall} src={coin3} />
        </div>
    );
}
