import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { MessageCard } from '@/entities/MessageCard/index.js';
import { DeleteMessageButton } from '@/features/DeleteMessage/index.js';
import { EditMessageButton } from '@/features/EditMessage/index.js';
import { DAY_MONTH_YEAR_VIA_DOT_AND_TIME_VIA_COLON_FORMAT } from '@/shared/constants/date-formats.js';
import { useMessagesContext, formatISOtimeByTimezone } from '@/shared/lib';
import { deleteMessageById } from '@/shared/redux/slices/messagesSlice.js';

import styles from './List.module.css';

export function List() {
    const { selectors, matchMessagesHistoryRoutePath, matchScheduledMessagesRoutePath } = useMessagesContext();
    const messages = useSelector(selectors.selectAll);
    const timezone = useSelector((state) => state.selectedChat.entity.timezone);

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { t } = useTranslation('messages');

    const mappedMessages = messages.map((item) => (
        !item.title
            ? { ...item, title: '<без текста>' }
            : item
    ));

    const fallbackText = matchMessagesHistoryRoutePath
        ? t('noSentMessages')
        : t('noScheduledMessages');

    const handleEditBtnClick = (id) => {
        let segment = '';

        if (matchMessagesHistoryRoutePath) {
            segment = 'sent';
        }

        if (matchScheduledMessagesRoutePath) {
            segment = 'scheduled';
        }

        navigate(`../${segment}/${id}/edit`);
    };

    if (mappedMessages.length === 0) {
        return <p className={styles.fallback}>{fallbackText}</p>;
    }

    return (
        <ul className={styles.list}>
            {mappedMessages.map((item) => (
                <li key={item.id}>
                    <MessageCard
                        text={item.title}
                        time={formatISOtimeByTimezone(
                            matchMessagesHistoryRoutePath ? item.actualSendTime : item.sendTime,
                            timezone,
                            DAY_MONTH_YEAR_VIA_DOT_AND_TIME_VIA_COLON_FORMAT,
                        )}
                        toolbarButtons={(
                            <>
                                <EditMessageButton
                                    disabled={matchMessagesHistoryRoutePath}
                                    onClick={() => handleEditBtnClick(item.id)}
                                />
                                <DeleteMessageButton
                                    disabled={matchMessagesHistoryRoutePath}
                                    onConfirm={() => dispatch(deleteMessageById(item.id))}
                                />
                            </>
                        )}
                    />
                </li>
            ))}
        </ul>
    );
}
