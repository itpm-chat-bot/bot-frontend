import { rUrl } from '@/shared/constants/regexp.js';
import { MAX_URL_LENGTH } from '@/shared/constants/urls.js';

import { MAX_BTN_HINT_VALUE_LENGTH, MAX_BTN_NAME_LENGTH } from './constants.js';

export const rules = {
    name: {
        required: {
            value: true,
            message: {
                key: 'validation.required',
                options: { ns: 'common' },
            },
        },
        maxLength: {
            value: MAX_BTN_NAME_LENGTH,
            message: {
                key: 'validation.maxLength',
                options: { count: MAX_BTN_NAME_LENGTH, ns: 'common' },
            },
        },
        validate: {
            maxLength: (value) => value.length <= MAX_BTN_NAME_LENGTH || {
                key: 'validation.maxLength',
                options: { count: MAX_BTN_NAME_LENGTH, ns: 'common' },
            },
        },
    },
    link: {
        required: {
            value: true,
            message: {
                key: 'validation.required',
                options: { ns: 'common' },
            },
        },
        maxLength: {
            value: MAX_URL_LENGTH,
            message: {
                key: 'validation.maxLength',
                options: { count: MAX_URL_LENGTH, ns: 'common' },
            },
        },
        pattern: {
            value: rUrl,
            message: {
                key: 'validation.invalidUrl',
                options: { ns: 'common' },
            },
        },
    },
    hint: {
        required: {
            value: true,
            message: {
                key: 'validation.required',
                options: { ns: 'common' },
            },
        },
        maxLength: {
            value: MAX_BTN_HINT_VALUE_LENGTH,
            message: {
                key: 'validation.maxLength',
                options: { count: MAX_BTN_HINT_VALUE_LENGTH, ns: 'common' },
            },
        },
    },
};
