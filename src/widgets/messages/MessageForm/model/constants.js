/* FOR rich text editor */
export const MAX_TEXT_LENGTH = 4095;
export const MAX_TEXT_LENGTH_WITH_ATTACHMENTS = 1023;

/* FOR file attachment processing */
export const MAX_IMAGE_WIDTH_PX = 5000; // px
export const MAX_IMAGE_HEIGHT_PX = 3000; // px
export const MAX_IMAGE_SIZE_MB = 10; // megabytes
export const MAX_FILES_COUNT = 10;
export const MAX_FILES_COUNT_WITH_BTN_ROWS = 1;
export const MAX_FILES_TOTAL_SIZE_GB = 2; // gigabytes
export const MIN_FILE_SIZE_B = 1; // bytes

/* FOR action button rows and their modals */
export const MAX_BTN_COUNT_PER_ROW = 2;
export const MAX_BTN_NAME_LENGTH = 32;
export const MAX_BTN_HINT_VALUE_LENGTH = 200;

/* FOR time */
export const MAX_ALLOWED_EDITING_TIME_BEFORE_SENDING_MIN = 1; // minute
export const CORRECTION_TIME_WHEN_PICK_CALENDAR_MIN = 5; // minute
