export const BUTTON_VALUE_FIELD_CONFIG = {
    link: {
        label: 'buttonConfig.link.label',
        placeholder: 'buttonConfig.link.placeholder',
    },
    hint: {
        label: 'buttonConfig.hint.label',
        placeholder: 'buttonConfig.hint.placeholder',
    },
};

export const MODAL_DEFAULT_VALUES = {
    name: '',
    type: 'link',
    value: '',
};
