import PropTypes from 'prop-types';

import { MAX_BTN_COUNT_PER_ROW } from '../../model/constants.js';
import { ActionButton } from '../Buttons/ActionBtn.jsx';
import { PlusButton } from '../Buttons/PlusBtn.jsx';

import styles from './ActionButtonRow.module.css';

export function ActionButtonRow({ row, actionButtonRowsAPI }) {
    const { rowId, buttons } = row;

    return (
        <div className={styles.row}>
            {buttons.map((btn) => (
                <ActionButton
                    key={btn.id}
                    actionButtonRowsAPI={actionButtonRowsAPI}
                    btn={btn}
                    currentRowId={rowId}
                />
            ))}

            {buttons.length < MAX_BTN_COUNT_PER_ROW && (
                <PlusButton
                    actionButtonRowsAPI={actionButtonRowsAPI}
                    currentBtnCountInRow={buttons.length}
                    currentRowId={rowId}
                />
            )}
        </div>
    );
}

/* eslint eslint-comments/no-use: off */
ActionButtonRow.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    row: PropTypes.object.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    actionButtonRowsAPI: PropTypes.object.isRequired,
};
