import { useDispatch } from 'react-redux';

import PropTypes from 'prop-types';

import { CloseIcon } from '@/shared/assets/icons/svg';
import { deleteFileById } from '@/shared/redux/slices/messagesSlice.js';

import styles from './Attachments.module.css';

export function Attachments({ FileListAPI }) {
    const { fileList, setFileList } = FileListAPI;

    const dispatch = useDispatch();

    const handleClick = ({ id, fromBackend }) => {
        const updatedList = fileList.filter((value) => value.id !== id);

        setFileList(updatedList);

        if (fromBackend) {
            dispatch(deleteFileById(id));
        }
    };

    return (
        <ul className={styles.list}>
            {fileList.map((item) => (
                <li key={item.id}>
                    {item.file.name}
                    <CloseIcon className={styles.closeIcon} onClick={() => handleClick(item)} />
                </li>
            ))}
        </ul>
    );
}

/* eslint eslint-comments/no-use: off */
Attachments.propTypes = {
    FileListAPI: PropTypes.shape({
        // eslint-disable-next-line react/forbid-prop-types
        fileList: PropTypes.array.isRequired,
        setFileList: PropTypes.func.isRequired,
    }).isRequired,
};
