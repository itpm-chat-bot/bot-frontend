import { Controller, useFormContext } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { TimePicker, Divider } from 'antd';
import dayjs from 'dayjs';
import PropTypes from 'prop-types';

import { CalendarIcon } from '@/shared/assets/icons/svg';
import { CUSTOM_DATE_FORMAT } from '@/shared/constants/date-formats.js';
import { DatePickerWithoutCaching, ErrorText } from '@/shared/ui/index.js';

import { disablePeriod, checkTimeLaterThanCurrent } from '../../../lib/index.js';
import { CORRECTION_TIME_WHEN_PICK_CALENDAR_MIN } from '../../../model/constants.js';

import optionsStyles from '../Options.module.css';
import styles from './PlannedSendingTime.module.css';

export function PlannedSendingTime({ clickedSubmitBtnId }) {
    const { control, formState: { errors } } = useFormContext();
    const timezone = useSelector((state) => state.selectedChat.entity.timezone);
    const { t } = useTranslation('messages');

    return (
        <section className={styles.rows}>
            <div className={optionsStyles.optionsSectionColumnRow}>
                <div className={optionsStyles.labelRow}>
                    <label className={optionsStyles.label} htmlFor='plannedSendingTime'>
                        {t('messageSendingTime')}
                    </label>
                </div>

                <Divider />
            </div>

            <div className={styles.timezone}>
                {dayjs().tz(timezone).format(`[(UTC]Z[) ${timezone}]`)}
            </div>

            <div>
                <div className={optionsStyles.pickersRow}>
                    <Controller
                        control={control}
                        name='plannedSendingTime'
                        render={({
                            field,
                        }) => (
                            <>
                                <DatePickerWithoutCaching
                                    allowClear={false}
                                    className='custom-date-picker'
                                    disabledDate={disablePeriod}
                                    format={CUSTOM_DATE_FORMAT}
                                    id='plannedSendingTime'
                                    popupClassName='custom-date-picker-popup'
                                    showToday={false}
                                    status={errors.plannedSendingTime ? 'error' : null}
                                    suffixIcon={<CalendarIcon />}
                                    value={field.value}
                                    inputReadOnly
                                    {...field}
                                    onChange={(dateTimeObj) => (
                                        field.value
                                            ? field.onChange(dateTimeObj)
                                            : field.onChange(dateTimeObj.startOf('minute').add(CORRECTION_TIME_WHEN_PICK_CALENDAR_MIN, 'minute'))
                                    )}
                                />
                                <TimePicker
                                    allowClear={false}
                                    className='custom-time-picker'
                                    placeholder='00:00:00'
                                    popupClassName='custom-time-picker-popup'
                                    showNow={false}
                                    status={errors.plannedSendingTime ? 'error' : null}
                                    suffixIcon={null}
                                    inputReadOnly
                                    {...field}
                                />
                            </>
                        )}
                        rules={{
                            required: clickedSubmitBtnId === 'schedule',
                            validate: {
                                laterThanCurrent: checkTimeLaterThanCurrent,
                            },
                        }}
                    />
                </div>

                {errors.plannedSendingTime
                    && <ErrorText>{errors.plannedSendingTime.message}</ErrorText>}
            </div>
        </section>
    );
}

/* eslint eslint-comments/no-use: off */
PlannedSendingTime.propTypes = {
    clickedSubmitBtnId: PropTypes.string,
};

PlannedSendingTime.defaultProps = {
    clickedSubmitBtnId: null,
};
