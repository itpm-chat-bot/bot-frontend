import PropTypes from 'prop-types';

import { AddIcon } from '@/shared/assets/icons/svg';
import { Button } from '@/shared/ui';

import { MessageDelete } from './MessageDelete/MessageDelete';
import { MessagePin } from './MessagePin/MessagePin';
import { PlannedSendingTime } from './PlannedSendingTime/PlannedSendingTime';

import styles from './Options.module.css';

export function Options(props) {
    const { clickedSubmitBtnId, handleAddRowBtnClick } = props;

    return (
        <div className={styles.optionsContainer}>
            <section className={styles.optionsSection}>
                <div className={styles.optionsSectionColumn}>
                    <MessagePin />
                    <MessageDelete />
                </div>

                <div className={styles.optionsSectionColumn}>
                    <PlannedSendingTime
                        clickedSubmitBtnId={clickedSubmitBtnId}
                    />
                </div>
            </section>

            <Button
                size='medium'
                variant='secondary'
                onClick={handleAddRowBtnClick}
            >
                <AddIcon />
            </Button>
        </div>
    );
}

/* eslint eslint-comments/no-use: off */
Options.propTypes = {
    clickedSubmitBtnId: PropTypes.string,
    handleAddRowBtnClick: PropTypes.func.isRequired,
};

Options.defaultProps = {
    clickedSubmitBtnId: null,
};
