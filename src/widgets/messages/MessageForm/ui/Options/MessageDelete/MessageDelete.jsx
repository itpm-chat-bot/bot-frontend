import { Controller, useFormContext } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { Divider, TimePicker } from 'antd';

import { HelpIcon, CalendarIcon } from '@/shared/assets/icons/svg';
import { CUSTOM_DATE_FORMAT } from '@/shared/constants/date-formats.js';
import { DatePickerWithoutCaching, ErrorText, FormSwitch } from '@/shared/ui';

import { disablePeriod, checkTimeLaterThanCurrent, checkDeletingTimeNecessary, checkDeletingTimeLaterThanPlanned } from '../../../lib/index.js';
import { CORRECTION_TIME_WHEN_PICK_CALENDAR_MIN } from '../../../model/constants.js';

import optionsStyles from '../Options.module.css';
import styles from './MessageDelete.module.css';

export function MessageDelete() {
    const { control, watch, getValues, resetField, formState: { errors } } = useFormContext();

    const { t } = useTranslation('messages');

    const watchSwitcher = watch('shouldMessageDelete');

    // "checked" comes as a first parameter, "event" as a second one (https://ant.design/components/switch#api)
    const handleSwitch = (checked) => {
        if (checked === false) resetField('deletingTime');
    };

    return (
        <section>
            <div className={optionsStyles.optionsSectionColumnRow}>
                <div className={optionsStyles.labelRow}>
                    <label className={optionsStyles.label} htmlFor='messageDelete'>
                        {t('deleteMessage')}
                        <HelpIcon title={t('deleteMessageInfoIcon')} />
                    </label>

                    <FormSwitch control={control} id='messageDelete' name='shouldMessageDelete' onClick={handleSwitch} />
                </div>

                <Divider />
            </div>

            {watchSwitcher && (
                <div className={styles.deletingTimeRow}>
                    <div className={optionsStyles.pickersRow}>
                        <Controller
                            control={control}
                            name='deletingTime'
                            render={({
                                field,
                            }) => (
                                <>
                                    <DatePickerWithoutCaching
                                        allowClear={false}
                                        className='custom-date-picker'
                                        disabledDate={disablePeriod}
                                        format={CUSTOM_DATE_FORMAT}
                                        popupClassName='custom-date-picker-popup'
                                        showToday={false}
                                        status={errors.deletingTime ? 'error' : null}
                                        suffixIcon={<CalendarIcon />}
                                        inputReadOnly
                                        {...field}
                                        onChange={(dateTimeObj) => {
                                            if (!field.value) {
                                                const plannedSendingTime = getValues('plannedSendingTime');
                                                return plannedSendingTime
                                                    ? field.onChange(plannedSendingTime.add(CORRECTION_TIME_WHEN_PICK_CALENDAR_MIN, 'minute'))
                                                    : field.onChange(dateTimeObj.startOf('minute').add(CORRECTION_TIME_WHEN_PICK_CALENDAR_MIN, 'minute'));
                                            }

                                            return field.onChange(dateTimeObj);
                                        }}
                                    />
                                    <TimePicker
                                        allowClear={false}
                                        className='custom-time-picker'
                                        placeholder='00:00:00'
                                        popupClassName='custom-time-picker-popup'
                                        showNow={false}
                                        status={errors.deletingTime ? 'error' : null}
                                        suffixIcon={null}
                                        inputReadOnly
                                        {...field}
                                    />
                                </>
                            )}
                            rules={{
                                validate: {
                                    checkNecessary: checkDeletingTimeNecessary,
                                    laterThanCurrent: checkTimeLaterThanCurrent,
                                    laterThanPlanned: checkDeletingTimeLaterThanPlanned(getValues('plannedSendingTime')),
                                },
                            }}
                        />
                    </div>

                    {errors.deletingTime
                        && <ErrorText>{errors.deletingTime.message}</ErrorText>}
                </div>
            )}
        </section>
    );
}
