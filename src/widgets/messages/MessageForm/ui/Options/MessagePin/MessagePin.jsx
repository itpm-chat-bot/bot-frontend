import { useFormContext } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { FormSwitch } from '@/shared/ui';

import optionsStyles from '../Options.module.css';

export function MessagePin() {
    const { control } = useFormContext();
    const { t } = useTranslation('messages');

    return (
        <section>
            <div className={optionsStyles.optionsSectionColumnRow}>
                <div className={optionsStyles.labelRow}>
                    <label className={optionsStyles.label} htmlFor='pin'>
                        {t('pinMessage')}
                    </label>

                    <FormSwitch control={control} id='pin' name='shouldMessagePin' size='small' />
                </div>
            </div>
        </section>
    );
}
