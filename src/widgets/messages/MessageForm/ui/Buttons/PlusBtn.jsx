import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { nanoid } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';

import { AddIcon } from '@/shared/assets/icons/svg';
import { Button } from '@/shared/ui';

import { Modal } from '../Modal/Modal';

import styles from './Buttons.module.css';

export function PlusButton({ currentRowId, currentBtnCountInRow, actionButtonRowsAPI }) {
    const { t } = useTranslation('messages');
    // Modal is opened automatically if there is no buttons in the row
    const [isModalOpen, setIsModalOpen] = useState(currentBtnCountInRow === 0);
    const { actionButtonRows, setActionButtonRows } = actionButtonRowsAPI;

    const toggleModalVisibility = () => setIsModalOpen((prev) => !prev);

    const handleSave = (data) => {
        const rowIndex = actionButtonRows.findIndex(({ rowId }) => rowId === currentRowId);
        if (rowIndex !== -1) {
            setActionButtonRows((draft) => {
                draft[rowIndex].buttons.push({ ...data, id: nanoid() });
            });
        }

        toggleModalVisibility();
    };

    const handleCancel = () => {
        if (currentBtnCountInRow === 0) {
            const filteredRows = actionButtonRows.filter(({ rowId }) => rowId !== currentRowId);
            setActionButtonRows(filteredRows);
        }

        toggleModalVisibility();
    };

    return (
        <>
            <div className={styles.button}>
                <Button
                    size='medium'
                    variant='secondary'
                    onClick={toggleModalVisibility}
                >
                    <AddIcon />
                </Button>
            </div>

            <Modal
                isOpen={isModalOpen}
                title={t('modal.titles.addButton')}
                onCancel={handleCancel}
                onSave={handleSave}
            />
        </>
    );
}

/* eslint eslint-comments/no-use: off */
PlusButton.propTypes = {
    currentBtnCountInRow: PropTypes.number.isRequired,
    currentRowId: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    actionButtonRowsAPI: PropTypes.object.isRequired,
};
