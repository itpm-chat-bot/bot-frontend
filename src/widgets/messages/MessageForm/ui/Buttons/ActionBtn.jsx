import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import PropTypes from 'prop-types';

import { CloseIcon } from '@/shared/assets/icons/svg';
import { Button } from '@/shared/ui';

import { Modal } from '../Modal/Modal';

import styles from './Buttons.module.css';

export function ActionButton({ btn, currentRowId, actionButtonRowsAPI }) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const { actionButtonRows, setActionButtonRows } = actionButtonRowsAPI;
    const { t } = useTranslation('messages');

    const values = {
        name: btn.name,
        type: btn.type,
        value: btn.value,
    };

    const toggleModalVisibility = () => setIsModalOpen((prev) => !prev);

    const handleSave = (data) => {
        setActionButtonRows((draft) => {
            const row = draft.find(({ rowId }) => rowId === currentRowId);
            const button = row.buttons.find(({ id }) => id === btn.id);

            Object.assign(button, data);
        });

        toggleModalVisibility();
    };

    const handleCancel = () => {
        toggleModalVisibility();
    };

    const handleRemoveClick = () => {
        const remainActionButtonRows = actionButtonRows.reduce((acc, row) => {
            if (row.rowId !== currentRowId) {
                acc.push(row);
                return acc;
            }

            const remainingButtons = row.buttons.filter(({ id }) => id !== btn.id);
            if (remainingButtons.length > 0) {
                acc.push({ ...row, buttons: remainingButtons });
            }
            return acc;
        }, []);

        setActionButtonRows(remainActionButtonRows);
    };

    return (
        <>
            <div className={styles.button}>
                <Button
                    size='medium'
                    variant='secondary'
                    onClick={toggleModalVisibility}
                >
                    {btn.name}
                </Button>

                <CloseIcon onClick={handleRemoveClick} />
            </div>

            <Modal
                isOpen={isModalOpen}
                title={t('modal.titles.actionBtn')}
                values={values}
                onCancel={handleCancel}
                onSave={handleSave}
            />
        </>
    );
}

/* eslint eslint-comments/no-use: off */
ActionButton.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    btn: PropTypes.object.isRequired,
    currentRowId: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    actionButtonRowsAPI: PropTypes.object.isRequired,
};
