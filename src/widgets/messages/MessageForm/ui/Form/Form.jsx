import { useCallback, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useLoaderData, useNavigate, useParams } from 'react-router-dom';

import { nanoid } from '@reduxjs/toolkit';
import dayjs from 'dayjs';
import { useImmer } from 'use-immer';

import { httpClient } from '@/app/services/http-client.js';
import { routes } from '@/shared/api/routes.js';
import { AddIcon } from '@/shared/assets/icons/svg';
import { useEditor, useMessagesContext, isEmpty, toast } from '@/shared/lib';
import { Button, FormRichTextEditor } from '@/shared/ui';

import { calculateTotalSizeGB, validateEditorMaxLength, validateEditorMinLength } from '../../lib';
import {
    MAX_ALLOWED_EDITING_TIME_BEFORE_SENDING_MIN,
    MAX_FILES_COUNT,
    MAX_FILES_COUNT_WITH_BTN_ROWS,
    MAX_FILES_TOTAL_SIZE_GB,
} from '../../model/constants.js';
import { ActionButtonRow } from '../ActionButtonRow/ActionButtonRow.jsx';
import { Attachments } from '../Attachments/Attachments.jsx';
import { FileInputs } from '../FileInputs/FileInputs.jsx';
import { MessagePin } from '../Options/MessagePin/MessagePin';

import styles from './Form.module.css';

export function Form() {
    const {
        control, setValue, setError, reset, handleSubmit, formState,
    } = useForm({
        defaultValues: {
            text: '',
            shouldMessagePin: false,
            shouldMessageDelete: false,
            deletingTime: null,
            plannedSendingTime: null,
        },
    });

    const { isSubmitSuccessful } = formState;

    const chat = useSelector((state) => state.selectedChat.entity);
    const [fileList, setFileList] = useState([]);
    const [actionButtonRows, setActionButtonRows] = useImmer([]);
    const [clickedSubmitBtnId, _setClickedSubmitBtnId] = useState(null);
    const loadedData = useLoaderData();
    const { matchMessageCreateRoutePath, matchScheduledMessageEditRoutePath } = useMessagesContext();

    const { messageId } = useParams();
    const navigate = useNavigate();

    const { editorInstance } = useEditor();
    const { t } = useTranslation('messages');

    const showMaxFilesCountWithBtnRowsError = () => {
        toast.error(`При наличии ряда кнопок отправить можно не более ${MAX_FILES_COUNT_WITH_BTN_ROWS} файла. Попробуйте еще раз`);
    };

    const showMaxFilesCountError = () => {
        toast.error(`Отправить можно не более ${MAX_FILES_COUNT} файлов. Попробуйте еще раз`);
    };

    const showMaxFilesTotalSizeError = () => {
        toast.error(`Итоговый размер прикрепленных файлов должен быть не более ${MAX_FILES_TOTAL_SIZE_GB} ГБ. Попробуйте еще раз`);
    };

    const showTooLateSendingError = () => {
        toast.error('Редактирование сообщения невозможно. Сообщение вероятно уже было отправлено или будет вскоре отправлено');
    };

    const resetForm = useCallback(() => {
        reset();
        setFileList([]);
        setActionButtonRows([]);
    }, [reset, setActionButtonRows]);

    // const handleSendNowBtnClick = () => {
    //     setClickedSubmitBtnId('now');
    //     resetField('plannedSendingTime');
    // };
    // const handleScheduleBtnClick = () => setClickedSubmitBtnId('schedule');

    const hasAtLeastOneAttachment = !isEmpty(fileList);
    const hasAtLeastOneActionButton = actionButtonRows.some((row) => !isEmpty(row.buttons));
    const isMaxFilesCountWithBtnRowsExceeded = fileList.length > MAX_FILES_COUNT_WITH_BTN_ROWS;
    const isMaxFilesCountExceeded = fileList.length > MAX_FILES_COUNT;
    const isFilesTotalSizeExceeded = calculateTotalSizeGB(fileList) > MAX_FILES_TOTAL_SIZE_GB;

    const onSubmit = async (submittingData) => {
        const { text, shouldMessagePin, shouldMessageDelete, deletingTime, plannedSendingTime } = submittingData;

        const currentTime = dayjs();
        const sendingTime = clickedSubmitBtnId === 'schedule' ? plannedSendingTime : currentTime;

        if (hasAtLeastOneActionButton && isMaxFilesCountWithBtnRowsExceeded) {
            setError('root.isMaxFilesCountWithBtnRowsExceeded', { type: 'maxFilesCountWithBtnRowsExceeded' });
            showMaxFilesCountWithBtnRowsError();
            return;
        }

        if (isMaxFilesCountExceeded) {
            setError('root.isMaxFilesCountExceeded', { type: 'maxFilesCountExceeded' });
            showMaxFilesCountError();
            return;
        }

        if (isFilesTotalSizeExceeded) {
            setError('root.isFilesTotalSizeExceeded', { type: 'filesTotalSizeExceeded' });
            showMaxFilesTotalSizeError();
            return;
        }

        // This doesn't allow to send edited scheduled message if user send edited message less than MAX_ALLOWED_EDITING_TIME_BEFORE_SENDING_MIN to already scheduled time
        if (matchScheduledMessageEditRoutePath && loadedData) {
            const alreadyScheduledTime = dayjs(loadedData.message.sendTime);
            const alreadyScheduledTimeWithCorrection = alreadyScheduledTime.subtract(MAX_ALLOWED_EDITING_TIME_BEFORE_SENDING_MIN, 'minute');
            const isTooLate = currentTime.isSameOrAfter(alreadyScheduledTimeWithCorrection);

            if (isTooLate) {
                setError('root.isTooLate', { type: 'tooLate' });
                showTooLateSendingError();
                return;
            }
        }

        const formData = new FormData();
        formData.append('chat', chat.id);
        formData.append('text', text);
        formData.append('pin_message', shouldMessagePin);
        if (!sendingTime.isSame(currentTime)) {
            formData.append('send_time', plannedSendingTime.tz(chat.timezone, true).format());
        } else {
            formData.append('send_time', null);
        }
        if (shouldMessageDelete && deletingTime?.isValid()) {
            formData.append('delete_time', deletingTime.tz(chat.timezone, true).format());
        }
        if (hasAtLeastOneActionButton) {
            const nonEmptyRows = actionButtonRows.filter(({ buttons }) => !isEmpty(buttons));
            formData.append('buttons', JSON.stringify((nonEmptyRows)));
        }

        let filesPromises;
        if (hasAtLeastOneAttachment) {
            // Different ways to send attachments depending on current route
            if (matchMessageCreateRoutePath) {
                fileList.forEach((f, idx) => {
                    formData.append(`files[${idx}]file`, f.file, f.file.name);
                    formData.append(`files[${idx}]as_media`, f.asMedia);
                });
            } else if (matchScheduledMessageEditRoutePath) {
                // Ignore meta data of files if they are exist at backend. Only new files will be sent
                filesPromises = fileList
                    .filter((f) => !f.fromBackend)
                    .map((f) => {
                        const fileFormData = new FormData();
                        fileFormData.append('file', f.file, f.file.name);
                        fileFormData.append('as_media', f.asMedia);
                        fileFormData.append('message_id', messageId);
                        return fileFormData;
                    })
                    .map((fileFormData) => httpClient.post(routes.messageFilesPath(), fileFormData));
            } else {
                console.error('Media or files is not possible to send. Unmatched route path');
            }
        }

        const config = {
            post: (data) => ({
                method: 'post',
                url: routes.messagesPath(),
                data,
            }),
            put: (data) => ({
                method: 'put',
                url: routes.messagePath(messageId),
                data,
            }),
        };

        let method;
        let successfulText = '';
        if (matchMessageCreateRoutePath) {
            method = 'post';
            successfulText = 'Сообщение успешно отправлено';
        } else if (matchScheduledMessageEditRoutePath) {
            const verb = clickedSubmitBtnId === 'now' ? 'отправлено' : 'отредактировано';
            method = 'put';
            successfulText = `Сообщение успешно ${verb}`;
        } else {
            console.error('Unmatched route path');
        }

        try {
            await httpClient(config[method](formData));

            if (matchScheduledMessageEditRoutePath && hasAtLeastOneAttachment) {
                await Promise.all(filesPromises);
            }

            toast.success(successfulText);
        } catch (e) {
            toast.error(e.name + e.message);
            console.error(e);
        }
    };

    useEffect(() => {
        if (loadedData) {
            // these are constant names from backend, so they have a bit non-semantic names
            const { text, pinMessage, sendTime, deleteTime, files, buttons } = loadedData.message;

            setValue('text', text);
            setValue('shouldMessagePin', pinMessage);
            setValue('shouldMessageDelete', !!deleteTime);
            if (deleteTime) setValue('deletingTime', dayjs(deleteTime));
            if (sendTime) setValue('plannedSendingTime', dayjs(sendTime));
            setFileList(files?.map((f) => ({
                id: f.id,
                file: {
                    name: f.filename,
                },
                asMedia: f.as_media,
                fromBackend: true,
            })) ?? []);
            if (buttons) setActionButtonRows(buttons);
        }
    }, [loadedData, setValue, setFileList, setActionButtonRows]);

    useEffect(() => {
        if (isSubmitSuccessful && matchMessageCreateRoutePath) {
            resetForm();
        }

        if (isSubmitSuccessful && matchScheduledMessageEditRoutePath) {
            navigate('../scheduled');
        }

        return () => {
            resetForm();
        };
    }, [isSubmitSuccessful, matchMessageCreateRoutePath, matchScheduledMessageEditRoutePath, resetForm, navigate]);

    const handleAddRowBtnClick = () => {
        setActionButtonRows((draft) => {
            draft.push({ rowId: nanoid(), buttons: [] });
        });
    };

    return (
        <form id='message-form' onSubmit={handleSubmit(onSubmit)}>
            <FormRichTextEditor
                control={control}
                isSubmitSuccessful={isSubmitSuccessful}
                name='text'
                placeholder={t('messageFormPlaceholder')}
                rules={{
                    validate: {
                        notEmptyWhenNoAttachments: () => validateEditorMinLength(editorInstance, { hasAtLeastOneAttachment }),
                        lessThanMaxLength: () => validateEditorMaxLength(editorInstance, { hasAtLeastOneAttachment }),
                    },
                }}
                toolbarFormats={['bold', 'italic', 'strikethrough', 'underline', 'spoiler', 'link', 'emoji', 'clean']}
                shouldBeFocusedAfterSubmit
                shouldBeFocusedInitially
            />

            <div className={styles.options}>
                <div className={styles.option}>
                    <h2>{t('subtitles.attachments')}</h2>
                    <FileInputs FileListAPI={{ fileList, setFileList }} />
                    <Attachments FileListAPI={{ fileList, setFileList }} />
                </div>
                <div className={styles.option}>
                    <h2>{t('subtitles.buttons')}</h2>
                    {!isEmpty(actionButtonRows) && (
                        <div className={styles.actionBtnRows}>
                            {actionButtonRows.map((row) => (
                                <ActionButtonRow
                                    key={row.rowId}
                                    actionButtonRowsAPI={{ actionButtonRows, setActionButtonRows }}
                                    row={row}
                                />
                            ))}
                        </div>
                    )}
                    <Button
                        size='medium'
                        variant='secondary'
                        onClick={handleAddRowBtnClick}
                    >
                        <AddIcon />
                    </Button>
                </div>
                <div className={styles.option}>
                    <h2>{t('subtitles.settings')}</h2>
                </div>
                <MessagePin />
                <Button size='medium' variant='secondary' onClick={() => resetForm()}>
                    {t('buttons.reset')}
                </Button>

            </div>
        </form>
    );
}
