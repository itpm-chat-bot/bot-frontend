import { useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { Modal as AntdModal } from 'antd';
import PropTypes from 'prop-types';

import { FormInput, RadioButton } from '@/shared/ui';

import { MODAL_DEFAULT_VALUES } from '../../model/config.js';
import { MAX_BTN_NAME_LENGTH } from '../../model/constants.js';
import { rules } from '../../model/rules.js';
import { ValueInput } from './ValueInput/ValueInput';

import styles from './Modal.module.css';

const options = [
    {
        label: 'modal.types.link',
        value: 'link',
    },
    {
        label: 'modal.types.hint',
        value: 'hint',
    },
];

export function Modal({ title, values, isOpen, onSave, onCancel }) {
    const {
        control, watch, setFocus, reset, unregister, handleSubmit, formState: { errors },
    } = useForm({
        defaultValues: MODAL_DEFAULT_VALUES,
        values,
    });
    const { t } = useTranslation(['messages', 'common']);

    const watchType = watch('type');
    const watchValue = watch('value') || '';

    const onSubmit = (data) => {
        onSave(data);
        reset(values);
    };
    // Button (type="submit") triggered main form submit. So it was replaced with button (type="button") and
    // RHF handleSubmit func was moved from <form> to handler (https://github.com/react-hook-form/react-hook-form/issues/1005)
    const handleOk = () => {
        handleSubmit(onSubmit)();
    };

    const handleCancelClick = () => {
        onCancel();
        reset(values);
    };

    useEffect(() => {
        if (isOpen) {
            setFocus('name');
        }
    }, [isOpen, setFocus]);

    const getErrorMessage = (fieldErrors) => (fieldErrors?.message?.key
        ? t(fieldErrors.message.key, fieldErrors.message.options)
        : '');

    return (
        <AntdModal
            cancelText={t('buttons.return')}
            className='custom-modal-settings'
            closable={false}
            okText={t('buttons.save')}
            open={isOpen}
            title={title}
            width='41.625rem'
            centered
            onCancel={handleCancelClick}
            onOk={handleOk}
        >
            <form className={styles.fieldGroup}>
                <div className={styles.field}>
                    <label className={styles.label}>{t('modal.labels.buttonName')}</label>
                    <FormInput
                        control={control}
                        maxLength={MAX_BTN_NAME_LENGTH}
                        name='name'
                        placeholder={t('modal.placeholders.buttonName')}
                        rules={rules.name}
                    />
                </div>

                <div className={styles.field}>
                    <label className={styles.label}>{t('modal.labels.type')}</label>

                    <Controller
                        control={control}
                        name='type'
                        render={({ field: { onChange, value } }) => (
                            <div className={styles.radioGroup}>
                                {options.map((option) => (
                                    <div key={option.value} className={styles.radio}>
                                        <RadioButton
                                            checked={value === option.value}
                                            label={t(option.label)}
                                            name='type'
                                            value={option.value}
                                            onChange={(e) => {
                                                onChange(e.target.value);
                                                unregister('value');
                                            }}
                                        />
                                    </div>
                                ))}
                            </div>
                        )}
                    />

                    <p className={styles.description}>
                        {t(`modal.descriptions.${watchType}`)}
                    </p>
                </div>

                <ValueInput
                    control={control}
                    errors={errors.value}
                    getErrorMessage={getErrorMessage}
                    type={watchType}
                    value={watchValue}
                />
            </form>
        </AntdModal>
    );
}

Modal.propTypes = {
    title: PropTypes.string.isRequired,
    values: PropTypes.shape({
        name: PropTypes.string,
        type: PropTypes.oneOf(['link', 'hint']),
        value: PropTypes.string,
    }),
    isOpen: PropTypes.bool.isRequired,
    onSave: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
};

Modal.defaultProps = {
    values: null,
};
