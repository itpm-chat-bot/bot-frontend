import { useTranslation } from 'react-i18next';

import { FormInput, FormTextArea } from '@/shared/ui';

import { MAX_BTN_HINT_VALUE_LENGTH } from '../../../model/constants.js';
import { rules } from '../../../model/rules.js';

export function ValueInput({
    type,
    value,
    errors,
    control,
    getErrorMessage,
}) {
    const { t } = useTranslation(['messages', 'common']);

    const getValueError = () => {
        if (type === 'hint' && value.length > MAX_BTN_HINT_VALUE_LENGTH) {
            return t('validation.maxLength', { count: MAX_BTN_HINT_VALUE_LENGTH });
        }
        return getErrorMessage(errors);
    };

    const commonProps = {
        key: type,
        control,
        label: t(`modal.labels.${type === 'hint' ? 'messageToUser' : 'link'}`),
        name: 'value',
        placeholder: t(`modal.placeholders.${type === 'hint' ? 'messageToUser' : 'link'}`),
        error: getValueError(),
        rules: rules[type],
    };

    if (type === 'hint') {
        return (
            <FormTextArea
                {...commonProps}
                maxLength={MAX_BTN_HINT_VALUE_LENGTH}
                style={{ height: '5.125rem' }}
            />
        );
    }

    return (
        <FormInput
            {...commonProps}
            showLength={false}
            type='url'
        />
    );
}
