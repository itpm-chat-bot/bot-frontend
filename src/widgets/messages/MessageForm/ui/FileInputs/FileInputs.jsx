import { Fragment, useRef } from 'react';
import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';

import { nanoid } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';

import { AddIcon } from '@/shared/assets/icons/svg';
import { Button } from '@/shared/ui';
import { getExtensions } from '@/shared/ui/RichTextEditor/util.js';

import { formatBytes } from '../../lib';
import { MIN_FILE_SIZE_B, MAX_IMAGE_SIZE_MB, MAX_IMAGE_WIDTH_PX, MAX_IMAGE_HEIGHT_PX } from '../../model/constants.js';

import styles from './FileInputs.module.css';

const isFileSizeZeroByte = (file) => file.size < MIN_FILE_SIZE_B;
const isImageSizeExceeded = (file) => formatBytes(file.size, 'MB') > MAX_IMAGE_SIZE_MB;
const isImageResolutionExceeded = async (file) => {
    const img = new Image();
    img.src = URL.createObjectURL(file);

    try {
        await img.decode();
    } catch (encodingErr) {
        console.error(encodingErr);
        return Promise.reject(encodingErr);
    }

    const { width, height } = img;
    const isResolutionExceeded = width > MAX_IMAGE_WIDTH_PX || height > MAX_IMAGE_HEIGHT_PX;
    URL.revokeObjectURL(img.src);
    return isResolutionExceeded ? Promise.reject() : Promise.resolve();
};

const mediaFileExtensions = [getExtensions('image'), getExtensions('tgAcceptedVideo')].join(', ');
const fileExtensions = [mediaFileExtensions, getExtensions('audio'), getExtensions('video'), getExtensions('text'), getExtensions('application')].join(', ');

export function FileInputs({ FileListAPI }) {
    const { fileList, setFileList } = FileListAPI;

    const inputMediaRef = useRef(null);
    const inputFileRef = useRef(null);

    const { t } = useTranslation('messages');

    const showMinFileSizeError = () => {
        toast.error(`Разрешается прикреплять файлы с весом не менее ${MIN_FILE_SIZE_B} байт. Попробуйте заново`);
    };

    const showMaxImageSizeError = () => {
        toast.error(`Разрешается прикреплять изображения с весом не более ${MAX_IMAGE_SIZE_MB} MB. Попробуйте заново`);
    };

    const showMaxImageResolutionError = () => {
        toast.error(`Разрешается прикреплять изображения не более ${MAX_IMAGE_WIDTH_PX}x${MAX_IMAGE_HEIGHT_PX}`
            + ' пикселей. Попробуйте заново');
    };

    const handleMediaFileInputChange = async (e) => {
        const mediaFiles = [...e.target.files];
        const images = mediaFiles.filter((file) => file.type.startsWith('image/'));

        if (mediaFiles.some(isFileSizeZeroByte)) {
            showMinFileSizeError();
            return;
        }

        if (images.some(isImageSizeExceeded)) {
            showMaxImageSizeError();
            return;
        }

        const resolutionPromises = images.map(isImageResolutionExceeded);
        const results = await Promise.allSettled(resolutionPromises);
        const hasEnormousImage = results.some((res) => res.status === 'rejected');

        if (hasEnormousImage) {
            showMaxImageResolutionError();
            return;
        }

        const newMediaFiles = mediaFiles.map((file) => ({ id: nanoid(), file, asMedia: true, fromBackend: false }));
        setFileList([...fileList, ...newMediaFiles]);

        // Reset input value to be able to attach the same mediafile multiple times
        e.target.value = null;
    };

    const handleFileInputChange = (e) => {
        const files = [...e.target.files];

        if (files.some(isFileSizeZeroByte)) {
            showMinFileSizeError();
            return;
        }

        const newFiles = files.map((file) => ({ id: nanoid(), file, asMedia: false, fromBackend: false }));
        setFileList([...fileList, ...newFiles]);

        // Reset input value to be able to attach the same file multiple times
        e.target.value = null;
    };

    const handleClickMediaInput = () => {
        if (inputMediaRef && inputMediaRef.current) {
            inputMediaRef.current.click();
        }
    };

    const handleClickFileInput = () => {
        if (inputFileRef && inputFileRef.current) {
            inputFileRef.current.click();
        }
    };

    const inputs = [
        {
            name: 'mediaFileInput',
            ref: inputMediaRef,
            accept: mediaFileExtensions,
            text: 'buttons.media',
            onChange: handleMediaFileInputChange,
            onClick: handleClickMediaInput,
        },
        {
            name: 'fileInput',
            ref: inputFileRef,
            accept: fileExtensions,
            text: 'buttons.file',
            onChange: handleFileInputChange,
            onClick: handleClickFileInput,
        },
    ];

    return (
        <div className={styles.container}>
            {inputs.map((item) => (
                <Fragment key={item.name}>
                    <Button
                        leftIcon={<AddIcon />}
                        variant='secondary'
                        onClick={item.onClick}
                    >
                        {t(item.text)}
                    </Button>

                    <input
                        ref={item.ref}
                        accept={item.accept}
                        className='sr-only-element'
                        name={item.name}
                        type='file'
                        multiple
                        onChange={item.onChange}
                    />
                </Fragment>
            ))}
        </div>
    );
}

/* eslint eslint-comments/no-use: off */
FileInputs.propTypes = {
    FileListAPI: PropTypes.shape({
        // eslint-disable-next-line react/forbid-prop-types
        fileList: PropTypes.array.isRequired,
        setFileList: PropTypes.func.isRequired,
    }).isRequired,
};
