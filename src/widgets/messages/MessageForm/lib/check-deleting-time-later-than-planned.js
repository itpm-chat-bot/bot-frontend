import dayjs from 'dayjs';

export function checkDeletingTimeLaterThanPlanned(planned) {
    return (value, formValues) => {
        // if planned sending time is not picked, then any deleting time is valid
        if (!planned) return true;

        if (formValues.shouldMessageDelete && value instanceof dayjs) {
            return value.isAfter(planned) || 'Неправильно выбраны дата и время удаления сообщения';
        }

        return true;
    };
}
