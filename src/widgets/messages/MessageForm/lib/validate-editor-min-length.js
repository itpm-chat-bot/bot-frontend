export function validateEditorMinLength(editorInstance, options) {
    if (!editorInstance) return null;

    const { hasAtLeastOneAttachment } = options;
    const length = editorInstance.getLength() - 1; // subtract 1 because editor always contain symbol "\n"
    const shouldValidate = hasAtLeastOneAttachment
        ? true
        : length > 0;
    return shouldValidate || 'Сообщение пустое';
}
