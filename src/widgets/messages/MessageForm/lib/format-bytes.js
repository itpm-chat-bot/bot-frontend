export function formatBytes(size, toUnit) {
    switch (toUnit) {
        case 'kB':
            return size / 1000;
        case 'MB':
            return size / (1000 ** 2);
        case 'GB':
            return size / (1000 ** 3);
        default:
            throw new Error(`Given unit ${toUnit} is not recognized`);
    }
}
