import dayjs from 'dayjs';

// disable days before today
export function disablePeriod(value) {
    if (value instanceof dayjs) {
        const endOfYesterday = dayjs().subtract(1, 'day').endOf('day');
        const endOfDayAfterOneYear = dayjs().add(1, 'year').endOf('day');
        const shouldDisableValue = value.isBefore(endOfYesterday) || value.isAfter(endOfDayAfterOneYear);
        return shouldDisableValue;
    }

    throw new Error(`Unexpected date-time value "${value}" was given`);
}
