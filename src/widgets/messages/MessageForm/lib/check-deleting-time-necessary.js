export function checkDeletingTimeNecessary(value, formValues) {
    if (formValues.shouldMessageDelete && !value) {
        return 'Выберите дату и время удаления сообщения';
    }

    return true;
}
