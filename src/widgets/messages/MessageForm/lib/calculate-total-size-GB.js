import { isEmpty } from '@/shared/lib';

import { formatBytes } from './format-bytes.js';

export function calculateTotalSizeGB(list) {
    if (isEmpty(list)) {
        return 0;
    }

    const sumOfBytes = list.reduce((acc, { file }) => acc + file.size, 0);
    return formatBytes(sumOfBytes, 'GB');
}
