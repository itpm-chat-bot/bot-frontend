import dayjs from 'dayjs';

// value must be 1 min later current time
export function checkTimeLaterThanCurrent(value, _formValues) {
    if (value === null) return null;

    if (value instanceof dayjs) {
        return (value.isAfter(dayjs().endOf('minute'))) || 'Выбранное время должно быть не раньше, чем через минуту от текущего';
    }

    throw new Error(`Unexpected date-time value "${value}" was given`);
}
