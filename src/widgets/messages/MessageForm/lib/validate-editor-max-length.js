import { MAX_TEXT_LENGTH, MAX_TEXT_LENGTH_WITH_ATTACHMENTS } from '../model/constants.js';

export function validateEditorMaxLength(editorInstance, options) {
    if (!editorInstance) return null;

    const { hasAtLeastOneAttachment } = options;
    const length = editorInstance.getLength() - 1; // subtract 1 because editor always contain symbol "\n"
    const maxCount = hasAtLeastOneAttachment ? MAX_TEXT_LENGTH_WITH_ATTACHMENTS : MAX_TEXT_LENGTH;
    return length <= maxCount || `Введите не более ${maxCount} символов`;
}
