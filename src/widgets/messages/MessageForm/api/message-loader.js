import camelcaseKeys from 'camelcase-keys';

import { httpClient } from '@/app/services/http-client.js';
import { routes } from '@/shared/api/routes.js';

export async function loader({ params }) {
    const res = await httpClient.get(routes.messagePath(params.messageId));
    const message = camelcaseKeys(res.data);

    return { message };
}
