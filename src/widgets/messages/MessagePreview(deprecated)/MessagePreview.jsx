import { useSelector } from 'react-redux';

import { PictureOutlined, FileOutlined } from '@ant-design/icons';
import * as DOMPurify from 'dompurify';
import PropTypes from 'prop-types';

import { useMessagesContext } from '../../../shared/lib';

import styles from './MessagePreview.module.css';

function TextPreview({ markup }) {
    /* eslint eslint-comments/no-use: off */
    // eslint-disable-next-line react/no-danger
    return <div dangerouslySetInnerHTML={markup} className={styles.preview} />;
}

function FileList({ files }) {
    return (
        <ul className={styles.ul}>
            {files.map((item) => <ListItem key={item.id} item={item} />)}
        </ul>
    );
}

function ListItem({ item }) {
    const { filename, asMedia } = item;

    return (
        <li className={styles.li}>
            {asMedia === true
                ? <PictureOutlined className={styles.prefixIcon} />
                : <FileOutlined className={styles.prefixIcon} />}
            {filename}
        </li>
    );
}

export function MessagePreview() {
    const { matchScheduledMessagesRoutePath, matchMessagesHistoryRoutePath } = useMessagesContext();
    const previewText = useSelector((state) => state.currentMessage.entity.previewText);
    const text = useSelector((state) => state.currentMessage.entity.text);
    const files = useSelector((state) => state.currentMessage.entity.files);

    let content;
    if (matchScheduledMessagesRoutePath) {
        content = previewText;
    } else if (matchMessagesHistoryRoutePath) {
        content = text;
    } else {
        console.error('Unmatched route path at MessagePreview');
    }

    const markup = {
        __html: DOMPurify.sanitize(content),
    };

    return (
        <>
            <TextPreview markup={markup} />
            <FileList files={files} />
        </>
    );
}

TextPreview.propTypes = {
    markup: PropTypes.shape({
        __html: PropTypes.string.isRequired,
    }).isRequired,
};

FileList.propTypes = {
    files: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
    })).isRequired,
};

ListItem.propTypes = {
    filename: PropTypes.string.isRequired,
    asMedia: PropTypes.bool.isRequired,
};

// Specify default props and prop types if MessagePreview component will accept props
