import PropTypes from 'prop-types';

import { cn } from '@/shared/lib';

import { ActiveChat } from '../ActiveChat/ActiveChat.jsx';
import { InactiveChat } from '../InactiveChat/InactiveChat.jsx';

import styles from './List.module.css';

export const List = ({ title, subtitle, chats, isBotAdmin, isStretched }) => {
    const isListEmpty = chats.length === 0;

    const sortedChats = chats.toSorted((a, b) => b.id - a.id);

    return (
        <div className={styles.container}>
            <h2>{title}</h2>

            <p className={cn({
                [styles.subtitle]: !isListEmpty,
                [styles.fallbackSubtitle]: isListEmpty,
            })}
            >
                {subtitle}
            </p>

            {!isListEmpty && (
                <ul className={cn(styles.list, {
                    [styles.stretched]: isStretched,
                })}
                >
                    {sortedChats.map((chat) => (
                        isBotAdmin
                            ? <ActiveChat key={chat.id} chat={chat} />
                            : <InactiveChat key={chat.id} chat={chat} />
                    ))}
                </ul>
            )}
        </div>
    );
};

List.propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    chats: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
    })),
    isBotAdmin: PropTypes.bool,
    isStretched: PropTypes.bool,
};

List.defaultProps = {
    chats: [],
    subtitle: '',
    isBotAdmin: undefined,
    isStretched: undefined,
};
