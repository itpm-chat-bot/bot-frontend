import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';

import { Avatar } from '@/entities/Avatar/index.js';
import { chatTypeLabelKeys } from '@/shared/constants/chatTypes';
import { cn, getInitials } from '@/shared/lib';

import styles from './ActiveChat.module.css';

export const ActiveChat = ({ chat }) => {
    const { t } = useTranslation('main');

    return (
        <li className={styles.container}>
            <Link className={styles.innerContainer} to={`chats/${chat.id}/about`}>
                <Avatar
                    alt=''
                    fallbackText={getInitials(chat.title)}
                    size='big'
                    src={chat.avatar}
                />
                <div className={styles.description}>
                    <h3>{chat.title}</h3>
                    <span className={cn(styles.type, styles[chat.type])}>{t(chatTypeLabelKeys[chat.type])}</span>
                </div>
            </Link>
        </li>
    );
};

ActiveChat.propTypes = {
    chat: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
    }).isRequired,
};
