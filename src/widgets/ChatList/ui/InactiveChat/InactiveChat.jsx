import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';

import { Avatar } from '@/entities/Avatar/index.js';
import { AddIcon, DeleteIcon } from '@/shared/assets/icons/svg';
import { chatTypeLabelKeys } from '@/shared/constants/chatTypes';
import { PRODUCTION_BOT_URL } from '@/shared/constants/urls.js';
import { cn, getInitials } from '@/shared/lib';
import { deleteChat } from '@/shared/redux/slices/chatsSlice.js';

import styles from './InactiveChat.module.css';

export const InactiveChat = ({ chat }) => {
    const { t } = useTranslation('main');
    const dispatch = useDispatch();

    return (
        <li className={styles.container}>
            <div className={styles.innerContainer}>
                <Avatar
                    alt=''
                    fallbackText={getInitials(chat.title)}
                    size='big'
                    src={chat.avatar}
                />

                <div className={styles.description}>
                    <h3>{chat.title}</h3>
                    <h3 className={cn(styles.type, styles[chat.type])}>{t(chatTypeLabelKeys[chat.type])}</h3>
                </div>
            </div>

            <div className={styles.icons}>
                <DeleteIcon
                    title={t('removeFromList')}
                    onClick={() => dispatch(deleteChat(chat.id))}
                />
                <Link
                    rel='noreferrer'
                    target='_blank'
                    to={PRODUCTION_BOT_URL}
                >
                    <AddIcon title={t('addToList')} />
                </Link>
            </div>
        </li>
    );
};

InactiveChat.propTypes = {
    chat: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
    }).isRequired,
};
