import { useTranslation } from 'react-i18next';

import { Modal } from 'antd';

export function ExitModal({ isVisible, onCancel, onClose }) {
    const { t } = useTranslation('common');
    const title = (
        <>
            {t('leavePageModal.title')}
            <br />
            {t('leavePageModal.subtitle')}
        </>
    );
    return (

        <Modal
            cancelText={t('leavePageModal.stay')}
            className='custom-ant-modal'
            closable={false}
            okText={t('leavePageModal.leave')}
            open={isVisible}
            title={title}
            centered
            onCancel={onCancel}
            onOk={onClose}
        />
    );
}
