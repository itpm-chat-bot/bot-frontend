export const generateMenuItemHref = (pathname, href) => pathname
    .split('/')
    .slice(0, 3)
    .join('/')
    .concat(href);
