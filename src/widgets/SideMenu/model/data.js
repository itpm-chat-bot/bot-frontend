import {
    ChatIcon,
    CodeIcon,
    ForumIcon,
    IndeterminateQuestionBoxIcon,
    PersonIcon,
    SwitchAccountIcon,
} from '@/shared/assets/icons/svg';

export const MENU_DATA = [
    {
        id: 1,
        leftIcon: ForumIcon,
        label: 'sideMenu.about',
        href: '/about',
    },
    {
        id: 2,
        leftIcon: ChatIcon,
        label: 'sideMenu.messages',
        href: '/messages/sending/history',
    },
    {
        id: 3,
        leftIcon: SwitchAccountIcon,
        label: 'sideMenu.participants',
        href: '/Participants/activity',
    },
    {
        id: 4,
        leftIcon: PersonIcon,
        label: 'sideMenu.experts',
        href: '/experts',
    },
    {
        id: 5,
        leftIcon: CodeIcon,
        label: 'sideMenu.commands',
        href: '/commands',
    },
    {
        id: 6,
        leftIcon: IndeterminateQuestionBoxIcon,
        label: 'sideMenu.faq',
        href: '/faq/published',
    },
];
