import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

import { SwitchLanguage } from '@/features/SwitchLanguage';
import { chatTypeLabelKeys } from '@/shared/constants/chatTypes';
import { cn } from '@/shared/lib';
import { selectedChatEntitySelector } from '@/shared/redux/slices/selectedChatSlice';
import { Button, ProjectLogo } from '@/shared/ui';

import { generateMenuItemHref } from '../../lib/generate-menu-item-href.js';
import { MENU_DATA } from '../../model/data.js';
import { ChatDropdown } from '../ChatDropdown/ChatDropdown.jsx';
import { UserDropdown } from '../UserDropdown/UserDropdown.jsx';

import styles from './Menu.module.css';

export function Menu() {
    const { t } = useTranslation('common');
    const { pathname } = useLocation();
    const navigate = useNavigate();
    const { type } = useSelector(selectedChatEntitySelector);

    const filteredMenuData = MENU_DATA.filter(({ href }) => {
        const isChannel = type === chatTypeLabelKeys.channel;
        if (isChannel) {
            return href !== '/experts' && href !== '/faq/published';
        }
        return true;
    });

    const isMenuItemActive = (href) => pathname.includes(`/${href.split('/')[1]}`);

    return (
        <section className={styles.section}>
            <div className={styles.menuContent}>
                <ProjectLogo />

                <div className={styles.menuActions}>
                    <ChatDropdown />

                    <nav className={styles.buttons}>
                        {filteredMenuData.map(({ id, href, leftIcon: Icon, label }) => (
                            <Button
                                key={id}
                                className={cn({ [styles.selected]: isMenuItemActive(href) })}
                                variant='tertiary'
                                onClick={() => navigate(generateMenuItemHref(pathname, href))}
                            >
                                <Icon />
                                {t(label)}
                            </Button>
                        ))}
                    </nav>
                </div>
            </div>

            <div className={styles.menuFooter}>
                <UserDropdown />

                <div>
                    <small>{t('labels.version', { version: import.meta.env.VITE_APP_NPM_PACKAGE_VERSION })}</small>
                    <SwitchLanguage />
                </div>
            </div>
        </section>
    );
}
