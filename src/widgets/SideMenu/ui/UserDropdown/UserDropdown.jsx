import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Dropdown } from 'antd';

import { Avatar } from '@/entities/Avatar';
import { CoffeeIcon, ExpandMoreIcon, HelpIcon, LogoutIcon } from '@/shared/assets/icons/svg';
import { FEEDBACK_BOT_URL, TRIBUTE } from '@/shared/constants/urls.js';
import { useAuth } from '@/shared/lib';

import styles from './UserDropdown.module.css';

export function UserDropdown() {
    const firstName = useSelector((state) => state.myProfile.entity.firstName);
    const lastName = useSelector((state) => state.myProfile.entity.lastName);
    const username = useSelector((state) => state.myProfile.entity.username);
    const avatar = useSelector((state) => state.myProfile.entity.avatar);

    const { logOut } = useAuth();
    const { t } = useTranslation('common');

    const fullname = `${firstName ?? ''} ${lastName ?? ''}`;

    const handleLogoutBtnClick = () => {
        logOut();
    };

    const items = [
        {
            key: 'logout',
            label: (
                <button className={styles.dropdownItemBtn} type='button' onClick={handleLogoutBtnClick}>
                    {t('sideMenu.logout')}
                </button>
            ),
            icon: <LogoutIcon />,
        },
        {
            key: 'support',
            label: (
                <a
                    className={styles.dropdownItemBtn}
                    href={FEEDBACK_BOT_URL}
                    rel='noreferrer'
                    target='_blank'
                >
                    {t('sideMenu.support')}
                </a>
            ),
            icon: <HelpIcon />,
        }, {
            key: 'coffee',
            label: (
                <a
                    className={styles.dropdownItemBtn}
                    href={TRIBUTE}
                    rel='noreferrer'
                    target='_blank'
                >
                    {t('sideMenu.coffee')}
                </a>
            ),
            icon: <CoffeeIcon />,
        },
    ];

    return (
        <Dropdown
            menu={{
                items,
            }}
            overlayClassName='custom-dropdown-overlay'
            placement='top'
            trigger={['click']}
        >
            <button className={styles.triggerBtn} type='button'>
                <div className={styles.wrapper}>
                    <Avatar
                        alt=''
                        fallbackText={fullname}
                        size='medium'
                        src={avatar}
                    />

                    <p className={styles.userinfo}>
                        <span>{fullname}</span>
                        <span>{'@'.concat(username)}</span>
                    </p>
                </div>

                <ExpandMoreIcon />
            </button>
        </Dropdown>
    );
}
