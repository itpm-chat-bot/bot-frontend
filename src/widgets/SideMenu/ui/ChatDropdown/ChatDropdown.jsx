import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

import { Dropdown } from 'antd';

import { Avatar } from '@/entities/Avatar';
import { ExpandMoreIcon, AddIcon } from '@/shared/assets/icons/svg';
import { PRODUCTION_BOT_URL } from '@/shared/constants/urls.js';
import { getInitials } from '@/shared/lib';
import { chatsSelectors } from '@/shared/redux/slices/chatsSlice.js';
import { selectedChatActions } from '@/shared/redux/slices/selectedChatSlice.js';

import styles from './ChatDropdown.module.css';

export function ChatDropdown() {
    const chatId = useSelector((state) => state.selectedChat.entity.id);
    const title = useSelector((state) => state.selectedChat.entity.title);
    const avatar = useSelector((state) => state.selectedChat.entity.avatar);

    const chats = useSelector(chatsSelectors.selectAll);

    const dispatch = useDispatch();

    const { t } = useTranslation('common');
    const { pathname } = useLocation();
    const navigate = useNavigate();

    const handleDropdownItemClick = (newId) => {
        const segments = pathname
            .split('/')
            .filter((item) => item !== '');

        navigate(segments.with(1, newId).join('/'));
        dispatch(selectedChatActions.initFetchingSelectedChat());
    };

    const items = chats
        .filter((item) => item.isBotAdmin)
        .filter((item) => item.id !== chatId)
        .map((item) => ({
            key: item.id,
            label: (
                <button
                    className={styles.dropdownItemBtn}
                    type='button'
                    onClick={() => handleDropdownItemClick(item.id)}
                >
                    <Avatar
                        alt=''
                        fallbackText={getInitials(item.title)}
                        size='small'
                        src={item.avatar}
                    />

                    <h3 className={styles.title}>{item.title}</h3>
                </button>
            ),
        }))
        .concat(
            {
                key: 'add',
                label: (
                    <a
                        className={styles.dropdownItemBtn}
                        href={PRODUCTION_BOT_URL}
                        rel='noreferrer'
                        target='_blank'
                    >
                        <AddIcon />
                        {t('sideMenu.addChat')}
                    </a>
                ),
            },
        );

    return (
        <Dropdown
            menu={{
                items,
                // onClick: () => console.log('click'),
            }}
            overlayClassName='custom-dropdown-overlay'
            placement='bottom'
            trigger={['click']}
        >
            <button className={styles.triggerBtn} type='button'>
                <div className={styles.wrapper}>
                    <Avatar
                        alt=''
                        fallbackText={getInitials(title)}
                        size='medium'
                        src={avatar}
                    />

                    <h3 className={styles.title}>{title}</h3>
                </div>

                <ExpandMoreIcon />
            </button>
        </Dropdown>
    );
}
