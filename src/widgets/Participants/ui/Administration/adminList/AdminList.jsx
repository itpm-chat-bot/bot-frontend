import { Participant } from '../../Participant/Participant';

import styles from './AdminList.module.css';

export function AdminList({ participants, onSelectUser, searchTerm }) {
    const isParticipant = participants.length === 0;

    const getBackgroundColor = (index) => {
        return index % 2 === 0 ? '' : '#F2F2F2';
    };

    const filteredParticipants = participants.filter((participant) => {
        const fullName = `${participant.firstName} ${participant.lastName}`.toLowerCase();
        return (
            participant.status !== 'member'
            && (fullName.includes(searchTerm.toLowerCase())
                || participant.username.toLowerCase().includes(searchTerm.toLowerCase()))
        );
    });

    return (
        <section className={styles.wrapper}>
            <div className={styles.option}>
                <h4>Участник</h4>
                <div className={styles.rating}>Тип прав</div>
            </div>
            <div className={styles.listContainer}>
                {filteredParticipants.length === 0 && !isParticipant && <div className={styles.noData}>Нет данных</div>}
                {!isParticipant && filteredParticipants.map((participant, index) => (
                    <div key={participant.tgId}>
                        <Participant
                            backgroundColor={getBackgroundColor(index)}
                            participant={participant}
                            onSelectUser={onSelectUser}
                        />
                    </div>
                ))}
            </div>
        </section>
    );
}
