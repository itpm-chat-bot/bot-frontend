import { useContext, useState } from 'react';

import { ParticipantsContext } from '../ParticipantPage/page';
import { SearchInput } from '../Search/Search';
import { AdminList } from './adminList/AdminList';

import styles from '../ParticipantPage/Participants.module.css';

export function Administration() {
    const { participants, onSelectUser } = useContext(ParticipantsContext);
    const [searchTerm, setSearchTerm] = useState('');

    const handleSearch = (value) => {
        setSearchTerm(value);
    };
    return (
        <section className={styles.box}>
            <SearchInput onSearch={handleSearch} />
            <AdminList participants={participants} searchTerm={searchTerm} onSelectUser={onSelectUser} />
        </section>
    );
}
