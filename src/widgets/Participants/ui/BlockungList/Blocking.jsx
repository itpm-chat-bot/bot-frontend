import { SearchInput } from '../Search/Search';

import styles from '../ParticipantPage/Participants.module.css';

export function Blocking() {
    return (
        <section className={styles.box}>
            <SearchInput />
        </section>
    );
}
