import { createContext, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Outlet, useLocation } from 'react-router-dom';

import { nanoid } from '@reduxjs/toolkit';

import { CachedIcon } from '@/shared/assets/icons/svg';
import { participantsSelectors } from '@/shared/redux/slices/participantsSlice';
import { Aside, AsideContent, CommonHead, Container, Main, NavTabs } from '@/shared/ui';

import { SelectedParticipantInfo } from '../SelectedParticipantInfo/SelectedParticipantInfo';

import styles from './Participants.module.css';

export const ParticipantsContext = createContext();

const tabItems = [
    { content: 'tabs.activity', id: nanoid(), link: 'activity' },
    { content: 'tabs.administration', id: nanoid(), link: 'administration' },
    { content: 'tabs.blocking', id: nanoid(), link: 'blocking' },
];

const pageTitles = {
    activity: 'commonHead.activeMembers',
    administration: 'commonHead.administration',
    blocking: 'commonHead.blockedMembers',
};

const pageSubtitles = {
    activity: 'commonHead.activeMembersSubtitle',
    administration: 'commonHead.administrationMembersSubtitle',
    blocking: 'commonHead.blockedMembersSubtitle',
};

export function ParticipantsPage() {
    const participants = useSelector(participantsSelectors.selectAll);
    const { t } = useTranslation('participants');
    const location = useLocation();
    const activeTab = tabItems.find((tab) => location.pathname.includes(tab.link));
    const membersPageTitle = t(pageTitles[activeTab.link]);
    const membersPageSubtitle = t(pageSubtitles[activeTab.link]);
    const [selectedUserId, setSelectedUserId] = useState(null);
    const [isAsideOpen, setIsAsideOpen] = useState(false);
    const [rotateIcon, setRotateIcon] = useState(false);
    const [upParticipantsList, setUpParticipantsList] = useState(false);
    const updateInfo = () => {
        setUpParticipantsList(!upParticipantsList);
    };

    const handleSelectUser = useCallback((userId) => {
        setSelectedUserId(userId);
        setIsAsideOpen(true);
    }, [setSelectedUserId, setIsAsideOpen]);

    const closeAside = useCallback(() => {
        setIsAsideOpen(false);
    }, [setIsAsideOpen]);

    const selectedUser = participants.find((participant) => participant.id === selectedUserId);

    const contextValues = useMemo(() => ({
        participants,
        onSelectUser: handleSelectUser,
        closeAside,
        upParticipantsList,
    }), [participants, handleSelectUser, closeAside, upParticipantsList]);

    const containerRef = useRef(null);
    const scrollToContainer = () => {
        containerRef.current.scrollIntoView({ behavior: 'smooth' });
    };
    const rotateIconOnce = () => {
        setRotateIcon(true);
        setTimeout(() => {
            setRotateIcon(false);
        }, 1000);
    };

    useEffect(() => {
        setIsAsideOpen(false);
    }, [location.pathname]);

    return (
        <Main>
            <ParticipantsContext.Provider value={contextValues}>
                <Container>
                    <section>
                        <div className={styles.container}>
                            <div className={styles.header}>
                                <div className={styles.head}>
                                    <CommonHead title={membersPageTitle} />
                                    <CachedIcon
                                        className={rotateIcon ? styles.rotateIcon : ''}
                                        onClick={() => {
                                            updateInfo();
                                            rotateIconOnce();
                                            scrollToContainer();
                                        }}
                                    />
                                </div>
                                <h4 className={styles.subtitle}>{membersPageSubtitle}</h4>
                            </div>
                            <NavTabs items={tabItems} namespace='participants' />
                        </div>
                        <Outlet />
                    </section>
                </Container>
                {isAsideOpen && selectedUserId && (
                    <Aside>
                        <AsideContent>
                            <SelectedParticipantInfo closeAside={closeAside} selectedUser={selectedUser} updateInfo={updateInfo} />
                        </AsideContent>
                    </Aside>
                )}
            </ParticipantsContext.Provider>

        </Main>
    );
}
