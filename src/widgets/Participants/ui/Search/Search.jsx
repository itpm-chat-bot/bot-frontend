import { useState } from 'react';

import { CloseIcon, SearchIcon } from '@/shared/assets/icons/svg';

import styles from './Search.module.css';

export function SearchInput({ onSearch }) {
    const [searchTerm, setSearchTerm] = useState('');

    const handleInputChange = (event) => {
        const { value } = event.target;
        setSearchTerm(value);
        onSearch(value);
    };

    const clearSearch = () => {
        setSearchTerm('');
        onSearch('');
    };

    return (
        <div className={styles.wrap}>
            <div className={styles.container}>
                <div className={styles.search}>
                    <SearchIcon />
                    <input
                        className={styles.searchInput}
                        placeholder='Поиск'
                        type='text'
                        value={searchTerm}
                        onChange={handleInputChange}
                    />
                </div>
                <CloseIcon
                    mainColor='currentColor'
                    onClick={clearSearch}
                />
            </div>
        </div>
    );
}
