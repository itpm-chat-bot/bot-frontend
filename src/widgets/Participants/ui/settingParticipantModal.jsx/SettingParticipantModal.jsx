import { useTranslation } from 'react-i18next';

import { Modal } from 'antd';

export function SettingParticipantModal({ isVisible, onCancel, onSave, title }) {
    const { t } = useTranslation('common');
    return (
        <Modal
            cancelText={t('modal.return')}
            className='custom-ant-modal'
            closable={false}
            okText={t('modal.save')}
            open={isVisible}
            title={title}
            centered
            onCancel={onCancel}
            onOk={onSave}
        />

    );
}
