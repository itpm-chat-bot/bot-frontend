import { useCallback, useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { participantsSelectors, updateParticipant } from '@/shared/redux/slices/participantsSlice';
import { Button, FormTextArea } from '@/shared/ui';

import { createRules } from '../../lib/rules';
import { getDefaultNoteValues } from '../../lib/utils';
import { MAX_NOTE_COUNT } from '../../model/constants.js';

import styles from './NoteForm.module.css';

export function NoteForm({ selectedUser, closeInput, updateInfo }) {
    const {
        control,
        handleSubmit,
        setValue,
    } = useForm({
        defaultValues: getDefaultNoteValues(),
    });

    const { t } = useTranslation('participants');
    const dispatch = useDispatch();
    const participantInfo = useSelector(participantsSelectors.selectAll);
    const participantId = selectedUser.id;
    const { chatTgId } = selectedUser;
    const rules = useMemo(() => createRules(t), [t]);

    const handleSaveNote = useCallback(async (data) => {
        const participantData = {
            additional_info: data.additionalInfo,
            chat: chatTgId,
        };
        await dispatch(updateParticipant({ participantId: Number(participantId), participantData }));
        toast.success('Дополнительная информация успешно обновлена');
        updateInfo();
    }, [dispatch, participantId, chatTgId, updateInfo]);

    const handleUpAdditionalInfo = handleSubmit(async (data) => {
        const participantInitData = participantInfo.find((item) => item.id === Number(participantId));
        if (participantInitData && participantInitData.additionalInfo !== data.additionalInfo) {
            await handleSaveNote(data);
            closeInput();
        } else {
            closeInput();
        }
    });

    useEffect(() => {
        const participantInitData = participantInfo.find((item) => item.id === Number(participantId));
        if (participantInitData) {
            setValue('additionalInfo', participantInitData.additionalInfo);
        }
    }, [participantInfo, participantId, setValue]);

    return (
        <div className={styles.container}>
            <form>
                <FormTextArea
                    control={control}
                    maxLength={MAX_NOTE_COUNT}
                    name='additionalInfo'
                    placeholder='Доп.информация'
                    rules={rules.additionalInfo}
                    style={{
                        height: '74px',
                    }}
                />
            </form>
            <div>
                <Button size='small' type='submit' variant='secondary' onClick={handleUpAdditionalInfo}>
                    Сохранить
                </Button>
            </div>
        </div>
    );
}
