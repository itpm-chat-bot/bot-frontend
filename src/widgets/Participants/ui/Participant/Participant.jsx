import { Avatar } from '@/entities/Avatar';
import { NoteStackIcon } from '@/shared/assets/icons/svg';
import { getInitials } from '@/shared/lib';

import styles from './Participant.module.css';

export function Participant({ participant, onSelectUser }) {
    const fullname = `${participant.firstName ?? ''} ${participant.lastName ?? ''}`;
    const additionalInfoExists = participant.additionalInfo && participant.additionalInfo.trim().length > 0;

    const userStatus = () => {
        if (participant.status === 'admin') {
            return 'Администратор';
        } if (participant.status === 'creator') {
            return 'Владелец';
        }
        return '';
    };

    const handleClick = () => {
        onSelectUser(participant.id);
    };

    const handleKeyPress = (event) => {
        if (event.key === 'Enter' || event.key === ' ') {
            onSelectUser(participant.id);
        }
    };

    return (
        <div
            className={styles.wrap}
            role='button'
            tabIndex={0}
            onClick={handleClick}
            onKeyDown={handleKeyPress}
            onKeyPress={handleKeyPress}
        >
            <div className={styles.block}>
                <div className={styles.ava}>
                    <Avatar alt='' fallbackText={getInitials(participant.firstName)} size='big' src={participant.avatar} />
                </div>
                <div className={styles.info}>
                    <h4 className={styles.name}>
                        {fullname}
                    </h4>
                    <h4 className={styles.nickName}>
                        @
                        {participant.username}
                    </h4>
                </div>
            </div>
            <div className={styles.ratingBlock}>
                {additionalInfoExists && <NoteStackIcon />}
                <div className={styles.points}>
                    <h4>{participant.status === 'member' ? participant.rating : userStatus()}</h4>
                </div>
            </div>
        </div>
    );
}
