import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { NorthTopIcon, SouthBottomIcon } from '@/shared/assets/icons/svg';
import { fetchParticipants } from '@/shared/redux/slices/participantsSlice';

import { Participant } from '../Participant/Participant';

import styles from './ParticipantsList.module.css';

export function ParticipantsList({ participants, onSelectUser, searchTerm, upParticipantsList }) {
    const { chatId } = useParams();
    const dispatch = useDispatch();
    const [isAscending, setIsAscending] = useState(true);
    const [selectedUserId, setSelectedUserId] = useState(null);

    const hasActiveMembers = participants.some((participant) => participant.status === 'member' && participant.rating > 0);

    const filteredParticipants = participants.filter((participant) => {
        const fullName = `${participant.firstName} ${participant.lastName}`.toLowerCase();
        return (
            participant.status === 'member'
            && participant.rating > 0 // Исключаем участников с рейтингом 0
            && (fullName.includes(searchTerm.toLowerCase())
                || participant.username.toLowerCase().includes(searchTerm.toLowerCase()))
        );
    });
    const hasFilteredParticipants = filteredParticipants.length > 0;

    const toggleSortOrder = () => {
        setIsAscending(!isAscending);
    };

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            toggleSortOrder();
        }
    };

    const handleSelectUser = (userId) => {
        setSelectedUserId(userId);
        onSelectUser(userId);
    };

    useEffect(() => {
        dispatch(fetchParticipants({ chatId, sortBy: isAscending }));
    }, [chatId, dispatch, isAscending, upParticipantsList]);
    return (
        <section className={styles.wrapper}>
            <div className={styles.option}>
                <h4>Участник</h4>
                <div
                    aria-label='Toggle Sort Order'
                    className={styles.rating}
                    role='button'
                    tabIndex={0}
                    onClick={toggleSortOrder}
                    onKeyDown={handleKeyDown}
                >
                    <h4> Баллы</h4>
                    {isAscending ? <NorthTopIcon /> : <SouthBottomIcon />}
                </div>
            </div>
            <div className={styles.listContainer}>
                {!hasActiveMembers && (
                    <div className={styles.noData}>
                        Нет активных участников
                    </div>
                )}
                {hasActiveMembers && !hasFilteredParticipants && (
                    <div className={styles.noData}>Нет данных</div>
                )}
                {hasFilteredParticipants
                    && filteredParticipants.map((participant, index) => (
                        <div
                            key={participant.id}
                            className={`${index % 2 === 0
                                ? styles.whiteBackground
                                : styles.grayBackground} ${selectedUserId === participant.id
                                ? styles.selectedUser
                                : ''
                            }`}
                        >
                            <Participant
                                participant={participant}
                                onSelectUser={handleSelectUser}
                            />
                        </div>
                    ))}
            </div>
        </section>
    );
}
