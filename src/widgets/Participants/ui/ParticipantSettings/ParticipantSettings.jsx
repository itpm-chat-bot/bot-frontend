import { CloseIcon } from '@/shared/assets/icons/svg';

import styles from './SettingParticipant.module.css';

export function ParticipantSettings({ onCancel }) {
    return (
        <div className={styles.container}>
            <div className={styles.close}>
                <CloseIcon onClick={onCancel} />
            </div>
            <h3>Тип прав</h3>
        </div>
    );
}
