import { useCallback, useEffect, useRef, useState } from 'react';

import { Avatar } from '@/entities/Avatar';
import { CloseIcon, EditIcon } from '@/shared/assets/icons/svg';
import { getInitials } from '@/shared/lib/util';
import { Button } from '@/shared/ui';

import { NoteForm } from '../ParticipantNote/NoteForm';
import { SettingParticipantModal } from '../settingParticipantModal.jsx/SettingParticipantModal.jsx';

import styles from './SelectedParticipantInfo.module.css';

export function SelectedParticipantInfo({
    selectedUser,
    closeAside,
    updateInfo,
}) {
    const [editingAdditionalInfo, setEditingAdditionalInfo] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);
    //
    // const openModal = () => {
    //     setIsModalVisible(true);
    // };

    const closeModal = () => {
        setIsModalVisible(false);
    };

    const inputRef = useRef(null);
    const handleUPAdditionalInfo = useCallback(() => {
        setEditingAdditionalInfo(false);
        updateInfo();
    }, [updateInfo]);

    useEffect(() => {
        function handleClickOutside(event) {
            if (inputRef.current && !inputRef.current.contains(event.target)) {
                handleUPAdditionalInfo();
            }
        }

        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [handleUPAdditionalInfo]);

    const fullname = `${selectedUser.firstName ?? ''} ${selectedUser.lastName ?? ''}`;
    const userStatus = () => {
        if (selectedUser.status === 'admin') {
            return 'Администратор';
        } if (selectedUser.status === 'member') {
            return 'Участник';
        } if (selectedUser.status === 'creator') {
            return 'Владелец';
        }
        return '';
    };

    const handleEditClick = () => {
        setEditingAdditionalInfo(true);
    };

    return (
        <div className={styles.container}>

            <div className={styles.close}>
                <CloseIcon onClick={closeAside} />
            </div>

            <div className={styles.content}>

                <div className={styles.infoBlock}>
                    <div className={styles.ava}>
                        <Avatar alt='' fallbackText={getInitials(selectedUser.firstName)} size='big' src={selectedUser.avatar} />
                    </div>
                    <div className={styles.info}>
                        <h4 className={styles.name}>
                            {fullname}
                        </h4>
                        <h4 className={styles.nickName}>
                            @
                            {selectedUser.username}
                        </h4>
                    </div>
                </div>

                <div className={styles.data}>
                    <div className={styles.rights}>
                        <div className={styles.rightsType}>
                            <h4>{userStatus()}</h4>
                            <h4 className={styles.rightsSubTitle}>Тип прав</h4>
                        </div>
                        {/* {selectedUser.status !== 'creator' && ( */}
                        {/*    <ChangeCircleIcon onClick={openModal} /> */}
                        {/* )} */}
                    </div>

                    <div ref={inputRef} className={styles.additionalInfoBlock}>
                        {editingAdditionalInfo ? (
                            <NoteForm closeInput={handleUPAdditionalInfo} selectedUser={selectedUser} updateInfo={updateInfo} />
                        ) : (
                            <div className={styles.additionalInfo}>
                                {selectedUser.additionalInfo}
                                <h4 className={styles.rightsSubTitle}>Доп. информация</h4>
                            </div>
                        )}
                        {!editingAdditionalInfo && (
                            <EditIcon onClick={handleEditClick} />
                        )}
                    </div>
                    <div className={styles.stats}>
                        <div className={styles.stat}>
                            {selectedUser.numberMessages}
                            <h4 className={styles.rightsSubTitle}>Сообщения</h4>
                        </div>
                        <div className={styles.stat}>
                            {selectedUser.numberReactions}
                            <h4 className={styles.rightsSubTitle}>Реакции</h4>
                        </div>
                        <div className={styles.stat}>
                            {selectedUser.numberLinks}
                            <h4 className={styles.rightsSubTitle}>Ссылки</h4>
                        </div>
                        <div className={styles.stat}>
                            {selectedUser.numberFiles}
                            <h4 className={styles.rightsSubTitle}>Файлы</h4>
                        </div>
                    </div>

                    <div className={styles.date}>
                        {selectedUser.firstActivity}
                        <h4 className={styles.rightsSubTitle}>Дата появления в чате</h4>
                    </div>
                </div>
            </div>

            <div className={styles.banBlock}>
                <Button
                    leftIcon={<CloseIcon />}
                    size='small'
                    variant='tertiary'
                >
                    Заблокировать
                </Button>
                <h4 className={styles.banInfo}>
                    Пользователь получит бан в чате
                    Ограничение можно убрать в разделе «Блокировка»
                </h4>
            </div>
            <SettingParticipantModal isVisible={isModalVisible} onCancel={closeModal} />
        </div>
    );
}
