import { useContext, useState } from 'react';

import { ParticipantsContext } from '../ParticipantPage/page';
import { ParticipantsList } from '../ParticipantsList/ParticipantsList';
import { SearchInput } from '../Search/Search';

import styles from '../ParticipantPage/Participants.module.css';

export function Activity() {
    const { participants, onSelectUser, upParticipantsList } = useContext(ParticipantsContext);
    const [searchTerm, setSearchTerm] = useState('');

    const handleSearch = (value) => {
        setSearchTerm(value);
    };

    return (
        <section className={styles.box}>
            <SearchInput onSearch={handleSearch} />
            <ParticipantsList
                participants={participants}
                searchTerm={searchTerm}
                upParticipantsList={upParticipantsList}
                onSelectUser={onSelectUser}
            />
        </section>
    );
}
