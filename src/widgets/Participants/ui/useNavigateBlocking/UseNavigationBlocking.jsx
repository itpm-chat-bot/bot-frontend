import { useState } from 'react';
import { useBlocker, useNavigate } from 'react-router-dom';

export function UseNavigationBlocking(isBlockingCondition) {
    const [isNavigationBlocked, setIsNavigationBlocked] = useState(false);
    const [blockedUrl, setBlockedUrl] = useState('');
    const [isModalVisible, setIsModalVisible] = useState(false);

    const navigate = useNavigate();

    const handleNavigationAttempt = (nextLocation) => {
        const shouldBlockNavigation = isBlockingCondition(nextLocation);

        if (shouldBlockNavigation && !isNavigationBlocked) {
            setIsModalVisible(true);
            const { pathname } = nextLocation.nextLocation;
            setBlockedUrl(pathname);
            setIsNavigationBlocked(true);
            return true;
        }

        return false;
    };

    const handleLeavePage = () => {
        if (isNavigationBlocked) {
            setIsNavigationBlocked(false);
            navigate(blockedUrl);
        }
    };

    const handleCancel = () => {
        setIsNavigationBlocked(false);
        setIsModalVisible(false);
    };

    useBlocker((nextLocation) => handleNavigationAttempt(nextLocation));

    return {
        isNavigationBlocked,
        blockedUrl,
        isModalVisible,
        handleLeavePage,
        handleCancel,
    };
}
