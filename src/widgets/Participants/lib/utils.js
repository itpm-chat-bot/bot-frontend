export function getDefaultNoteValues(
    additionalInfo = '',
    type = 'text',
    value = '',
) {
    return {
        additionalInfo,
        type,
        value,
    };
}
