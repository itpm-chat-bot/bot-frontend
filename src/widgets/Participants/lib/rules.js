import { MAX_NOTE_COUNT } from '../model/constants.js';

export function createRules(t) {
    return {
        additionalInfo: {
            maxLength: {
                value: MAX_NOTE_COUNT,
                message: `${t('rules.enterNoMore')} ${MAX_NOTE_COUNT} ${t('rules.symbols')}`,
            },
        },
    };
}
