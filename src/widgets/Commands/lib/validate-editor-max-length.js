import { t } from 'i18next';

import { MAX_TEXT_LENGTH } from '../model/constants.js';

export function validateEditorMaxLength(editorInstance) {
    if (!editorInstance) return null;

    const length = editorInstance.getLength() - 1;
    const maxCount = MAX_TEXT_LENGTH;
    return length <= maxCount || `${t('rules.enterNoMore', { ns: 'commands' })} ${maxCount} ${t('rules.symbols', { ns: 'commands' })}`;
}
