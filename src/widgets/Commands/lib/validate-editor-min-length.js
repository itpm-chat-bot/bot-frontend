import { t } from 'i18next';

export function validateEditorMinLength(editorInstance) {
    if (!editorInstance) return null;

    const length = editorInstance.getLength() - 1;
    return length > 0 || (t('rules.obligatoryField', { ns: 'commands' }));
}
