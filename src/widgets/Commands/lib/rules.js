import { MAX_COMMAND_DESCRIPTION_COUNT, MAX_COMMAND_NAME_COUNT } from '../model/constants.js';

export function commandRules(t, existingCommands, isEditing) {
    return {
        commandName: {
            maxLength: {
                value: MAX_COMMAND_NAME_COUNT,
                message: `${t('rules.enterNoMore')} ${MAX_COMMAND_NAME_COUNT} ${t('rules.symbols')}`,
            },
            validate: (value) => {
                const sanitizedValue = value.trim().replace(/^\//, ''); // Удаление символа '/' в начале строки
                const isValidFormat = /^[a-zA-Z0-9_/]+$/.test(sanitizedValue);

                if (sanitizedValue === '') {
                    return t('rules.obligatoryField');
                }

                if (!isValidFormat) {
                    return t('rules.unacceptableSymbols');
                }

                if (!isEditing && existingCommands.includes(sanitizedValue)) {
                    return t('rules.commandAlreadyExists');
                }

                return true;
            },
        },
        commandDescription: {
            maxLength: {
                value: MAX_COMMAND_DESCRIPTION_COUNT,
                message: `${t('rules.enterNoMore')} ${MAX_COMMAND_DESCRIPTION_COUNT} ${t('rules.symbols')}`,
            },
            validate: (value) => {
                if (value.trim() === '') {
                    return (t('rules.obligatoryField'));
                }
                return true;
            },
        },
    };
}
