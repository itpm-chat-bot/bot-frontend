export function getDefaultCommandValues(
    commandName = '',
    commandDescription = '',
    commandText = '',
    type = 'text',
    value = '',
) {
    return {
        commandName,
        commandDescription,
        commandText,
        type,
        value,
    };
}
export function getDefaultCommandRulesValues(
    commandName = '/rules',
    commandDescription = 'Правила этого чата',
    commandText = '',
    type = 'text',
    value = '',
) {
    return {
        commandName,
        commandDescription,
        commandText,
        type,
        value,
    };
}
