export const MAX_COMMAND_NAME_COUNT = 32;
export const MAX_COMMAND_DESCRIPTION_COUNT = 256;
export const MAX_COMMAND_TEXT_COUNT = 20;

/* FOR rich text editor */
export const MAX_TEXT_LENGTH = 4095;
export const MAX_TEXT_LENGTH_WITH_ATTACHMENTS = 1023;

export const sanitizeCommand = (text) => {
    return text.replace(/^\//, '').replace(/<[^>]*>/g, '');
};
