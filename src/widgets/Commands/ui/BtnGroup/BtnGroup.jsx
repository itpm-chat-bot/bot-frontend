import { useTranslation } from 'react-i18next';

import { Button } from '@/shared/ui';

import styles from './BtnGroup.module.css';

export function BtnGroup({ saveCommand, cancelCommand }) {
    const { t } = useTranslation('commands');
    const handleSaveClick = () => {
        saveCommand();
    };
    const handleCancelClick = () => {
        cancelCommand();
    };
    return (
        <div className={styles.btnGroup}>
            <Button
                id='cancel'
                variant='secondary'
                onClick={handleCancelClick}
            >
                {t('commandForm.cancelButton')}
            </Button>

            <Button
                id='now'
                variant='primary'
                onClick={handleSaveClick}
            >
                {t('commandForm.saveBtn')}
            </Button>
        </div>
    );
}

// Specify default props and prop types if NotificationSettings component will accept props
