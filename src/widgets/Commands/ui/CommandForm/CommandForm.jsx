import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';

import { useEditor } from '@/shared/lib';
import { commandsSelectors, createCommand, updateCommand } from '@/shared/redux/slices/commandsSlice';
import { Aside, CommonHead, Container, FormInput, FormRichTextEditor, Main } from '@/shared/ui';

import { ExitModal } from '../../../onReviewComponents/UseNavigateBlocking/ExitModal';
import { useNavigationBlocking } from '../../../onReviewComponents/UseNavigateBlocking/useNavigationBlocking';
import { commandRules } from '../../lib/rules';
import { getDefaultCommandRulesValues, getDefaultCommandValues } from '../../lib/utils.js';
import { validateEditorMaxLength } from '../../lib/validate-editor-max-length.js';
import { validateEditorMinLength } from '../../lib/validate-editor-min-length.js';
import { MAX_COMMAND_DESCRIPTION_COUNT, MAX_COMMAND_NAME_COUNT, sanitizeCommand } from '../../model/constants';
import { BtnGroup } from '../BtnGroup/BtnGroup';
import { CommandAvailable } from '../CommandManage/CommandAvailable';
import { CommandManage } from '../CommandManage/CommandManage';

import styles from './CreateCommand.module.css';

export function CommandForm(props) {
    const {
        selectedOption,
        setSelectedOption,
        handleCheckboxChange,
        mainText,
        selectedNumberOfCalls,
        setSelectedNumberOfCalls,
        selectDeleteTime,
        setSelectDeleteTime,

        isEditing = false,
        isRules = false,
        subtitle,
    } = props;

    const {
        watch,
        handleSubmit,
        setValue, control,
        reset,
    } = useForm({
        defaultValues: isRules ? getDefaultCommandRulesValues() : getDefaultCommandValues(),
    });

    const { chatId, commandId } = useParams();
    const dispatch = useDispatch();
    const command = useSelector(commandsSelectors.selectAll);
    const navigate = useNavigate();
    const { t } = useTranslation('commands');
    const [existingCommands, setExistingCommands] = useState([]);
    const rules = useMemo(() => commandRules(t, existingCommands, isEditing), [t, existingCommands, isEditing]);
    const { editorInstance } = useEditor();
    const commandText = watch('commandText');

    const isBlockingCondition = useCallback(() => {
        const { commandName, commandDescription } = watch();
        if (isRules) {
            const defaultValues = getDefaultCommandRulesValues();
            return (
                commandName.trim() !== defaultValues.commandName.trim()
                || commandDescription.trim() !== defaultValues.commandDescription.trim()
            );
        }
        if (isEditing) {
            const commandItem = command.find((item) => item.id === Number(commandId));
            return (
                commandName.trim() !== commandItem?.commandName
                || commandDescription.trim() !== commandItem?.commandDescription
            );
        }
        return commandName || commandDescription;
    }, [command, commandId, isEditing, isRules, watch]);

    const {
        isModalVisible,
        handleLeavePage,
        handleCancel,
    } = useNavigationBlocking(isBlockingCondition);

    const transformCommandName = (value) => {
        const regex = /^[a-zA-Z0-9_/]*$/;
        const inputValue = value.trim();
        const sanitizedValue = inputValue.replace(/\//g, '').replace(/^[^/]/, '/$&');
        return regex.test(sanitizedValue) ? sanitizedValue : value;
    };

    const callPeriod = parseInt(selectedNumberOfCalls, 10) === 10 ? 'day' : 'hour';
    const onNavigate = () => {
        navigate(`/chats/${chatId}/commands`);
    };

    const handleCreateOrUpdateCommand = useCallback(async (data) => {
        const commandData = {
            chat_id: chatId,
            command_name: sanitizeCommand(data.commandName),
            command_description: data.commandDescription,
            command_text: sanitizeCommand(commandText),
            number_calls: selectedNumberOfCalls,
            call_period: callPeriod,
            delete_after_seconds: selectDeleteTime,
            allowed_access: selectedOption,
            chat: chatId,
        };

        try {
            const action = isEditing ? updateCommand : createCommand;
            const commandIdParam = isEditing ? { commandId } : {};
            dispatch(action({ ...commandIdParam, commandData }));
            if (!isEditing) {
                reset();
            }
            navigate(`/chats/${chatId}/commands`);
            toast.success(isEditing
                ? t('commandForm.commandNotification.commandUp')
                : t('commandForm.commandNotification.saveCommand'));
        } catch (error) {
            toast.error('Ошибка при создании команды');
            console.error(error);
        }
    }, [chatId, commandText,
        selectedNumberOfCalls, callPeriod, selectDeleteTime,
        selectedOption, isEditing, commandId, dispatch, navigate, t, reset]);

    const handleCommandSave = handleSubmit(async (data) => {
        await handleCreateOrUpdateCommand(data);
    });
    const onSubmit = () => {
        setValue('commandText', commandText || '');
    };

    useEffect(() => {
        const commandInitData = command.find((item) => item.id === Number(commandId));
        if (commandInitData) {
            setValue('commandName', commandInitData.commandName);
            setValue('commandDescription', commandInitData.commandDescription);
            setValue('commandText', commandInitData.commandText);
        }
    }, [command, commandId, setValue]);

    useEffect(() => {
        const existingCommandName = command.map((item) => item.commandName);
        setExistingCommands(existingCommandName);
    }, [command]);

    return (
        <Main>
            <Container>
                <form className={styles.content} onSubmit={handleSubmit(onSubmit)}>

                    <CommonHead subtitle={subtitle} title={mainText} />
                    <div className={styles.block}>

                        <FormInput
                            control={control}
                            label={t('commandForm.command')}
                            maxLength={MAX_COMMAND_NAME_COUNT}
                            name='commandName'
                            placeholder='/commandName'
                            rules={rules.commandName}
                            transform={transformCommandName}
                        />
                    </div>

                    <div className={styles.block}>
                        <FormInput
                            control={control}
                            label={t('commandForm.description')}
                            maxLength={MAX_COMMAND_DESCRIPTION_COUNT}
                            name='commandDescription'
                            placeholder={t('commandForm.descriptionPlaceholder')}
                            rules={rules.commandDescription}
                        />
                    </div>

                    <div className={styles.block}>
                        <h4 className={styles.h4}>{t('commandForm.commandText')}</h4>
                        <FormRichTextEditor
                            control={control}
                            name='commandText'
                            placeholder={t('commandForm.commandTextPlaceholder')}
                            rules={{
                                validate: {
                                    notEmptyWhenNoAttachments: () => validateEditorMinLength(editorInstance),
                                    lessThanMaxLength: () => validateEditorMaxLength(editorInstance),
                                },
                            }}
                            toolbarFormats={['bold', 'italic',
                                'strikethrough', 'underline',
                                'spoiler', 'link', 'emoji',
                                'clean']}
                            shouldBeFocusedAfterSubmit
                            shouldBeFocusedInitially
                        />
                    </div>

                    <CommandManage
                        selectDeleteTime={selectDeleteTime}
                        selectedNumberOfCalls={selectedNumberOfCalls}
                        setSelectDeleteTime={setSelectDeleteTime}
                        setSelectedNumberOfCalls={setSelectedNumberOfCalls}
                    />

                    <CommandAvailable
                        selectedOption={selectedOption}
                        setSelectedOption={setSelectedOption}
                        onCheckboxChange={handleCheckboxChange}
                    />

                </form>
            </Container>
            <Aside>
                <div className={styles.buttonBlock}>
                    <BtnGroup cancelCommand={onNavigate} saveCommand={handleCommandSave} />
                </div>
                <ExitModal isVisible={isModalVisible} onCancel={handleCancel} onClose={handleLeavePage} />
            </Aside>

        </Main>
    );
}
