import { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate, useParams } from 'react-router-dom';

import { AddIcon, DeleteIcon, DoneIcon, EditIcon } from '@/shared/assets/icons/svg';
import { commandsSelectors, deleteCommand, fetchCommands } from '@/shared/redux/slices/commandsSlice';
import { Button } from '@/shared/ui';

import { DeleteModal } from '../DeleteModal';

import styles from './MyCommands.module.css';

export function MyCommands() {
    const { t } = useTranslation('commands');
    const accessMap = {
        all: t('myCommands.forAll'),
        admin: t('myCommands.forAdmin'),
    };

    const dispatch = useDispatch();
    const { chatId } = useParams();
    const commands = useSelector(commandsSelectors.selectAll);
    const navigate = useNavigate();
    const [hoveredCommand, setHoveredCommand] = useState(null);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [deleteCommandId, setDeleteCommandId] = useState(null);
    const showModal = () => {
        setIsModalVisible(true);
    };

    const deleteModalTitle = t('modals.deleteCommandTitle');
    const deleteModalSubTitle = t('modals.deleteCommandSubTitle');

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleDeleteCommand = useCallback(() => {
        if (deleteCommandId !== null) {
            dispatch(deleteCommand(deleteCommandId)); // Используем deleteCommandId для удаления команды
            setIsModalVisible(false); // Закрываем модальное окно после удаления
        }
    }, [deleteCommandId, dispatch]);

    const handleMouseEnter = useCallback((commandId) => {
        setHoveredCommand(commandId);
    }, []);

    const handleMouseLeave = useCallback(() => {
        setHoveredCommand(null);
    }, []);

    useEffect(() => {
        if (!chatId) {
            console.error('Invalid chatId');
            return;
        }

        dispatch(fetchCommands(chatId));
    }, [chatId, dispatch]);

    const sortedCommands = commands
        .filter((command) => !['faq', 'experts'].includes(command.commandName))
        .sort((a, b) => b.id - a.id);

    return (
        <div className={styles.myCommand}>
            <h3 className={styles.title}>{t('myCommands.myCommands')}</h3>

            <div className={styles.commandBlock}>
                <Link to={`/chats/${chatId}/commands/command`}>
                    <div className={styles.addCommand}>
                        <Button
                            rightIcon={<AddIcon title={t('myCommands.plusIconTitle')} />}
                            variant='tertiary'
                        />
                    </div>
                </Link>

                {sortedCommands.map((command) => (
                    <div
                        key={command.id}
                        className={styles.command}
                        onMouseEnter={() => handleMouseEnter(command.id)}
                        onMouseLeave={handleMouseLeave}
                    >
                        <div>
                            <h5 className={styles.commandTitle}>{command.commandDescription}</h5>
                            <h5 className={styles.commandContentText}>
                                /
                                {command.commandName}
                            </h5>
                            <h5 className={styles.commandContentText}>
                                {accessMap[command.allowedAccess] || `#${command.allowedAccess}`}
                            </h5>
                        </div>
                        <div className={styles.controlBlock}>
                            <div title={t('myCommands.callCounter')}>
                                {command.callCounter}
                            </div>
                            <div className={styles.control}>
                                {hoveredCommand === command.id ? (
                                    <div className={styles.control}>
                                        <Button
                                            rightIcon={<EditIcon title={t('myCommands.editIconTitle')} />}
                                            variant='tertiary'
                                            onClick={() => navigate(`/chats/${chatId}/commands/editCommand/${command.id}`)}
                                        />
                                        <Button
                                            rightIcon={<DeleteIcon title={t('myCommands.deleteIconTitle')} />}
                                            variant='tertiary'
                                            onClick={() => {
                                                showModal();
                                                setDeleteCommandId(command.id); // Устанавливаем ID команды для удаления при клике на кнопку удаления
                                            }}
                                        />
                                    </div>
                                ) : (
                                    <DoneIcon />
                                )}
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            <DeleteModal
                isVisible={isModalVisible}
                subTitle={deleteModalSubTitle}
                title={deleteModalTitle}
                onCancel={handleCancel}
                onDelete={handleDeleteCommand}
            />
        </div>

    );
}
