import { useTranslation } from 'react-i18next';
import { NavLink, useParams } from 'react-router-dom';

import styles from './MyCommands.module.css';

export function ReadyMadeCommands() {
    const { chatId } = useParams();
    const { t } = useTranslation('commands');

    return (
        <div className={styles.myCommand}>
            <h3 className={styles.title}>
                {t('readyCommands')}
            </h3>

            <div className={styles.commandBlock}>
                <NavLink to={`/chats/${chatId}/rules`}>
                    <div className={styles.command}>
                        <div>
                            <h5 className={styles.commandTitle}>Правила чата</h5>
                            <h5 className={styles.commandContentText}> /rules</h5>
                            <h5 className={styles.commandContentText}> #для всех</h5>
                        </div>
                    </div>
                </NavLink>
            </div>
        </div>
    );
}
