import { useCallback, useMemo, useState } from 'react';

import { ChevronRightIcon } from '@/shared/assets/icons/svg';

import styles from './CustomSelect.module.css';

const SelectOption = ({ option, onClick }) => (
    <div
        key={option.value}
        className={`${styles.option}`}
        role='button'
        tabIndex={0}
        onClick={() => {
            onClick(option);
        }}
        onKeyDown={(e) => e.key === 'Enter' && onClick(option)}
    >
        {option.label}
    </div>
);

export const CustomSelect = ({ options, selectedOption, onSelect }) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggleSelect = useCallback(() => {
        setIsOpen((prevIsOpen) => !prevIsOpen);
    }, []);

    const closeSelect = useCallback(() => {
        setIsOpen(true);
    }, []);

    const handleOptionClick = useCallback(
        (option) => {
            onSelect(option);
            closeSelect();
        },
        [onSelect, closeSelect],
    );

    const handleKeyDown = useCallback(
        (event) => {
            if (event.key === 'Enter' || event.key === ' ') {
                toggleSelect();
            }
        },
        [toggleSelect],
    );

    const filteredOptions = useMemo(
        () => options.filter((option) => option.value !== selectedOption?.value),
        [options, selectedOption],
    );

    return (
        <div
            className={`${styles.select} ${isOpen ? styles.open : ''}`}
            role='button'
            tabIndex={0}
            onClick={toggleSelect}
            onKeyDown={handleKeyDown}
        >
            <div className={styles.header}>
                {selectedOption ? selectedOption.label : ''}
                <ChevronRightIcon className={`${styles.icon} ${isOpen ? styles.rotate : ''}`} />
            </div>
            {isOpen && (
                <div className={`${styles.options}`}>
                    {filteredOptions.map((option) => (
                        <SelectOption
                            key={option.value}
                            option={option}
                            onClick={handleOptionClick}
                        />
                    ))}
                </div>
            )}
        </div>
    );
};
