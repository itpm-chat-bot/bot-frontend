import { useTranslation } from 'react-i18next';

import { CustomSelect } from '../CustomSelect/CustomSelect';

import styles from './CommandManage.module.css';

export function NumberOfCalls({
    selectedNumberOfCalls,
    setSelectedNumberOfCalls,
    t,
}) {
    const callsOptions = [
        { value: 3, label: `${t('commandManage.noMore')} 3 ${t('commandManage.onceAnHour')}` },
        { value: 5, label: `${t('commandManage.noMore')} 5 ${t('commandManage.onceAnHour')}` },
        { value: 10, label: `${t('commandManage.noMore')} 10 ${t('commandManage.onceAnDay')}` },
        { value: 0, label: t('commandManage.noLimits') },
    ];

    return (
        <div className={styles.manage}>
            <h4>
                {' '}
                {t('commandManage.numberOfCalls')}
                {' '}
            </h4>
            <CustomSelect
                defaultOption={callsOptions[3]}
                options={callsOptions}
                selectedOption={callsOptions.find((option) => option.value === selectedNumberOfCalls)}
                onSelect={(option) => setSelectedNumberOfCalls(option.value)}
            />
        </div>
    );
}

export function DeleteTime({
    selectDeleteTime,
    setSelectDeleteTime,
    t,
}) {
    const deleteOptions = [
        { value: 300, label: `5 ${t('commandManage.minutes')}` },
        { value: 1800, label: `30 ${t('commandManage.minutes')}` },
        { value: 3600, label: `1 ${t('commandManage.hour')}` },
        { value: 86400, label: `1 ${t('commandManage.day')}` },
        { value: 0, label: t('commandManage.doNotDelete') },
    ];
    return (
        <div className={styles.manage}>
            <h4>
                {t('commandManage.deleteMessageTitle')}
            </h4>
            <CustomSelect
                defaultOption={deleteOptions[3]}
                options={deleteOptions}
                selectedOption={deleteOptions.find((option) => option.value === selectDeleteTime)}
                onSelect={(option) => setSelectDeleteTime(option.value)}
            />
        </div>
    );
}

export function CommandManage({
    selectedNumberOfCalls,
    setSelectedNumberOfCalls,
    selectDeleteTime,
    setSelectDeleteTime,
}) {
    const { t } = useTranslation('commands');
    return (
        <div className={styles.ruleManage}>
            <NumberOfCalls
                selectedNumberOfCalls={selectedNumberOfCalls}
                setSelectedNumberOfCalls={setSelectedNumberOfCalls}
                t={t}
            />
            <DeleteTime
                selectDeleteTime={selectDeleteTime}
                setSelectDeleteTime={setSelectDeleteTime}
                t={t}
            />
        </div>
    );
}
