import { useTranslation } from 'react-i18next';

import { RadioButton } from '@/shared/ui';

import styles from './CommandManage.module.css';

export function CommandAvailable({ onCheckboxChange, selectedOption, setSelectedOption }) {
    const handleOptionChange = (event) => {
        setSelectedOption(event.target.value);
        onCheckboxChange(event.target.value);
    };
    const { t } = useTranslation('commands');
    return (
        <div className={styles.commandAvailable}>
            <h4>
                {t('commandManage.commandAvailable.availableTitle')}
            </h4>
            <div className={styles.choice}>
                <RadioButton
                    checked={selectedOption === 'all'}
                    label={t('commandManage.commandAvailable.all')}
                    name='all'
                    value='all'
                    onChange={handleOptionChange}
                />
                <RadioButton
                    checked={selectedOption === 'admin'}
                    label={t('commandManage.commandAvailable.admin')}
                    name='admin'
                    value='admin'
                    onChange={handleOptionChange}
                />
            </div>
        </div>
    );
}
