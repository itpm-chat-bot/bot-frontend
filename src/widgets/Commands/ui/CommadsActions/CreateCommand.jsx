import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CommandForm } from '../CommandForm/CommandForm';

export function CreateCommand() {
    const { t } = useTranslation('commands');
    const [selectedOption, setSelectedOption] = useState('all');
    const [selectedNumberOfCalls, setSelectedNumberOfCalls] = useState(0);
    const [selectDeleteTime, setSelectDeleteTime] = useState(300);

    const handleCheckboxChange = (selectedValue) => {
        setSelectedOption(selectedValue);
    };

    return (
        <CommandForm
            handleCheckboxChange={handleCheckboxChange}
            mainText={t('createCommand')}
            selectDeleteTime={selectDeleteTime}
            selectedNumberOfCalls={selectedNumberOfCalls}
            selectedOption={selectedOption}
            setSelectDeleteTime={setSelectDeleteTime}
            setSelectedNumberOfCalls={setSelectedNumberOfCalls}
            setSelectedOption={setSelectedOption}
            onCheckboxChange={handleCheckboxChange}

        />
    );
}
