import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { commandsSelectors } from '@/shared/redux/slices/commandsSlice';

import { CommandForm } from '../CommandForm/CommandForm';

export function EditCommand() {
    const { commandId } = useParams();
    const { t } = useTranslation('commands');
    const command = useSelector(commandsSelectors.selectAll);

    const [selectedOption, setSelectedOption] = useState('');

    const [selectedNumberOfCalls, setSelectedNumberOfCalls] = useState('');
    const [selectDeleteTime, setSelectDeleteTime] = useState('');

    useEffect(() => {
        const commandItem = command.find((item) => item.id === Number(commandId));
        if (commandItem) {
            setSelectedOption(commandItem.allowedAccess);
            setSelectedNumberOfCalls(commandItem.numberCalls);
            setSelectDeleteTime(commandItem.deleteAfterSeconds);
        }
    }, [command, commandId]);

    const handleCheckboxChange = (selectedValue) => {
        setSelectedOption(selectedValue);
    };

    return (
        <CommandForm
            handleCheckboxChange={handleCheckboxChange}
            mainText={t('editCommand')}
            selectDeleteTime={selectDeleteTime}
            selectedNumberOfCalls={selectedNumberOfCalls}
            selectedOption={selectedOption}
            setSelectDeleteTime={setSelectDeleteTime}
            setSelectedNumberOfCalls={setSelectedNumberOfCalls}
            setSelectedOption={setSelectedOption}
            isEditing
        />
    );
}
