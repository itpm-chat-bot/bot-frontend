import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { commandsSelectors } from '@/shared/redux/slices/commandsSlice';
import { CommonHead, Container, Main } from '@/shared/ui';

import { MyCommands } from '../MyCommands/MyCommands';
import { ReadyMadeCommands } from '../MyCommands/ReadyMadeCommands';

import styles from './Commands.module.css';

export function CommandsPage() {
    const commands = useSelector(commandsSelectors.selectAll);
    const { t } = useTranslation('commands');

    const shouldRenderReadyMadeCommands = commands.length === 2;

    return (
        <Main>
            <Container>
                <div className={styles.wrap}>
                    <CommonHead
                        subtitle={t('commandsSubTitle')}
                        title={t('commandsTitle')}
                    />
                    {shouldRenderReadyMadeCommands && <ReadyMadeCommands />}
                    <MyCommands />
                </div>
            </Container>
        </Main>
    );
}
