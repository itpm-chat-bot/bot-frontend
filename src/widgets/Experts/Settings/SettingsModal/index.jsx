import { Modal } from 'antd';

import { Settings } from '../index';

export function SettingModal({ open, onCancel, onReturn }) {
    const handleOk = () => {
        onReturn(false);
    };

    return (
        <Modal
            className='custom-modal-settings'
            closable={false}
            footer={null}
            open={open}
            onCancel={onCancel}
            onOk={handleOk}
        >
            <Settings onCancel={onCancel} />
        </Modal>
    );
}
