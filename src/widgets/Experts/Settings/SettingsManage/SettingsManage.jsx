import { CallCommand } from './CallCommand';
import { DeleteCommand } from './DeleteCommand';

import styles from '../Settings.module.css';

export function SettingsManage({
    initialNumberOfCalls,
    setSelectedNumberOfCalls,
    initialSelectDeleteTime,
    setSelectDeleteTime,
}) {
    return (
        <div className={styles.manage}>
            <CallCommand
                initialData={initialNumberOfCalls}
                setSelectedNumberOfCalls={setSelectedNumberOfCalls}
            />
            <DeleteCommand
                initialData={initialSelectDeleteTime}
                setSelectDeleteTime={setSelectDeleteTime}
            />
        </div>
    );
}
