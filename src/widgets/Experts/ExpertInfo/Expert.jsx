import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Avatar } from '@/entities/Avatar';
import { EditIcon, DeleteIcon } from '@/shared/assets/icons/svg';
import { getInitials } from '@/shared/lib';
import { Button } from '@/shared/ui';

import { DeleteModal } from '../Modals/DeleteModal';

import styles from './Expert.module.css';

export function Expert({ expert, deleteExpert, toggleAddExpertVisibility }) {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const { t } = useTranslation('experts');

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleEditClick = () => {
        toggleAddExpertVisibility(expert.id);
    };
    const deleteModalTitle = t('modals.deleteExpertNotification');

    return (
        <div key={expert.id} className={styles.expert}>
            <div className={styles.block}>
                <div className={styles.info}>
                    <div className={styles.ava}>
                        <Avatar alt='' fallbackText={getInitials(expert.fullName)} size='big' />
                    </div>
                    <div>
                        <h4 className={styles.name}>{expert.fullName}</h4>
                        <h4>{expert.username}</h4>
                    </div>
                </div>
                <div className={styles.description}>
                    <h4>{expert.description}</h4>
                </div>
            </div>
            <div className={styles.manage}>
                <Button
                    rightIcon={<EditIcon title={t('edit')} />}
                    size='medium'
                    variant='tertiary'
                    onClick={handleEditClick}
                />
                <Button
                    rightIcon={<DeleteIcon title={t('delete')} />}
                    size='medium'
                    variant='tertiary'
                    onClick={showModal}
                />

            </div>
            <DeleteModal
                isVisible={isModalVisible}
                title={deleteModalTitle}
                onCancel={handleCancel}
                onDelete={() => deleteExpert(expert.id)}
            />
        </div>
    );
}
