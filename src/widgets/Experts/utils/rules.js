import { MAX_INFO_COUNT, MAX_NAME_COUNT, MAX_NICK_NAME_COUNT, MIN_NAME_COUNT } from '../model/constants.js';

export function createRules(t, existingUsernames) {
    return {
        nickName: {
            required: t('rules.obligatoryField'),
            maxLength: {
                value: MAX_NICK_NAME_COUNT,
                message: `${t('rules.enterNoMore')} ${MAX_NICK_NAME_COUNT} ${t('rules.symbols')}`,
            },
            minLength: {
                value: MIN_NAME_COUNT,
                message: `${t('rules.minimumLength')} ${MIN_NAME_COUNT} ${t('rules.symbols')}`,
            },
            validate: (value) => {
                const isStartsWithAt = value.startsWith('@');
                const isValidFormat = /^[a-zA-Z0-9_@]+$/.test(value);

                if (!isStartsWithAt) {
                    return t('rules.nicknameShouldBegin');
                }

                if (!isValidFormat) {
                    return t('rules.unacceptableSymbols');
                }
                if (existingUsernames.includes(value)) {
                    return t('rules.expertExists');
                }

                return true;
            },
        },

        name: {
            required: t('rules.obligatoryField'),
            maxLength: {
                value: MAX_NAME_COUNT,
                message: `${t('rules.enterNoMore')} ${MAX_NAME_COUNT} ${t('rules.symbols')}`,
            },
        },
        info: {
            maxLength: {
                value: MAX_INFO_COUNT,
                message: `${t('rules.enterNoMore')} ${MAX_INFO_COUNT} ${t('rules.symbols')}`,
            },
        },
    };
}
