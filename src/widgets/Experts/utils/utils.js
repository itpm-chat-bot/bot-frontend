export function getDefaultExpertsValues(
    expertUserName = '',
    expertInfo = '',
    expertName = '',
    type = 'text',
    value = '',
) {
    return {
        expertUserName,
        expertName,
        expertInfo,
        type,
        value,
    };
}

export const capitalizeEachWord = (str) => {
    return str
        .split(' ')
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ');
};
