import { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { CloseIcon } from '@/shared/assets/icons/svg';
import { createExpert, expertsSelectors, fetchExperts, updateExpert } from '@/shared/redux/slices/expertsSlice';
import { Button, FormInput, FormTextArea } from '@/shared/ui';

import { ExitModal } from '../../onReviewComponents/UseNavigateBlocking/ExitModal';
import { useNavigationBlocking } from '../../onReviewComponents/UseNavigateBlocking/useNavigationBlocking';
import { MAX_INFO_COUNT, MAX_NAME_COUNT, MAX_NICK_NAME_COUNT, MIN_NAME_COUNT } from '../model/constants';
import { createRules } from '../utils/rules';
import { capitalizeEachWord, getDefaultExpertsValues } from '../utils/utils';

import styles from './ExpertForm.module.css';

export function ExpertForm({ type, expertId, closeAside, blockTitle }) {
    const [isExpertUp, setIsExpertUp] = useState(false);
    const [existingUsernames, setExistingUsernames] = useState([]);
    const expertData = useSelector(expertsSelectors.selectAll);
    const { chatId } = useParams();
    const dispatch = useDispatch();
    const { t } = useTranslation('experts');
    const rules = useMemo(() => createRules(t, existingUsernames), [t, existingUsernames]);
    const {
        watch,
        control,
        handleSubmit,
        setValue,
        reset,
    } = useForm({
        defaultValues: getDefaultExpertsValues(),
    });

    const watchExpertInfo = watch('expertInfo');
    const watchExpertUserName = watch('expertUserName');
    const watchExpertName = watch('expertName');

    const handleCloseAside = useCallback(() => {
        closeAside();
    }, [closeAside]);

    const isBlockingCondition = () => {
        if (type === 'edit') {
            const expertInitData = expertData.find((item) => item.id === Number(expertId));
            return (
                expertInitData && (watchExpertUserName !== expertInitData.username
                    || watchExpertName !== expertInitData.fullName
                    || watchExpertInfo !== expertInitData.description)
            );
        }
        return watchExpertUserName || watchExpertName || watchExpertInfo;
    };
    const {
        isModalVisible,
        handleLeavePage,
        handleCancel,
    } = useNavigationBlocking(isBlockingCondition);

    const handleSaveExpert = useCallback(async (data) => {
        const expertDetails = {
            username: data.expertUserName,
            full_name: capitalizeEachWord(data.expertName),
            description: data.expertInfo,
            chat: chatId,
        };

        if (type === 'edit') {
            dispatch(updateExpert({ expertId: Number(expertId), expertData: expertDetails }));
            toast.success(t('expertActions.expertDateUpNotification'));
        } else {
            dispatch(createExpert({ newExpert: data, chatId, expertData: expertDetails }));
            toast.success(t('expertActions.expertAddedNotification'));
        }

        setIsExpertUp(true);
    }, [dispatch, expertId, chatId, t, type]);

    const handleExpertSave = handleSubmit(async (data) => {
        await handleSaveExpert(data);
        reset();
        handleCloseAside();
    });

    const handleInputChange = useCallback((e) => {
        const regex = /^[a-zA-Z0-9_@]*$/;
        const inputValue = e.target.value.trim();
        const sanitizedValue = inputValue.replace(/@/g, '').replace(/^[^@]/, '@$&');

        if (regex.test(sanitizedValue)) {
            e.target.value = sanitizedValue;
        }
    }, []);

    useEffect(() => {
        dispatch(fetchExperts(chatId));
        setIsExpertUp(false);
    }, [chatId, isExpertUp, dispatch]);

    useEffect(() => {
        const expertInitData = expertData.find((item) => item.id === Number(expertId));
        if (expertInitData) {
            setValue('expertUserName', expertInitData.username);
            setValue('expertName', expertInitData.fullName);
            setValue('expertInfo', expertInitData.description);
        }
    }, [expertData, expertId, setValue]);

    useEffect(() => {
        if (type === 'edit') return;
        dispatch(fetchExperts(chatId));
        setIsExpertUp(false);
    }, [chatId, isExpertUp, dispatch, type]);

    useEffect(() => {
        if (type === 'edit') return;
        const usernames = expertData.map((expert) => expert.username);
        setExistingUsernames(usernames);
    }, [expertData, type]);

    return (
        <div className={styles.container}>
            <div className={styles.close}>
                <Button leftIcon={<CloseIcon />} size='medium' variant='tertiary' onClick={handleCloseAside} />
            </div>
            <div className={styles.content}>
                <div className={styles.topBlock}>
                    <h2 className={styles.h2}>{blockTitle}</h2>
                </div>
                <form className={styles.fieldGroup}>
                    <FormInput
                        control={control}
                        id='expertUserName'
                        label={t('expertActions.nickname')}
                        maxLength={MAX_NICK_NAME_COUNT}
                        minLength={MIN_NAME_COUNT}
                        name='expertUserName'
                        placeholder='@nickname'
                        rules={rules.nickName}
                        transform={handleInputChange}
                    />

                    <FormInput
                        control={control}
                        label={t('expertActions.expertName')}
                        maxLength={MAX_NAME_COUNT}
                        name='expertName'
                        placeholder={t('expertActions.howIsExpertName')}
                        rules={rules.name}
                    />

                    <FormTextArea
                        control={control}
                        label={t('expertActions.howTheyCanHelp')}
                        maxLength={MAX_INFO_COUNT}
                        name='expertInfo'
                        placeholder={t('expertActions.howTheyCanHelpPlaceholder')}
                        rules={rules.info}
                    />
                </form>
            </div>

            <Button type='submit' variant='primary' onClick={handleExpertSave}>
                {blockTitle === t('newExpert') ? t('newExpert') : t('expertActions.saveBtn')}
            </Button>
            <ExitModal isVisible={isModalVisible} onCancel={handleCancel} onClose={handleLeavePage} />
        </div>
    );
}
