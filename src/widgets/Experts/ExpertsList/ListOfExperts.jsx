import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { AddIcon } from '@/shared/assets/icons/svg';
import { cn } from '@/shared/lib';
import { deleteExpert, fetchExperts, expertsSelectors } from '@/shared/redux/slices/expertsSlice';
import { Button } from '@/shared/ui';

import { Expert } from '../ExpertInfo/Expert';

import styles from './ExpertsList.module.css';

export function ListOfExperts({ toggleAddExpertVisibility, hasAsideContent }) {
    const { t } = useTranslation('experts');
    const { chatId } = useParams();
    const dispatch = useDispatch();
    const experts = useSelector(expertsSelectors.selectAll);
    const [editingIndex, setEditingIndex] = useState(null);

    const handleDeleteExpert = async (id) => {
        dispatch(deleteExpert(id));
    };

    const handleEditExpert = async (index) => {
        setEditingIndex(index);
    };

    useEffect(() => {
        dispatch(fetchExperts(chatId));
    }, [chatId, dispatch]);

    const handleAddExpertClick = () => {
        toggleAddExpertVisibility();
    };

    const isExpert = experts.length === 0;
    return (
        <div className={styles.list}>
            <div className={cn(
                styles.expertInfo,
                {
                    [styles.displayType]: !experts.length,
                    [styles.columnFlex]: hasAsideContent,
                },
            )}
            >
                <div className={styles.addButton}>
                    <Button leftIcon={<AddIcon />} variant='tertiary' onClick={handleAddExpertClick}>
                        {t('addExpert')}
                    </Button>
                </div>
                {!isExpert && experts.map((expert, index) => (
                    <Expert
                        key={expert.id}
                        deleteExpert={handleDeleteExpert}
                        editExpert={() => handleEditExpert(index)}
                        expert={expert}
                        isEditing={editingIndex === index}
                        toggleAddExpertVisibility={toggleAddExpertVisibility}
                    />
                ))}
            </div>
        </div>
    );
}
