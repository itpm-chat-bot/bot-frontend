import { useTranslation } from 'react-i18next';

import { ExpertForm } from '../ExpertForm/ExpertForm';

export function AddNewExpert({ closeAside }) {
    const { t } = useTranslation('experts');
    const blockTitle = t('newExpert');

    return (
        <ExpertForm blockTitle={blockTitle} closeAside={closeAside} type='add' />
    );
}
