import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { fetchExperts } from '@/shared/redux/slices/expertsSlice';

import { ExpertForm } from '../ExpertForm/ExpertForm';

export function EditExpert({ expertId, closeAside }) {
    const dispatch = useDispatch();
    const { chatId } = useParams();
    const { t } = useTranslation('experts');

    useEffect(() => {
        dispatch(fetchExperts(chatId));
    }, [chatId, dispatch]);

    const blockTitle = t('edit');

    return (
        <ExpertForm blockTitle={blockTitle} closeAside={closeAside} expertId={expertId} type='edit' />
    );
}
