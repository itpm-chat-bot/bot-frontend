import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { SettingsIcon } from '@/shared/assets/icons/svg';
import { Aside, AsideContent, Button, CommonHead, Container, Main } from '@/shared/ui';

import { AddNewExpert } from './ExpertActions/AddNewExpert';
import { EditExpert } from './ExpertActions/EditExpert';
import { ListOfExperts } from './ExpertsList/ListOfExperts';
import { SettingModal } from './Settings/SettingsModal';

import styles from './Experts.module.css';

export function ExpertsPage() {
    const { t } = useTranslation('experts');
    const [isAddExpertVisible, setIsAddExpertVisible] = useState(false);
    const [editingExpertId, setEditingExpertId] = useState(null);
    const [isSettingsModalVisible, setSettingsModalVisible] = useState(false);

    const toggleAddExpertVisibility = (expertId) => {
        setIsAddExpertVisible(true);
        setEditingExpertId(expertId);
    };

    const closeAside = () => {
        setIsAddExpertVisible(false);
        setEditingExpertId(null);
    };

    const openSettingsModal = () => {
        setSettingsModalVisible(true);
    };

    const closeSettingsModal = () => {
        setSettingsModalVisible(false);
    };
    return (
        <Main>
            <Container>
                <div className={styles.header}>
                    <CommonHead
                        subtitle={t('subtitle')}
                        title={t('experts')}
                    />
                    <Button
                        leftIcon={<SettingsIcon />}
                        size='big'
                        variant='tertiary'
                        onClick={openSettingsModal}
                    />
                </div>
                <ListOfExperts
                    hasAsideContent={isAddExpertVisible || editingExpertId}
                    toggleAddExpertVisibility={toggleAddExpertVisibility}
                />
            </Container>

            {(isAddExpertVisible || editingExpertId) && (
                <Aside>
                    <AsideContent>
                        {editingExpertId ? (
                            <EditExpert closeAside={closeAside} expertId={editingExpertId} />
                        ) : (
                            <AddNewExpert closeAside={closeAside} />
                        )}
                    </AsideContent>
                </Aside>
            )}
            <SettingModal open={isSettingsModalVisible} onCancel={closeSettingsModal} onReturn={closeSettingsModal} />
        </Main>
    );
}
