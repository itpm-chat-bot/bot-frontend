export const MAX_NAME_COUNT = 32;
export const MIN_NAME_COUNT = 5;
export const MAX_NICK_NAME_COUNT = 32;
export const MAX_INFO_COUNT = 144;
