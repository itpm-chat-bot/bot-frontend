import react from '@vitejs/plugin-react';
import path from 'path';
import { defineConfig } from 'vite';
import svgr from 'vite-plugin-svgr';
import viteTsconfigPaths from 'vite-tsconfig-paths';

/* eslint eslint-comments/no-use: off */
// eslint-disable-next-line import/no-default-export
export default defineConfig({
    plugins: [
        react(),
        viteTsconfigPaths(),
        svgr({
            include: '**/*.svg',
        }),
    ],
    css: {
        modules: {
            generateScopedName: '[name]_[local]_[hash:base64:8]',
        },
    },
    server: {
        open: true,
        port: 3000,
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, './src'),
        },
    },
});
