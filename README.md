# Description

Web admin-panel for ChatControl bot.

The project is built with [Vite](https://vitejs.dev) and managed by classic [Yarn](https://classic.yarnpkg.com/lang/en/docs/).

To figure out more about project and approaches which is used, read information at Confluence.

Production version: https://chatcontrol.xyz
Dev version: https://dev.chatcontrol.xyz

## Prerequisites
- Node.js >= 18.14.2
- Yarn >= 1.22.19

## 📦 Installation

1. Clone the repository (via SSH / HTTPS):
```
$ git clone <link>
```

2. Change the working directory:
```
$ cd bot-frontend
```

3. Install dependencies:
```
$ yarn install
```

4. Create .env.local and add current environment variables (ask collegues)

## 🔨 Development

To start local server:
```
$ yarn start
```

In order to enter an admin-panel locally, you need to:
- find telegram bot for dev environment in Telegram (the last was `@ITPM_dev_main_bot`);
- enter an admin panel from this bot;
- open Console (F12), copy `searchParams` value
- open new tab with localhost and paste the value to the end of the link `localhost/login?` as query parameters

To lint code:
```
$ yarn lint
```

To lint code and fix all possible problems:
```
$ yarn lint:fix
```

To build app for production:
```
$ yarn build
```