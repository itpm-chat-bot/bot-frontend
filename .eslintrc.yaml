---
root: true

env:
  browser: true
  es2022: true

parserOptions:
  ecmaVersion: latest
  sourceType: module

plugins:
  - import
  - jsx-a11y
  - react
  - react-hooks
  - perfectionist
  - eslint-comments

extends:
  - airbnb
  - plugin:import/recommended
  - plugin:react/recommended
  - plugin:react-hooks/recommended
  - plugin:storybook/recommended

rules:
  # base config rules
  arrow-body-style: "off"
  indent: ["error", 4, { "SwitchCase": 1 }]
  jsx-quotes: ["error", "prefer-single"]
  linebreak-style: "warn"
  lines-between-class-members:
    ["error", "always", { "exceptAfterSingleLine": true }]
  max-len:
    ["error", { "code": 135, "ignoreStrings": true, "ignoreComments": true }]
  no-console: ["error", { "allow": ["warn", "error"] }]
  no-param-reassign: ["error", { "props": false }]
  no-underscore-dangle: ["error", { "allow": ["_retry", ""] }]
  no-unused-vars:
    [
      "error",
      { "argsIgnorePattern": "^_", "destructuredArrayIgnorePattern": "^_", varsIgnorePattern: "^_" },
    ]
  object-curly-newline: ["error", { "minProperties": 9, "consistent": true }]
  prefer-destructuring: "off"

  # plugin rules
  import/extensions: "off"
  import/no-extraneous-dependencies: ["error", { "packageDir": "./" }]
  import/prefer-default-export: "off"
  import/no-default-export: "error"
  import/no-unresolved: ["error", { ignore: ['\.svg\?url$'] }]
  eslint-comments/no-use:
    [
      "error",
      { "allow": ["eslint", "eslint-env", "exported", "global", "globals"] },
    ]
  jsx-a11y/label-has-associated-control: ["error", { "assert": "either" }]
  react/function-component-definition: "off"
  react/jsx-filename-extension: ["warn", { "extensions": [".js", ".jsx"] }]
  react/jsx-indent: ["error", 4, { "indentLogicalExpressions": true }]
  react/jsx-indent-props: ["off", 4]
  react/react-in-jsx-scope: "off"
  react/jsx-sort-props:
    [
      "error",
      { "callbacksLast": true, "shorthandLast": true, "reservedFirst": true },
    ]
  react/jsx-props-no-spreading: "off"
  react/jsx-uses-react: "off"
  react/jsx-uses-vars: "error"
  react/prop-types: "off"
  react/prefer-exact-props: "off"
  perfectionist/sort-imports:
    [
      "error",
      {
        "groups":
          [
            "react",
            ["builtin", "external"],
            "internal-type",
            "internal",
            ["parent-type", "sibling-type", "index-type"],
            ["parent", "sibling", "index"],
            "side-effect",
            "side-effect-style",
            "style",
            "object",
            "unknown",
          ],
        "custom-groups": { "value": { "react": ["react", "react-*"] } },
        "internal-pattern": [
          "@/**"
        ]
      },
    ]

overrides:
  # by default, linter checks only .js file. It allows checking .jsx files in addition
  - files: ["*.jsx", "*.js"]
  - files: ["**/reused/Icons/**/*.jsx"]
    rules:
      react/jsx-sort-props: "off"
  - files: ["*.stories.jsx"]
    rules:
      import/no-default-export: "off"

settings:
  import/resolver:
    alias:
      map: [['@', './src']]
      extensions: ['.js', '.jsx', '.ts', 'tsx', '.json']